Configuration Instructions
====================

1. Mover el folder del proyecto (daedalus) al folder de aplicaciones del servidor web. E.g. En xampp se utiliza htdocs (windows) y www (ubuntu).


Installation Instructions
====================


1. Instalar un ambiente de desarrollo (WAMP, XAMPP).
```
Ref: http://www.magentocommerce.com/wiki/1_-_installation_and_configuration/installing_on_windows_with_xampp_and_wamp
```

2. Instalar Yii v1.1 (http://www.yiiframework.com/)
```
Ref: http://www.yiiframework.com/doc/guide/1.1/en/quickstart.installation
```

3. Descargar proyecto


Operating Instructions
====================

Copyright and Licensing Information
====================

2014 © Instituto de Investigaciones, UVG

Known Bugs
====================

Troubleshooting
====================

Changelog
====================

