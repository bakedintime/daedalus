<?php
$this->breadcrumbs=array(
	'Facultads'=>array('index'),
	$model->idFacultad=>array('view','id'=>$model->idFacultad),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Facultad', 'url'=>array('index')),
	array('label'=>'Crear Facultad', 'url'=>array('create')),
	array('label'=>'View Facultad', 'url'=>array('view', 'id'=>$model->idFacultad)),
	array('label'=>'Administrar Facultad', 'url'=>array('admin')),
);
?>

<h1>Update Facultad <?php echo $model->idFacultad; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>