<?php
$this->breadcrumbs=array(
	'Facultads'=>array('index'),
	$model->idFacultad,
);

$this->menu=array(
	array('label'=>'Listar Facultad', 'url'=>array('index')),
	array('label'=>'Crear Facultad', 'url'=>array('create')),
	array('label'=>'Update Facultad', 'url'=>array('update', 'id'=>$model->idFacultad)),
	array('label'=>'Delete Facultad', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idFacultad),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Facultad', 'url'=>array('admin')),
);
?>

<h1>View Facultad #<?php echo $model->idFacultad; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idFacultad',
		'nombre',
		'decano',
	),
)); ?>
