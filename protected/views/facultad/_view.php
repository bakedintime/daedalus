<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idFacultad')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idFacultad), array('view', 'id'=>$data->idFacultad)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('decano')); ?>:</b>
	<?php echo CHtml::encode($data->decano); ?>
	<br />


</div>