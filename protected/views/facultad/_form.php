<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'facultad-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
	
		<?php echo $form->labelEx($model,'decano'); ?>
		<?php #echo $form->textField($model,'decano'); ?>
		<?php #echo $form->dropDownList($model,'decano',$decanos); ?>
		<?php echo $form->dropDownList($model,'decano', CHtml::listData(decano::model()->findAll(), 'idDecano', 'nombre'), array('empty'=>'-- Seleccionar decano --')); ?>
		<?php echo $form->error($model,'decano'); ?>

	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->