<?php
$this->breadcrumbs=array(
	'Facultads'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar Facultad', 'url'=>array('index')),
	array('label'=>'Administrar Facultad', 'url'=>array('admin')),
);
?>

<h1>Crear Facultad</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>