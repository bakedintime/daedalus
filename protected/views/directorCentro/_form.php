<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'director-centro-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'personal_dc'); ?>
		<?php #echo $form->textField($model,'personal_dc'); ?>

		<?php echo $form->dropDownList($model,'personal_dc', CHtml::listData($personal, 'idPersonal', 'nombreCompleto'), array('empty'=>'-- Seleccionar personal --')); ?>
		<?php echo $form->error($model,'personal_dc'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
