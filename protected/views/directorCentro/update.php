<?php
$this->breadcrumbs=array(
	'Director Centros'=>array('index'),
	$model->idDirector_centro=>array('view','id'=>$model->idDirector_centro),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar DirectorCentro', 'url'=>array('index')),
	array('label'=>'Crear DirectorCentro', 'url'=>array('create')),
	array('label'=>'View DirectorCentro', 'url'=>array('view', 'id'=>$model->idDirector_centro)),
	array('label'=>'Administrar DirectorCentro', 'url'=>array('admin')),
);
?>

<h1>Update DirectorCentro <?php echo $model->idDirector_centro; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>