<?php
$this->breadcrumbs=array(
	'Director Centros'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar Centro', 'url'=>array('index')),
	array('label'=>'Administrar Centro', 'url'=>array('admin')),
	array('label'=>'Listar DirectorCentro', 'url'=>array('index')),
	array('label'=>'Administrar DirectorCentro', 'url'=>array('admin')),
);
?>

<h1>Crear DirectorCentro</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'personal'=>$personal)); ?>