<?php
$this->breadcrumbs=array(
	'Director Centros'=>array('index'),
	$model->idDirector_centro,
);

$this->menu=array(
	array('label'=>'Listar DirectorCentro', 'url'=>array('index')),
	array('label'=>'Crear DirectorCentro', 'url'=>array('create')),
	array('label'=>'Update DirectorCentro', 'url'=>array('update', 'id'=>$model->idDirector_centro)),
	array('label'=>'Delete DirectorCentro', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idDirector_centro),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar DirectorCentro', 'url'=>array('admin')),
);
?>

<h1>View DirectorCentro #<?php echo $model->idDirector_centro; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idDirector_centro',
		'personal_dc',
	),
)); ?>
