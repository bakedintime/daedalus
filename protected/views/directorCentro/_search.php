<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idDirector_centro'); ?>
		<?php echo $form->textField($model,'idDirector_centro'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'personal_dc'); ?>
		<?php echo $form->textField($model,'personal_dc'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->