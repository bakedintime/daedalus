<?php
$this->breadcrumbs=array(
	'Director Centros',
);

$this->menu=array(
	array('label'=>'Crear DirectorCentro', 'url'=>array('create')),
	array('label'=>'Administrar DirectorCentro', 'url'=>array('admin')),
);
?>

<h1>Director Centros</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
