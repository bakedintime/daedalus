<?php
$this->breadcrumbs=array(
	'Investigadors'=>array('index'),
	$model->idInvestigador=>array('view','id'=>$model->idInvestigador),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Investigador', 'url'=>array('index')),
	array('label'=>'Crear Investigador', 'url'=>array('create')),
	array('label'=>'View Investigador', 'url'=>array('view', 'id'=>$model->idInvestigador)),
	array('label'=>'Administrar Investigador', 'url'=>array('admin')),
);
?>

<h1>Update Investigador <?php echo $model->idInvestigador; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>