<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'investigador-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'personal_i'); ?>
		<?php #echo $form->textField($model,'personal_i'); ?>
		<?php echo $form->dropDownList($model,'personal_i', CHtml::listData($personal, 'idPersonal', 'nombreCompleto'), array('empty'=>'-- Seleccionar personal --')); ?>
		<?php echo $form->error($model,'personal_i'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
