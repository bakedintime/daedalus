<?php
$this->breadcrumbs=array(
	'Investigadors'=>array('index'),
	$model->idInvestigador,
);

$this->menu=array(
	array('label'=>'Listar Investigador', 'url'=>array('index')),
	array('label'=>'Crear Investigador', 'url'=>array('create')),
	array('label'=>'Update Investigador', 'url'=>array('update', 'id'=>$model->idInvestigador)),
	array('label'=>'Delete Investigador', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idInvestigador),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Investigador', 'url'=>array('admin')),
);
?>

<h1>View Investigador #<?php echo $model->idInvestigador; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idInvestigador',
		'personal_i',
	),
)); ?>
