<?php
$this->breadcrumbs=array(
	'Investigadors',
);

$this->menu=array(
	array('label'=>'Crear Investigador', 'url'=>array('create')),
	array('label'=>'Administrar Investigador', 'url'=>array('admin')),
);
?>

<h1>Investigadors</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
