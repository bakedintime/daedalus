<?php
$this->breadcrumbs=array(
	'Investigadors'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar Investigador', 'url'=>array('index')),
	array('label'=>'Administrar Investigador', 'url'=>array('admin')),
);
?>

<h1>Crear Investigador</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model, 'personal'=>$personal)); ?>