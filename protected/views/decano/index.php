<?php
$this->breadcrumbs=array(
	'Decanos',
);

$this->menu=array(
	array('label'=>'Crear Decano', 'url'=>array('create')),
	array('label'=>'Administrar Decano', 'url'=>array('admin')),
);
?>

<h1>Decanos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
