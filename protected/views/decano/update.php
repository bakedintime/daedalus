<?php
$this->breadcrumbs=array(
	'Decanos'=>array('index'),
	$model->idDecano=>array('view','id'=>$model->idDecano),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Decano', 'url'=>array('index')),
	array('label'=>'Crear Decano', 'url'=>array('create')),
	array('label'=>'View Decano', 'url'=>array('view', 'id'=>$model->idDecano)),
	array('label'=>'Administrar Decano', 'url'=>array('admin')),
);
?>

<h1>Update Decano <?php echo $model->idDecano; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>