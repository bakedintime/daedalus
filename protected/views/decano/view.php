<?php
$this->breadcrumbs=array(
	'Decanos'=>array('index'),
	$model->idDecano,
);

$this->menu=array(
	array('label'=>'Listar Decano', 'url'=>array('index')),
	array('label'=>'Crear Decano', 'url'=>array('create')),
	array('label'=>'Update Decano', 'url'=>array('update', 'id'=>$model->idDecano)),
	array('label'=>'Delete Decano', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idDecano),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Decano', 'url'=>array('admin')),
);
?>

<h1>View Decano #<?php echo $model->idDecano; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idDecano',
		'nombre',
		'apellido',
		'telefono',
		'email',
	),
)); ?>
