<?php
$this->breadcrumbs=array(
	'Decanos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar Decano', 'url'=>array('index')),
	array('label'=>'Administrar Decano', 'url'=>array('admin')),
);
?>

<h1>Crear Decano</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>