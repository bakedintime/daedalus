<?php
$this->breadcrumbs=array(
	'Donantes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar Donante', 'url'=>array('index')),
	array('label'=>'Administrar Donante', 'url'=>array('admin')),
);
?>

<h1>Crear Donante</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>