<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idDonante')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idDonante), array('view', 'id'=>$data->idDonante)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('website')); ?>:</b>
	<?php echo CHtml::encode($data->website); ?>
	<br />


</div>