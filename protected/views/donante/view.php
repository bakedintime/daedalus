<?php
$this->breadcrumbs=array(
	'Donantes'=>array('index'),
	$model->idDonante,
);

$this->menu=array(
	array('label'=>'Listar Donante', 'url'=>array('index')),
	array('label'=>'Crear Donante', 'url'=>array('create')),
	array('label'=>'Update Donante', 'url'=>array('update', 'id'=>$model->idDonante)),
	array('label'=>'Delete Donante', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idDonante),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Donante', 'url'=>array('admin')),
);
?>

<h1>View Donante #<?php echo $model->idDonante; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idDonante',
		'nombre',
		'website',
	),
)); ?>
