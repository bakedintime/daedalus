<?php
$this->breadcrumbs=array(
	'Donantes'=>array('index'),
	$model->idDonante=>array('view','id'=>$model->idDonante),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Donante', 'url'=>array('index')),
	array('label'=>'Crear Donante', 'url'=>array('create')),
	array('label'=>'View Donante', 'url'=>array('view', 'id'=>$model->idDonante)),
	array('label'=>'Administrar Donante', 'url'=>array('admin')),
);
?>

<h1>Update Donante <?php echo $model->idDonante; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>