<?php
$this->breadcrumbs=array(
	'Donantes',
);

$this->menu=array(
	array('label'=>'Crear Donante', 'url'=>array('create')),
	array('label'=>'Administrar Donante', 'url'=>array('admin')),
);
?>

<h1>Donantes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
