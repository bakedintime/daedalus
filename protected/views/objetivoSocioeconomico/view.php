<?php
$this->breadcrumbs=array(
	'Objetivo Socioeconomicos'=>array('index'),
	$model->idObjetivo_socioeconomico,
);

$this->menu=array(
	array('label'=>'Listar ObjetivoSocioeconomico', 'url'=>array('index')),
	array('label'=>'Crear ObjetivoSocioeconomico', 'url'=>array('create')),
	array('label'=>'Update ObjetivoSocioeconomico', 'url'=>array('update', 'id'=>$model->idObjetivo_socioeconomico)),
	array('label'=>'Delete ObjetivoSocioeconomico', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idObjetivo_socioeconomico),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar ObjetivoSocioeconomico', 'url'=>array('admin')),
);
?>

<h1>View ObjetivoSocioeconomico #<?php echo $model->idObjetivo_socioeconomico; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idObjetivo_socioeconomico',
		'nombre',
	),
)); ?>
