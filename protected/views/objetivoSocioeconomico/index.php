<?php
$this->breadcrumbs=array(
	'Objetivo Socioeconomicos',
);

$this->menu=array(
	array('label'=>'Crear ObjetivoSocioeconomico', 'url'=>array('create')),
	array('label'=>'Administrar ObjetivoSocioeconomico', 'url'=>array('admin')),
);
?>

<h1>Objetivo Socioeconomicos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
