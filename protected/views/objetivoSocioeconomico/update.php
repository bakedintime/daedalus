<?php
$this->breadcrumbs=array(
	'Objetivo Socioeconomicos'=>array('index'),
	$model->idObjetivo_socioeconomico=>array('view','id'=>$model->idObjetivo_socioeconomico),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar ObjetivoSocioeconomico', 'url'=>array('index')),
	array('label'=>'Crear ObjetivoSocioeconomico', 'url'=>array('create')),
	array('label'=>'View ObjetivoSocioeconomico', 'url'=>array('view', 'id'=>$model->idObjetivo_socioeconomico)),
	array('label'=>'Administrar ObjetivoSocioeconomico', 'url'=>array('admin')),
);
?>

<h1>Update ObjetivoSocioeconomico <?php echo $model->idObjetivo_socioeconomico; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>