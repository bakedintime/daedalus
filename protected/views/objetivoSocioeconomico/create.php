<?php
$this->breadcrumbs=array(
	'Objetivo Socioeconomicos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar ObjetivoSocioeconomico', 'url'=>array('index')),
	array('label'=>'Administrar ObjetivoSocioeconomico', 'url'=>array('admin')),
);
?>

<h1>Crear ObjetivoSocioeconomico</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>