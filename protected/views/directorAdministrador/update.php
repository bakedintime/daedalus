<?php
$this->breadcrumbs=array(
	'Director Administradors'=>array('index'),
	$model->idDir_admin=>array('view','id'=>$model->idDir_admin),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar DirectorAdministrador', 'url'=>array('index')),
	array('label'=>'Crear DirectorAdministrador', 'url'=>array('create')),
	array('label'=>'View DirectorAdministrador', 'url'=>array('view', 'id'=>$model->idDir_admin)),
	array('label'=>'Administrar DirectorAdministrador', 'url'=>array('admin')),
);
?>

<h1>Update DirectorAdministrador <?php echo $model->idDir_admin; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>