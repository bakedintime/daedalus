<?php
$this->breadcrumbs=array(
	'Director Administradors'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar DirectorAdministrador', 'url'=>array('index')),
	array('label'=>'Administrar DirectorAdministrador', 'url'=>array('admin')),
);
?>

<h1>Crear DirectorAdministrador</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>