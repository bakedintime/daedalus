<?php
$this->breadcrumbs=array(
	'Director Administradors',
);

$this->menu=array(
	array('label'=>'Crear DirectorAdministrador', 'url'=>array('create')),
	array('label'=>'Administrar DirectorAdministrador', 'url'=>array('admin')),
);
?>

<h1>Director Administradors</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
