<?php
$this->breadcrumbs=array(
	'Director Administradors'=>array('index'),
	$model->idDir_admin,
);

$this->menu=array(
	array('label'=>'Listar DirectorAdministrador', 'url'=>array('index')),
	array('label'=>'Crear DirectorAdministrador', 'url'=>array('create')),
	array('label'=>'Update DirectorAdministrador', 'url'=>array('update', 'id'=>$model->idDir_admin)),
	array('label'=>'Delete DirectorAdministrador', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idDir_admin),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar DirectorAdministrador', 'url'=>array('admin')),
);
?>

<h1>View DirectorAdministrador #<?php echo $model->idDir_admin; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idDir_admin',
		'nombre',
		'apellido',
		'telefono',
		'email',
	),
)); ?>
