<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="language" content="en" />
  <!-- blueprint CSS framework -->
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/screen.css" media="screen, projection" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/print.css" media="print" />
  <!--[if lt IE 8]>
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/ie.css" media="screen, projection" />
  <![endif]-->

  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/font-awesome.min.css" >
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/font-awesome-ie7.min.css" >
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/main.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/js/toastr/toastr-responsive.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/js/toastr/toastr.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/prettyCheckable.css" />
  <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<div class="container" id="page">
  <div id="mainmenu">
    <?php $this->widget('bootstrap.widgets.TbNavbar',array(
      'brand'=>Yii::app()->name,
      'brandUrl'=>array('site/index'),
      'fixed'=>'top',
      'collapse'=>true,
      'items'=>array(
        array(
        'class'=>'bootstrap.widgets.TbMenu',
        'items'=>array(
            array(
              'label'=>'Búsqueda',
              'icon'=>'search',
              'url'=>array('/search/index'),
              'visible'=>!Yii::app()->user->isGuest
            ),
            array(
              'label'=>'Proyectos',
              'icon'=>'calendar',
              'url'=>array('/proyecto/index'),
              'visible'=>!Yii::app()->user->isGuest
            ),
            array(
              'label'=>'Centros',
              'icon'=>'beaker',
              'url'=>'#',//array('/centro/index'),
              'visible'=>!Yii::app()->user->isGuest
            ),
            array(
              'label'=>'Facultades',
              'icon'=>'book',
              'url'=>'#',//array('/facultad/index'),
              'visible'=>!Yii::app()->user->isGuest
            ),
            array('label'=>'Personal',
              'icon'=>'user',
              'url'=>'#',//array('/personal/index'),
              'visible'=>!Yii::app()->user->isGuest
            ),
          ),
        ),
        array(
          'class'=>'bootstrap.widgets.TbMenu',
          'htmlOptions'=>array('class'=>'pull-right'),
          'items'=>array(
            array(
              'label'=>'Login',
              'icon'=>'signin',
              'url'=>array('/site/login'),
              'visible'=>Yii::app()->user->isGuest
            ),
            array(
              'label'=>'Salir ('.Yii::app()->user->name.')',
              'icon'=>'signout',
              'url'=>array('/site/logout'),
              'visible'=>!Yii::app()->user->isGuest
            )
          ),
        ),
      ),
    )); ?>
  </div>
  <div class="jumbotron subhead" id="overview">
    <div class="container">
      <img src="<?php echo Yii::app()->baseUrl; ?>/images/logo.png" alt="logo"/>
      <h1>Instituto de Investigaciones</h1>
      <p class="lead">Intranet para la gestión de proyectos.</p>
    </div>
  </div>
  <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links'=>$this->breadcrumbs,
  )); ?><!-- breadcrumbs -->

  <?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/toastr/toastr.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/prettyCheckable.js');
    $this->widget('bootstrap.widgets.TbAlert', array(
      'block'=>true, // display a larger alert block?
      'fade'=>true, // use transitions?
      'closeText'=>'×', // close link text - if set to false, no close link is displayed
      'alerts'=>array( // configurations per alert type
        'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'×'),
        'info'=>array('block'=>true, 'fade'=>true, 'closeText'=>'×'),
        'warning'=>array('block'=>true, 'fade'=>true, 'closeText'=>'×'),
        'error'=>array('block'=>true, 'fade'=>true, 'closeText'=>'×'),// success, info, warning, error or danger
      ),
    ));
  ?>
  <?php echo $content; ?>
  <hr style="margin-left:auto;margin-right:auto;width:95%;"/>
  <div id="footer">
    Copyright &copy; <?php echo date('Y'); ?><br/>
    All Rights Reserved.<br/>
    <?php echo Yii::powered(); ?>
  </div><!-- footer -->

</div><!-- page -->

</body>
</html>
