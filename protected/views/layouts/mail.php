<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="language" content="en" />














</head>
<html xmlns="http://www.w3.org/1999/xhtml">
  <body style="margin: 0;font-family: 'Helvetica Neue', Arial, sans-serif;font-size: 14px;line-height: 20px;color: #333;background-color: #fff;padding-top: 50px;background: #f0f0f0 url(http://www.aprendocontigo.net/daedalus/images/noise.gif) repeat -70% 0">
<div class="container" id="page" style="width: 940px;margin-right: auto;margin-left: auto;margin-bottom: 15px;position: relative;background-color: #fff;box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25), 0 0 50px rgba(0, 0, 0, 0.1) inset;border-radius: 18px 18px 0 0">
  <div class="jumbotron subhead" id="overview" style="background: url(http://www.aprendocontigo.net/daedalus/images/backlogo.png) repeat 0px 0px;position: relative;padding: 70px 0 0;color: #fff;text-align: center;text-shadow: 0 1px 3px rgba(0, 0, 0, 0.4), 0 0 30px rgba(0, 0, 0, 0.075);-webkit-box-shadow: inset 0 3px 7px rgba(0, 0, 0, 0.2), inset 0 -3px 7px rgba(0, 0, 0, 0.2);-moz-box-shadow: inset 0 3px 7px rgba(0, 0, 0, 0.2), inset 0 -3px 7px rgba(0, 0, 0, 0.2);box-shadow: inset 0 3px 7px rgba(0, 0, 0, 0.2), inset 0 -3px 7px rgba(0, 0, 0, 0.2);-webkit-border-radius: 18px 18px 0 0;-moz-border-radius: 18px 18px 0 0;border-radius: 18px 18px 0 0">
    <div class="container" style="width: 940px;margin-right: auto;margin-left: auto;position: relative;z-index: 2">
      <img src="http://www.aprendocontigo.net/daedalus/images/logo.png" alt="logo" style="max-width: 100%;width: auto;height: auto;vertical-align: middle;border: 0;-ms-interpolation-mode: bicubic" /><h1 style="margin: 10px 0;font-family: inherit;font-weight: bold;line-height: 1;color: inherit;text-rendering: optimizelegibility;font-size: 80px;letter-spacing: -1px">Instituto de Investigaciones</h1>
      <p class="lead" style="margin: 0 0 10px;margin-bottom: 20px;font-size: 21px;font-weight: 200;line-height: 30px">Intranet para la gestión de proyectos.</p>
    </div>
  </div>
  <div id="content" style="padding: 20px">
    <?php echo $content ?>
  </div>  <hr style="margin-left: auto;margin-right: auto;width: 95%;margin: 20px 0;border: 0;border-top: 1px solid #eee;border-bottom: 1px solid #fff" /><div id="footer" style="padding: 10px;margin: 10px 20px;font-size: 0.8em;text-align: center">
    Copyright © 2013<br /> All Rights Reserved.<br /></div>
</div>
</body>
</html>
