<?php $this->beginContent('//layouts/main'); ?>
<div class="container">
	<div class="span11">
		<div id="content">
			<?php echo $content; ?>
		</div><!-- content -->
	</div>
</div>
<?php $this->endContent(); ?>