<?php $this->beginContent('//layouts/main'); ?>
<div class="container">
	<div class="<?php echo (empty($this->menu))?"span2":"span2 well"; ?> style="padding:8px 0;">
		<?php
			$this->widget('bootstrap.widgets.TbMenu', array(
				'type'=>'list',
				'items'=>$this->menu,
			));
		?>
	</div>
	<div class="span9">
		<div id="content">
			<?php echo $content; ?>
		</div><!-- content -->
	</div>
</div>
<?php $this->endContent(); ?>