<?php $this->beginContent('//layouts/main'); ?>
<?php
    Yii::app()->clientScript->registerScript('hideJumbotron',"
        $('.jumbotron').remove();
    ",CClientScript::POS_READY);
?>
<div class="container">
    <div id="content">
        <?php echo $content; ?>
    </div><!-- content -->
</div>
<?php $this->endContent(); ?>