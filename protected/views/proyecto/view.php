<?php
	$this->breadcrumbs=array(
		'Proyectos'=>array('index'),
		$model->nombre_sistema,
	);
	Yii::app()->clientScript->registerScript('readyFunctions', "
    $('.showContent').off('click');
    $('.showContent').on('click', function(e) {
      var button = $(this);
      // Find the next box element in the DOM
      $(this).next('.content-box').slideToggle('fast', function() {
        button.text($(this).is(':visible') ? 'Ocultar' : $(button).attr('data-title'));
      });
      return false;
    });
  ",CClientScript::POS_READY);
?>
<h1>Descripción del Sistema
	<?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
		'buttons'=>array(
		    array('label'=>'Enviar Encuesta', 'url'=>'#','icon'=>'share',
			  'url'=>Yii::app()->getController()->createUrl('enviarEncuesta', array('id'=>$model->idProyecto))),
		    array('label'=>'Editar Información', 'url'=>'#','icon'=>'edit',
			  'url'=>Yii::app()->getController()->createUrl('editOne',array('id'=>$model->idProyecto))),
		),
		'htmlOptions'=>array('class'=>'pull-right'),
	));
	?>
</h1>
<div class="well">
	<h3>Información General</h3>
	<div class="row-fluid">
	  <div class="span2">
		<p><small><span class="muted">Nombre del Sistema:</span></small></p>
		<p class="lead"><?php echo $model->nombre_sistema; ?><p>
		<p><small><span class="muted">Tipo de Proyecto:</span></small></p>
		<p><?php echo is_numeric($model->tipo)?$model->tipoProyecto->descripcion:$model->tipo; ?><p>
	  </div>
	  <div class="span7">
		<p><small><span class="muted">Nombre largo:</span></small></p>
		<p class="text-center"><?php echo $model->nombre_largo; ?><p>
		<p><small><span class="muted">Url Abstract:</span></small></p>
		<p class="text-center"><?php echo $model->url_abstract; ?><p>
		<p><small><span class="muted">Url Informe Final:</span></small></p>
		<p class="text-center"><?php echo $model->url_informe_final; ?><p>
	  </div>
	  <div class="span3">
		<p><small><span class="muted">Permiso:</span></small></p>
		<p class="btn btn-info disabled"><?php echo $model->idPermiso; ?><p>
		<p><small><span class="muted">No. de Conv.:</span></small></p>
		<p class="btn btn-info disabled"><?php echo $model->no_conv; ?><p>
		<p><small><span class="muted">Disciplina Ciéntifica:</span></small></p>
		<p><?php
			if (empty($model->disciplina_cientifica)){
				$model->disciplina_cientifica=7;
			}
			echo $model->disciplinas->nombre;
		?><p>
		<p><small><span class="muted">Objetivo Socioeconómico.:</span></small></p>
		<p><?php
			if (empty($model->objetivo_socioeconomico)){
				$model->objetivo_socioeconomico=14;
			}
			echo $model->objetivosSocio->nombre;
		?><p>
	  </div>
	</div>
	<hr/>
	<div class="row-fluid">
	  <div class="span3">
		<p><small><span class="muted">Inicio de Ejecución:</span></small></p>
		<p class="lead"><?php echo date('d/m/Y', $model->inicio_ejecucion); ?><p>
	  </div>
	  <div class="span3">
		<p><small><span class="muted">Fin de Ejecución:</span></small></p>
		<p class="lead"><?php echo date('d/m/Y', $model->fin_ejecucion); ?><p>
	  </div>
	  <div class="span3">
		<p><small><span class="muted">Fondo Internacional:</span></small></p>
		<p><i class="icon-<?php echo ($model->fondo_internacional)?"ok":"remove"; ?>"></i><p>
	  </div>
	  <div class="span3">
		<p><small><span class="muted">Concluído:</span></small></p>
		<p><i class="icon-<?php echo (strtolower($model->concluido)=='vigente')?"remove":"ok"; ?>"></i><p>
	  </div>
	</div>
	<hr/>
	<?php $this->widget('bootstrap.widgets.TbButton', array(
        'size'=>'small',
        'type'=>'info',
        'buttonType'=>'button',
        'label'=>'Información Financiera',
        'htmlOptions'=>array('class'=>'showContent','data-title'=>'Información Financiera',
                            'style'=>'display: block; width: 100%;')
    )); ?>
    <div class="content-box well" style="display:none;">
      <div class="row-fluid">
		  	<div class="offset2 span4">
				<p><small><span class="muted">Monto Presupuestado:</span></small></p>
				<p class="lead"><?php echo $model->monto_presupuestado; ?><p>
			  </div>
			  <div class="offset2 span4">
				<p><small><span class="muted">Monto Ejecutado:</span></small></p>
				<p class="lead"><?php echo $model->monto_ejecutado; ?><p>
			  </div>
			</div>
			<hr/>
			<div class="row-fluid">
			  <div class="span2">
				<p><small><span class="muted">Salarios:</span></small></p>
				<p class="lead"><?php echo $model->salarios; ?><p>
			  </div>
			  <div class="span2">
				<p><small><span class="muted">Equipo:</span></small></p>
				<p class="lead"><?php echo $model->equipo; ?><p>
			  </div>
			  <div class="span2">
				<p><small><span class="muted">Software:</span></small></p>
				<p class="lead"><?php echo $model->software; ?><p>
			  </div>
			  <div class="span2">
				<p><small><span class="muted">Otros:</span></small></p>
				<p class="lead"><?php echo $model->otros; ?></i><p>
			  </div>
			  <div class="span2">
				<p><small><span class="muted">Overhead:</span></small></p>
				<p class="lead"><?php echo $model->overhead; ?><p>
			  </div>
			  <div class="span2">
				<p><small><span class="muted">Total:</span></small></p>
				<p class="lead"><?php echo $model->salarios+$model->equipo+$model->software+$model->otros+$model->overhead; ?><p>
			  </div>
			</div>
    </div>
	<hr/>
	<h3>Información de Gestión</h3>
	<div class="row-fluid">
	  <div class="span3">
		<p><small><span class="muted">Investigador(a) Principal:</span></small></p>
		<p class="lead"><?php echo $model->investigador0->nombreCompleto; ?><p>
	  </div>
	  <div class="span3">
		<p><small><span class="muted">Centro:</span></small></p>
		<p class="lead"><?php echo $model->centro0->nombre; ?><p>
	  </div>
	  <div class="span3">
		<p><small><span class="muted">Facultad:</span></small></p>
		<p class="lead"><?php echo $model->facultad0->nombre; ?><p>
	  </div>
	  <div class="span3">
		<p><small><span class="muted">Donante:</span></small></p>
		<p class="lead"><?php echo $model->donante0->nombre; ?></i><p>
	  </div>
	</div>
</div>
