<?php
$this->breadcrumbs=array(
	'Proyectos'=>array('index'),
	$model->idProyecto=>array('view','id'=>$model->idProyecto),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Proyecto', 'url'=>array('index')),
	array('label'=>'Crear Proyecto', 'url'=>array('create')),
	array('label'=>'View Proyecto', 'url'=>array('view', 'id'=>$model->idProyecto)),
	array('label'=>'Administrar Proyecto', 'url'=>array('admin')),
);
?>

<h1>Update Proyecto <?php echo $model->idProyecto; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>