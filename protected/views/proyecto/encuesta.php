<?php
	Yii::app()->clientScript->registerScript('procedenciaFondos'," 
		$('#Donante_procedencia').change(function() {
			if ($(this).val()==2){
				$('#paisDonanteDirecto').show('fast');
			}
			else{
				$('#paisDonanteDirecto').hide('fast');
				$('#paisDonanteDirecto').val('');
			}
		});
		$('#donantesIndirectos').off('click','.removerDonanteIndirecto');
		$('#donantesIndirectos').on('click','.removerDonanteIndirecto',function(){
			$(this).parent().parent().remove();
		});
		$('#donantesIndirectos').off('change','select[name=\"Donanteindirecto[procedencia]\"]');
		$('#donantesIndirectos').on('change','select[name=\"Donanteindirecto[procedencia]\"]',function(){
			if ($(this).val()==2){
				$(this).parent().parent().next().show('fast');
			}
			else{
				$(this).parent().parent().next().hide('fast');
				$(this).parent().parent().next().val('');
			}
		});
		$('#agregarDonanteIndirecto').off('click');
		$('#agregarDonanteIndirecto').on('click',function(){
			htmlForm = '<div class=\'well\'>'+".json_encode($donanteForm)."+'</div>';
			$('#donantesIndirectos').append(htmlForm);
		});
		$('.ipopover').popover();
	",CClientScript::POS_READY);
	
	Yii::app()->clientScript->registerScript('palabrasClave',"
		$('.showContent').off('click');
		$('.showContent').on('click', function(e) {
		    var button = $(this);
		    // Find the next box element in the DOM
		    $(this).next('.content-box').slideToggle('fast', function() {
			button.text($(this).is(':visible') ? 'Ocultar' : $(button).attr('data-title'));
		    });
		    return false;
		});
	",CClientScript::POS_READY);
	  
	Yii::app()->clientScript->registerScript('vinculacionHead',"
		function noErrors(errores){
			resultado = true;
			$.each(errores,function(i, error){
				if (!error.hasOwnProperty('label')){
					$.each(error,function(indice, instancia){
						if (instancia.hasOwnProperty('label')){
							resultado = false;
						}
					});
				}
				else{
					resultado = false;
				}
			});
			return resultado;
		}
		function desplegarErrores(errores){
			listaErrores = '<ul>';
			$.each(errores,function(i, error){
				if (!error.hasOwnProperty('label')){
					$.each(error,function(indice, instancia){
						if (instancia.hasOwnProperty('label')){
							if (!$($(i)[indice]).hasClass('error')){
								$($(i)[indice]).addClass('error');
								$($(i)[indice]).parent().append('<label style=\'display:inline;color:red;\'><b> '+instancia.label+'</b> '+instancia.message+'</label>');
								$($(i)[indice]).on('change',function(){
									$($(i)[indice]).removeClass('error').addClass('success');
									$($(i)[indice]).parent().find('label').remove();
								});
							}
							listaErrores += '<li><label><b>'+instancia.label+'</b> '+instancia.message+' | <a id=\'link'+i+'\' page=\''+instancia.page+'\' elem=\''+indice+'\'>Ir a la página</a></label></li>';
						}
						
					});
				}
				else{
					if (!$('#'+i).hasClass('error')){
						$('#'+i).addClass('error');
						$('#'+i).parent().append('<label style=\'display:inline;color:red;\'><b> '+error.label+'</b> '+error.message+'</label>');
						if ($('#s2id_'+i).length==1){// el input utiliza la libreria select2
										// se le asigna el comportamiento de manera distinta
							$('#'+i).prev().find('.select2-choices').bind('DOMNodeInserted', function(event) {
								$('#'+i).removeClass('error').addClass('success');
								$('#'+i).parent().find('label').remove();
							});
						}else{
							$('#'+i).on('change',function(){
								$('#'+i).removeClass('error').addClass('success');
								$('#'+i).parent().find('label').remove();
							});
						}
					}
					listaErrores += '<li><label><b>'+error.label+'</b> '+error.message+' | <a id=\'link'+i+'\' page=\''+error.page+'\'>Ir a la página</a></label></li>';
				}
			});
			listaErrores += '</ul>';
			//attach behaviour to a links
			$('#errorWell').html('<a href=\'#\' style=\'right:-5px\' class=\'close\' data-dismiss=\'alert\'>×</a><p>Se han encontrado los siguientes errores en el formulario:</p><br/>'+listaErrores);
			$.each($('a[id^=\'link\']'),function(i,elem){
				$(elem).off('click').on('click',function(){
					$($('ul.nav-pills li a')[$(this).attr('page')]).click();
					return false;
				});
			});
			$('#errorWell').show('fast');
		}
	",CClientScript::POS_HEAD);
	Yii::app()->clientScript->registerScript('vinculacion'," 
	    $('#agregarInstitucion').off('click');
	    $('#agregarInstitucion').on('click',function(){
		htmlForm = '<div class=\'well\'>'+".json_encode($instForm)."+'</div>';
		$('#instituciones').append(htmlForm);
		$('input[type=\'checkbox\']').prettyCheckable();
	    });
	    $('#instituciones').off('click','.removerInstitucion');
	    $('#instituciones').on('click','.removerInstitucion',function(){
		$(this).parent().parent().remove();
	    });
	    $('#enviarDatos').off('click');
	    $('#enviarDatos').on('click',function(){
		errores={};
		//Procedencia de Fondos
		donanteDirecto = {};
		donanteDirecto['id']=$('#donanteDirecto').attr('data-label');
		donanteDirecto['procedencia']=$('#Donante_procedencia').val();
		donanteDirecto['pais']=$('#Donante_pais').val();
		donanteDirecto['tipo']=$('#Donante_tipo').val();
		
		//validaciones
		errores['Donante_procedencia'] = (donanteDirecto['procedencia']!=0)?false:{label:'Donante Directo - Procedencia',message:'Debe ingresar la procedencia del donante directo.',page:1};
		errores['Donante_tipo'] = (donanteDirecto['tipo']!=0)?false:{label:'Donante Directo - Tipo',message:'Debe ingresar el tipo del donante directo.',page:1};
		errores['Donante_pais'] = (donanteDirecto['procedencia']==2 && donanteDirecto['pais']==0)?{label:'Donante Directo - País',message:'Debe ingresar el país del donante directo.',page:1}:false;
		
		donantesIndirectos = {};
		errores['input[name=\'Donanteindirecto[nombre]\']']={};
		errores['select[name=\'Donanteindirecto[procedencia]\']']={};
		errores['select[name=\'Donanteindirecto[pais]\']'] ={};
		errores['select[name=\'Donanteindirecto[tipo]\']']={};
		$.each($('input[name=\'Donanteindirecto[nombre]\']'),function(i,val){
			donanteIndirecto = {};
			donanteIndirecto['nombre']=$(val).val();
			donanteIndirecto['procedencia']=$($('select[name=\'Donanteindirecto[procedencia]\']')[i]).val();
			donanteIndirecto['pais']=$($('select[name=\'Donanteindirecto[pais]\']')[i]).val();
			donanteIndirecto['tipo']=$($('select[name=\'Donanteindirecto[tipo]\']')[i]).val();
			
			//validaciones
			//guardar {label,mensaje,pagina,indice} if length == 4 donanteIndirecto o Institucion
			errores['input[name=\'Donanteindirecto[nombre]\']'][i] = (donanteIndirecto['nombre']!='')?false:{label:'Donante Indirecto - Nombre',message:'Debe ingresar un nombre valido para el Donante',page:2};
			errores['select[name=\'Donanteindirecto[procedencia]\']'][i] = (donanteIndirecto['procedencia']!=0)?false:{label:'Donante Indirecto - Procedencia',message:'Debe ingresar la procedencia del Donante',page:2};
			errores['select[name=\'Donanteindirecto[pais]\']'][i] = (donanteIndirecto['procedencia']==2 && donanteIndirecto['pais']==0)?{label:'Donante Indirecto - Pais',message:'Debe ingresar un pais valido para el Donante',page:2}:false;
			errores['select[name=\'Donanteindirecto[tipo]\']'][i] = (donanteIndirecto['tipo']!=0)?false:{label:'Donante Indirecto - Tipo',message:'Debe ingresar un tipo valido para el Donante',page:2};
			
			donantesIndirectos[i] = donanteIndirecto;
		});
		
		//Informacion Tecnica
		abstract = $('#Proyecto_url_abstract').val();
		informeFinal = $('#Proyecto_url_informe_final').val();
		referencias = $('#Proyecto_referencias').val();
		
		//validaciones
		errores['Proyecto_url_abstract'] = (abstract!='')?false:{label:'Informacion Tecnica - Url Abstract',message:'Debe ingresar el abstract del proyecto.',page:2};
		
		//Palabras Clave
		tipoProyecto = $('#Proyecto_tipo').val();
		disciplina = $('#Proyecto_disciplina_cientifica').val();
		objetivo = $('#Proyecto_objetivo_socioeconomico').val();
		
		tagsUVG = $('#listaUVG').val();
		tagsCONCYT = $('#listaCONCYT').val();
		tagsOTRAS = $('#listaOTRAS').val();
		
		//validaciones
		errores['Proyecto_tipo'] = (Proyecto_tipo!=0)?false:{label:'Palabras Clave - Tipo Proyecto',message:'Debe seleccionar un tipo de proyecto valido',page:3};
		errores['listaUVG'] = (tagsUVG!='')?false:{label:'Palabras Clave - UVG',message:'Debe ingresar por lo menos una palabra clave de esta lista.',page:3};
		errores['listaCONCYT'] = (tagsCONCYT!='')?false:{label:'Palabras Clave - CONCYT',message:'Debe ingresar por lo menos una palabra clave de esta lista.',page:3};
		errores['listaOTRAS'] = (tagsOTRAS!='')?false:{label:'Palabras Clave - OTRAS',message:'Debe ingresar por lo menos una palabra clave de esta lista.',page:3};
		
		instituciones = {};
		errores['input[name=\'Institucion[nombre]\']']={};
		errores['select[name=\'Institucion[idTipoColaboracion]\']']={};
		$.each($('input[name=\'Institucion[nombre]\']'),function(i,val){
			institucion = {};
			institucion['nombre']=$(val).val();
			institucion['tipoColaboracion']=$($('select[name=\'Institucion[idTipoColaboracion]\']')[i]).val();
			institucion['subcontratada']=$($('input[name=\'subcontratada\']')[i]).prop('checked');
			
			//validaciones
			errores['input[name=\'Institucion[nombre]\']'][i] = (institucion['nombre']!='')?false:{label:'Institucion - Nombre',message:'Debe ingresar un nombre valido para la institucion',page:4};
			errores['select[name=\'Institucion[idTipoColaboracion]\']'][i] = (institucion['tipoColaboracion']!=0)?false:{label:'Institucion - Tipo de Colaboracion',message:'Debe ingresar un tipo de colaboracion valido para la institucion',page:4};

			instituciones[i] = institucion;
		});
		
		proyecto = {};
		proyecto['idProyecto']=".$model->idProyecto.";
		proyecto['donanteDirecto']=donanteDirecto;
		proyecto['donantesIndirectos']=donantesIndirectos;
		proyecto['abstract']=abstract;
		proyecto['informeFinal']=informeFinal;
		proyecto['referencias']=referencias;
		proyecto['tipoProyecto']=tipoProyecto;
		proyecto['disciplina']=disciplina;
		proyecto['objetivo']=objetivo;
		proyecto['tagsUVG']=tagsUVG;
		proyecto['tagsCONCYT']=tagsCONCYT;
		proyecto['tagsOTRAS']=tagsOTRAS;
		proyecto['instituciones']=instituciones;
		
		if (noErrors(errores)){
			//enviar a servidor
			$.ajax({
				type:'POST',
				url:'".Yii::app()->getController()->createUrl('saveEncuesta')."',
				data:{'proyecto':proyecto},
				dataType:'json',
				success:function(response){
					if (response.result=='success'){
						toastr.success(response.message,response.title);
					}else
					{
						toastr.warning(response.message,response.title);
					}
				},
				error:function(xhr, ajaxOptions, thrownError){
					toastr.error('Error al enviar Datos','Por favor intente otra vez.');
				},
				complete:function(){
					window.location.href = '".((Yii::app()->user->isGuest)?Yii::app()->createAbsoluteUrl('site/index'):Yii::app()->createAbsoluteUrl('proyecto/view',array('id'=>$model->idProyecto)))."';
				}
			});
			$('#errorWell').hide('fast');
			
		}
		else{
			//desplegar errores
			desplegarErrores(errores);
		}
	    });
	",CClientScript::POS_READY);	
?>
<h1>Encuesta de Proyecto
	<?php /*$this->widget('bootstrap.widgets.TbButton',array(
		'label' => '¿Esta encuesta tiene algún dato erróneo?',
		'type' => 'primary',
		'htmlOptions'=>array('class'=>'pull-right'),
		));*/
	?>
</h1>
<div id="errorWell" style="display:none;" class="well alert in alert-block fade alert-warning"></div>
<?php
	$this->widget('bootstrap.widgets.TbWizard', array(
	'type' => 'pills',
	'pagerContent' => '<div style="float:right">
		<input type="button" class="btn button-next btn-primary" name="next" value="Próxima" />
		</div>
		<div style="float:left">
			<input type="button" class="btn button-previous btn-primary" name="previous" value="Anterior" />
		</div><br /><br />',
	'options' => array(
		'nextSelector' => '.button-next',
		'previousSelector' => '.button-previous',
		'onTabShow' => 'js:function(tab, navigation, index) {
			if (index == 0){
				$(".button-previous").addClass("fade");
			}else{
				$(".button-previous").removeClass("fade");
			}
			if (index == 4){
				$(".button-next").addClass("fade");
			}else{
				$(".button-next").removeClass("fade");
			}
			var $total = navigation.find("li").length;
			var $current = index+1;
			var $percent = ($current/$total) * 100;
			$("#wizard-bar .bar").css({width:$percent+"%"});
		}',
		'onTabClick' => 'js:function(tab, navigation, index) {
			return false;
		}',
	),
	'tabs' => array(
		array('label' => 'Índice', 'content' =>$indice, 'active' => true),
		array('label' => 'Procedencia de los fondos', 'content' => $procedenciaFondos),
		array('label' => 'Información Técnica', 'content' => $infoTecnica),
    array('label' => 'Categorías y Palabras clave','content'=>$palabrasClave),
    array('label' => 'Vinculación','content'=>$vinculacion),
	),
));
?>
<p>Porcentaje Completado:</p>
<div id="wizard-bar" class="progress progress-striped active">
    <div class="bar"></div>
</div>
