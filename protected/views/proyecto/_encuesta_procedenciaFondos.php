<?php

	/** @var BootActiveForm $form */
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'procedenciaFondos',
		'type'=>'horizontal',
	)); ?>

    <fieldset>
        <legend>Página 1</legend>
   </fieldset>
	<label id="donanteDirecto" data-label="<?php echo $model->donante0->idDonante; ?>">¿El donante directo <small><?php echo $model->donante0->nombre; ?></small> es Nacional o Internacional?</label>
	<?php echo $form->dropDownListRow($donante, 'procedencia',array('Seleccionar...', 'Nacional', 'Internacional')); ?>
	<div id="paisDonanteDirecto" style="display:none;">
		<?php echo $form->dropDownListRow($donante, 'pais',array('Seleccionar...')+Donante::$countries); ?>
	</div>
	<?php
		$tiposDonantes = new TipoDonante();
		echo $form->dropDownListRow($donante, 'tipo',array('Seleccionar...')+$tiposDonantes->getTipos());
	?>
        <hr/>
	<h4>Donantes Indirectos
		<?php echo CHtml::Link('<i class="icon-info-sign"></i>', null, array(
		    'class' => 'ipopover',
		    'data-trigger' => 'click',
		    'data-placement'=>'right',
		    'data-title' => 'Donante Indirecto',
		    'data-html'=>true,
		    'data-content' => '<small>Son todos aquellos donantes que apoyan las investigaciones del centro financiando proyectos de investigación por medio de un intermediario o tercero.</small>',
		))?>
		<?php $this->widget('bootstrap.widgets.TbButton',array(
			'label' => 'Agregar',
			'icon'=>'plus',
			'htmlOptions'=>array('class'=>'pull-right','id'=>'agregarDonanteIndirecto'),
			));
		?>
	</h4>
	<div id='donantesIndirectos'></div>
<?php $this->endWidget(); ?>
