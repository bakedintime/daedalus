<?php /** @var BootActiveForm $form */
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'horizontalForm',
		'type'=>'horizontal',
	)); ?>
    <fieldset>
        <legend>Verificación de Campos</legend>
        <p style="text-align: center">
            Se ha encontrado algunos campos en los proyectos que necesitan ser verificados. Esto es,
            identificar si hay algún nombre que ya esté en la base de datos con alguna variación.
            Por ejemplo, identificar si "Juan Ricardo Morales Gonzáles" es la misma persona que "Juan Morales".
            Seleccione la opción existente que se relaciona al elemento mostrado o indique que
            es un nuevo elemento que debe ser creado:
        </p>
        <div id="listsHtml"></div>
    </fieldset>
   
<?php $this->endWidget(); ?>