<?php
//CVarDumper::dump($projects, 10, true);
if(isset($errors)){
    if($errors > 0){
        //echo CHTML::openTag('div', array('class'=>'errorSummary'));
        echO CHtml::openTag('div', array('class'=>'form')).
        CHtml::openTag('div', array('class'=>'errorSummary')). 
        CHtml::openTag('h3')."Errores encontrados".CHtml::closeTag('h3').
        CHtml::openTag('dl');
        foreach($errors as $idProject=>$projectErrors){
            echo CHtml::openTag('dt')."Proyecto: <span style='color:red'>$idProject</span>:".CHtml::closeTag('dt');
            foreach($projectErrors as $idColumn=>$errors){
                echo CHtml::openTag('dd').
                        "Columna: <span style='color:orange'>$idColumn</span>".
                     CHtml::openTag('ul');
                foreach($errors as $error){
                    echo CHtml::openTag('li').
                            $error.
                    CHtml::closeTag('li');
                }
                echo CHtml::closeTag('ul').
                CHtml::closeTag('dd');
            }
        }
        echo CHtml::closeTag('dl').
        CHtml::closeTag('div').
        CHtml::closeTag('div');
    }
}
if(isset($projects)){
    $projectDataProvider = empty ($projects) ? null : new CArrayDataProvider($projects, array(
        'id' => 'project',
        'keyField'=>'idProyecto',
        'sort' => array(
            'attributes' => array(
                'idProyecto',
                'tipo',
                'idPermiso',
                'no_conv',
                'donante',
                'fondo_internacional',
                'nombre_sistema',
                'nombre_largo',
                'url_abstract',
                'periodo_ejecucion',
                'investigador',
                'dir_admin',
                'centro',
                'concluido',
                'url_informe_final',
                'monto_presupuestado',
                'monto_ejecutado',
                'disciplina_cientifica',
                'objetivo_socioeconomico',
                'salarios',
                'equipo',
                'software',
                'otros'
            ),
        ),
        'pagination' => array(
            'pageSize' => 20,
        ),
    ));

   
    $form=$this->beginWidget('CActiveForm', array(
            'id'=>'proyecto-form',
            'enableAjaxValidation'=>false,
    ));
    echo CHtml::submitButton('Guardar', array('name'=>'Guardar')).
        CHTML::button('Cancelar', array('onClick'=>"location.href='loadfile".(isset($filename)?"?filename=$filename'":"'")));
    $this->endWidget();
}
?>
<div id="contentloaded"></div>