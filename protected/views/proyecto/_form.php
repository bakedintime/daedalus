<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'proyecto-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'idPermiso'); ?>
		<?php echo $form->textField($model,'idPermiso'); ?>
		<?php echo $form->error($model,'idPermiso'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo'); ?>
		<?php echo $form->textField($model,'tipo',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'tipo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'no_conv'); ?>
		<?php echo $form->textField($model,'no_conv',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'no_conv'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'salarios'); ?>
		<?php echo $form->textField($model,'salarios',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'salarios'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'equipo'); ?>
		<?php echo $form->textField($model,'equipo',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'equipo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'software'); ?>
		<?php echo $form->textField($model,'software',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'software'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'otros'); ?>
		<?php echo $form->textField($model,'otros',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'otros'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'disciplina_cientifica'); ?>
		<?php echo $form->textField($model,'disciplina_cientifica',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'disciplina_cientifica'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'url_informe_final'); ?>
		<?php echo $form->textField($model,'url_informe_final',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'url_informe_final'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'monto_presupuestado'); ?>
		<?php echo $form->textField($model,'monto_presupuestado',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'monto_presupuestado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'monto_ejecutado'); ?>
		<?php echo $form->textField($model,'monto_ejecutado',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'monto_ejecutado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fondo_internacional'); ?>
		<?php echo $form->textField($model,'fondo_internacional',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'fondo_internacional'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'concluido'); ?>
		<?php echo $form->textField($model,'concluido'); ?>
		<?php echo $form->error($model,'concluido'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'periodo_ejecucion'); ?>
		<?php echo $form->textField($model,'periodo_ejecucion',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'periodo_ejecucion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'url_abstract'); ?>
		<?php echo $form->textField($model,'url_abstract',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'url_abstract'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'disciplina'); ?>
		<?php echo $form->textField($model,'disciplina'); ?>
		<?php echo $form->error($model,'disciplina'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'investigador'); ?>
		<?php echo $form->textField($model,'investigador'); ?>
		<?php echo $form->error($model,'investigador'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'objetivo_socioeconomico'); ?>
		<?php echo $form->textField($model,'objetivo_socioeconomico'); ?>
		<?php echo $form->error($model,'objetivo_socioeconomico'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dir_admin'); ?>
		<?php echo $form->textField($model,'dir_admin'); ?>
		<?php echo $form->error($model,'dir_admin'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'donante'); ?>
		<?php echo $form->textField($model,'donante'); ?>
		<?php echo $form->error($model,'donante'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->