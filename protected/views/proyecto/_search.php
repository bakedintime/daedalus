<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idProyecto'); ?>
		<?php echo $form->textField($model,'idProyecto'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idPermiso'); ?>
		<?php echo $form->textField($model,'idPermiso'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipo'); ?>
		<?php echo $form->textField($model,'tipo',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'no_conv'); ?>
		<?php echo $form->textField($model,'no_conv',array('size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'salarios'); ?>
		<?php echo $form->textField($model,'salarios',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'equipo'); ?>
		<?php echo $form->textField($model,'equipo',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'software'); ?>
		<?php echo $form->textField($model,'software',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'otros'); ?>
		<?php echo $form->textField($model,'otros',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'disciplina_cientifica'); ?>
		<?php echo $form->textField($model,'disciplina_cientifica',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'url_informe_final'); ?>
		<?php echo $form->textField($model,'url_informe_final',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'monto_presupuestado'); ?>
		<?php echo $form->textField($model,'monto_presupuestado',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'monto_ejecutado'); ?>
		<?php echo $form->textField($model,'monto_ejecutado',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fondo_internacional'); ?>
		<?php echo $form->textField($model,'fondo_internacional',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'concluido'); ?>
		<?php echo $form->textField($model,'concluido'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'periodo_ejecucion'); ?>
		<?php echo $form->textField($model,'periodo_ejecucion',array('size'=>60,'maxlength'=>60)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'url_abstract'); ?>
		<?php echo $form->textField($model,'url_abstract',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'disciplina'); ?>
		<?php echo $form->textField($model,'disciplina'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'investigador'); ?>
		<?php echo $form->textField($model,'investigador'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'objetivo_socioeconomico'); ?>
		<?php echo $form->textField($model,'objetivo_socioeconomico'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dir_admin'); ?>
		<?php echo $form->textField($model,'dir_admin'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'donante'); ?>
		<?php echo $form->textField($model,'donante'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->