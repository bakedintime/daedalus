<p><small><i>Debe cargar un archivo a la vez</i></small></p>
<?php

    Yii::app()->clientScript->registerScript('headFunction',"
        function reattachButtonBehaviour(file){
            $('.sendBestMatch').off('click');
            $('.sendBestMatch').on('click',function(){
                var button = $(this);
                if (button.attr('disabled')=='disabled'){
                    return false;
                }
                button.attr('disabled', 'disabled');
                buttonDiv = this;
                matchings = [];
                proyectoPermiso = button.parent().attr('id');
                bestMatches = [];
                campos = [];
                button.parent().find('.controls select').each(function(index,value){
                    campo = $(this).parent().parent().prev().text();
                    valor = $(this).parent().parent().find('label').text();
                    bestMatch=$(this).children('option').filter(':selected').text();
                    bestMatches[campo]=[valor,bestMatch];
                    campos.push(campo);
                });
                matchings[0]=proyectoPermiso;
                matchings[1]=campos;
                $(campos).each(function(index,value){
                    matchings[2+index]=bestMatches[value];
                });
                $.ajax({
                    type:'POST',
                    dataType:'json',
                    data:{'matches':JSON.stringify(matchings)},
                    url:'".Yii::app()->createUrl('proyecto/saveBestMatches')."',
                    success:function(response){
                        toastr.success(response.message);
                        $.ajax({
                            type:'POST',
                            dataType:'JSON',
                            url:'".Yii::app()->createUrl('proyecto/parseExcel')."',
                            data:{file_name:'".Yii::getPathOfAlias('frontend.www.files').'/excel/'."'+file},
                            success:function(response){
                                if (response.message){
                                    toastr.success('Todos los proyectos han sido cargados exitosamente!');
                                    $('#resultadoArchivo').hide('fast');
                                }
                                else{
                                    $('#listsHtml').html(response.listsHtml);
                                    reattachButtonBehaviour(file);
                                    $.fn.yiiGridView.update('projects_grid')
                                    $('#resultadoArchivo').show('fast');
                                }
                            },
                            error:function(response,xhr, textStatus, errorThrown){
                                toastr.error('Error al parsear el archivo.'+textStatus);
                            },
                        });
                    },
                    error:function(){
                        toastr.error('Error Interno');
                    },
                    complete:function(){
                      button.removeAttr('disabled');
                    },
                });
                return false;
            });
            $('.cancelBestMatch').off('click');
            $('.cancelBestMatch').on('click',function(){
                $(this).parent().remove();
                return false;
            });
        }",CClientScript::POS_HEAD);
    $this->widget('bootstrap.widgets.TbFileUpload', array(
        'url' => $this->createUrl("proyecto/up"),
        'model' => new ExcelFile(),
        'attribute' => 'excelFile',
        'multiple' => false,
        'options' => array(
        'maxFileSize' => 2000000,
        'acceptFileTypes' => 'js:/(\.|\/)(xlsx?)$/i',
        'successCallback'=>'
            file = data.files[0].name;
            if (data.result=="YII_LOGIN_REQUIRED"){
                window.location.href = "'.Yii::app()->createUrl('/site/login').'";
            }
            else {
                toastr.success("Archivo cargado exitosamente!");
                $.ajax({
                   type:"POST",
                   dataType:"json",
                   url:"'.Yii::app()->createUrl('proyecto/parseExcel').'",
                   data:{file_name:"'.Yii::getPathOfAlias('frontend.www.files').'/excel/'.'"+file},
                   success:function(data){
                        if (data.message){
                            toastr.success("Todos los proyectos han sido cargados exitosamente!");
                            $("#resultadoArchivo").hide("fast");
                        }
                        else{
                            $("#listsHtml").html(data.listsHtml);
                            reattachButtonBehaviour(file);
                            $("#resultadoArchivo").show("fast");
                        }
                   },
                   error:function(response){
                       toastr.error("Error al parsear el archivo.");
                   },
               });
           }
        ',
        'errorCallback'=>'
            toastr.error("Error al cargar archivo.");
        ',
    )));
?>
