<?php
    Yii::app()->clientScript->registerScript('identificarMail',"
        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            return re.test(email);
        }
    ",CClientScript::POS_HEAD);
    Yii::app()->clientScript->registerScript('enviaMail',"
        $('#enviaMail').off('click').on('click',function(){
            if ($('#email').val()!='' && validateEmail($('#email').val())){
                $.ajax({
                    type:'POST',
                    dataType: 'json',
                    url:'".Yii::app()->getController()->createUrl('enviarMailEncuesta')."',
                    data:{
                        idProyecto:".$model->idProyecto.",
                        emails:$('#email').val(),
                    },
                    success:function(datas){
                        toastr.success(datas.message, datas.title);
			window.location.href = window.location.href.replace('enviarEncuesta','view');
                    },
                    error:function(){
                        toastr.error('Por favor, intente otra vez','Error Interno');
                    }
                });
            }else{
                toastr.info('Ingrese un email válido','Revisar campos');
            }
        });
        $('#cancelarMail').off('click').on('click',function(){
            window.location.href = window.location.href.replace('enviarEncuesta','view');
        });
    ",CClientScript::POS_READY);

	/** @var BootActiveForm $form */
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'enviarEncuesta',
		'type'=>'horizontal',
	)); ?>

    <fieldset>
        <legend>Enviar Encuesta a Investigador</legend>
    </fieldset>
    <div class="row">
	<div class="well">
        <h2>Datos del Proyecto a enviar</h2>
        <hr/>
            <div class="control-group">
                <label class="control-label"><b>Nombre del Proyecto</b></label>
                <div class="controls">
                    <?php echo $model->nombre_largo; ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><b>Investigador Principal</b></label>
                <div class="controls">
                    <?php echo $model->investigador0->nombreCompleto; ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><b>Centro</b></label>
                <div class="controls">
                    <?php echo $model->centro0->nombre; ?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"><b>Facultad</b></label>
                <div class="controls">
                    <?php echo $model->facultad0->nombre; ?>
                </div>
            </div>
            <hr/>
            <div class="control-group">
                <label class="control-label"><b>Correo Electrónico</b></label>
                <div class="controls">
                    <?php echo CHtml::textField('email','', array('id'=>'email','placeholder'=>'Ingrese el correo del investigador...')); ?>
                </div>
            </div>
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton',array(
                    'label' => 'Enviar encuesta',
                    'icon'=>'ok white',
                    'type' => 'primary',
                    'htmlOptions'=>array('id'=>'enviaMail'),
                    ));
                ?>
                <?php $this->widget('bootstrap.widgets.TbButton',array(
                    'label' => 'Cancelar',
                    'icon'=>'remove',
                    'htmlOptions'=>array('id'=>'cancelarMail'),
                    ));
                ?>
            </div>
	</div>
    </div>

<?php $this->endWidget(); ?>
