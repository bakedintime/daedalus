<?php
  $this->breadcrumbs=array(
    'Proyectos'=>array('index'),
    $model->nombre_sistema,
  );
    Yii::app()->clientScript->registerScript('prettyCheckable',"
        $('input[type=\'checkbox\']').prettyCheckable();
        $('label.checkbox label').remove();
        $('#enviarCambios').off('click').on('click',function(){
            $.ajax({
                url:'".Yii::app()->createUrl('proyecto/update', array('id'=>$model->idProyecto))."',
                type:'POST',
                dataType:'json',
                data:$('#editOneForm').serialize(),
                success:function(response){
                    if (response.result=='success'){
                        toastr.success(response.message,response.title);
                        window.location.href = window.location.href.replace('editOne','view');
                    }else{
                        toastr.warning(response.message,response.title);
                    }
                },
                error:function(){
                    toastr.error('Por favor intente de nuevo','Error al actualizar los datos');
                }
            });
        });
    ",CClientScript::POS_READY);
?>
<h1>Descripción del Sistema
  <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'buttons'=>array(
        array(
          'label'=>'Cancelar Edición',
          'url'=>Yii::app()->getController()->createUrl('view', array('id'=>$model->idProyecto)),
          'icon'=>'remove'
        ),
    ),
    'htmlOptions'=>array('class'=>'pull-right'),
  ));
  ?>
</h1>
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'editOneForm',
    'htmlOptions'=>array('class'=>'well'),
    )); ?>
  <h3>Información General</h3>
  <div class="row-fluid">
    <div class="span3">
    <p><small><span class="muted">Nombre del Sistema:</span></small></p>
    <p class="lead"><?php echo $form->textFieldRow($model,'nombre_sistema',array('labelOptions'=>array('label'=>''))); ?><p>
    <p><small><span class="muted">Tipo de Proyecto:</span></small></p>
    <p><?php echo $form->dropDownListRow($model,'tipo',TipoProyecto::model()->getTipos(),array('labelOptions'=>array('label'=>''))); ?><p>
    </div>
    <div class="span6">
    <p><small><span class="muted">Nombre largo:</span></small></p>
    <p class="text-center">
      <?php echo $form->textAreaRow($model,'nombre_largo',array('class'=>'span12','labelOptions'=>array('label'=>''))); ?><p>
    <p><small><span class="muted">Url Abstract:</span></small></p>
    <p class="text-center"><?php echo $form->textAreaRow($model,'url_abstract',array('class'=>'span12','labelOptions'=>array('label'=>''))); ?><p>
    <p><small><span class="muted">Url Informe Final:</span></small></p>
    <p class="text-center"><?php echo $form->textAreaRow($model,'url_informe_final',array('class'=>'span12','labelOptions'=>array('label'=>''))); ?><p>
    </div>
    <div class="span3">
    <p><small><span class="muted">Permiso:</span></small></p>
    <p class="btn btn-info disabled"><?php echo $model->idPermiso; ?><p>
    <p><small><span class="muted">No. de Conv.:</span></small></p>
    <p class="btn btn-info disabled"><?php echo $model->no_conv; ?><p>
    <p><small><span class="muted">Disciplina Ciéntifica:</span></small></p>
    <p><?php
      echo $form->dropDownListRow($model,'disciplina_cientifica',Disciplina::model()->getDisciplinas(),array('labelOptions'=>array('label'=>'')));
    ?><p>
    <p><small><span class="muted">Objetivo Socioeconómico.:</span></small></p>
    <p><?php
      echo $form->dropDownListRow($model,'objetivo_socioeconomico',ObjetivoSocioeconomico::model()->getObjetivos(),array('labelOptions'=>array('label'=>'')));
    ?><p>
    </div>
  </div>
  <hr/>
  <div class="row-fluid">
    <div class="span3">
    <p><small><span class="muted">Inicio de Ejecución:</span></small></p>
    <p class="lead"><?php
    echo $form->datePickerRow($model, 'inicio_ejecucion',array('prepend'=>'<i class="icon-calendar"></i>',
                                                               'value'=>date('d/m/Y',$model->inicio_ejecucion),
                                                               'options'=>array('format' => 'dd/mm/yyyy' , 'weekStart'=> 1),
                                                              'labelOptions'=>array('label'=>' ')));
    ?><p>
    </div>
    <div class="span3">
    <p><small><span class="muted">Fin de Ejecución:</span></small></p>
    <p class="lead"><?php
        echo $form->datePickerRow($model, 'fin_ejecucion',array('prepend'=>'<i class="icon-calendar"></i>',
                                                                'value'=>date('d/m/Y',$model->fin_ejecucion),
                                                                'options'=>array('format' => 'dd/mm/yyyy' , 'weekStart'=> 1),
                                                               'labelOptions'=>array('label'=>' ')));
    ?><p>
    </div>
    <div class="span3">
    <p><small><span class="muted">Fondo Internacional:</span></small></p>
    <p><?php echo $form->checkBoxRow($model,'fondo_internacional',array('labelOptions'=>array('label'=>''))); ?><p>
    </div>
    <div class="span3">
    <p><small><span class="muted">Concluído:</span></small></p>
    <p>
    <?php
      $checkBoxValue = (strtolower($model->concluido)=='vigente')?0:1;
      if ($checkBoxValue){
        echo $form->checkBoxRow(
          $model,
          'concluido',
          array(
            'labelOptions'=>array('label'=>''),
            'checked'=>true
          )
        );
      }else{
        echo $form->checkBoxRow(
          $model,
          'concluido',
          array(
            'labelOptions'=>array('label'=>'')
          )
        );
      }
    ?><p>
    </div>
  </div>
  <hr/>
  <?php $this->widget('bootstrap.widgets.TbButton', array(
        'size'=>'small',
        'type'=>'info',
        'buttonType'=>'button',
        'label'=>'Información Financiera',
        'htmlOptions'=>array('class'=>'showContent','data-title'=>'Información Financiera',
                            'style'=>'display: block; width: 100%;')
    )); ?>
    <div class="content-box well" style="display:none;">
    <div class="row-fluid">
      <div class="offset2 span4">
      <p><small><span class="muted">Monto Presupuestado:</span></small></p>
      <p class="lead"><?php echo $form->textFieldRow($model,'monto_presupuestado',array('prepend'=>'<i class="icon-money"></i>',
                                                                             'labelOptions'=>array('label'=>' '),
                                                                             'class'=>'span6')); ?><p>
      </div>
      <div class="offset2 span4">
      <p><small><span class="muted">Monto Ejecutado:</span></small></p>
      <p class="lead"><?php echo $form->textFieldRow($model,'monto_ejecutado',array('prepend'=>'<i class="icon-money"></i>',
                                                                             'labelOptions'=>array('label'=>' '),
                                                                             'class'=>'span6')); ?><p>
      </div>
    </div>
    <hr/>
    <div class="row-fluid">
      <div class="span2">
      <p><small><span class="muted">Salarios:</span></small></p>
      <p class="lead"><?php echo $form->textFieldRow($model,'salarios',array('prepend'=>'<i class="icon-money"></i>',
                                                                             'labelOptions'=>array('label'=>' '),
                                                                             'class'=>'span6')); ?><p>
      </div>
      <div class="span2">
      <p><small><span class="muted">Equipo:</span></small></p>
      <p class="lead"><?php echo $form->textFieldRow($model,'equipo',array('prepend'=>'<i class="icon-money"></i>',
                                                                             'labelOptions'=>array('label'=>' '),
                                                                              'class'=>'span6')); ?><p>
      </div>
      <div class="span2">
      <p><small><span class="muted">Software:</span></small></p>
      <p class="lead"><?php echo $form->textFieldRow($model,'software',array('prepend'=>'<i class="icon-money"></i>',
                                                                             'labelOptions'=>array('label'=>' '),
                                                                              'class'=>'span6')); ?><p>
      </div>
      <div class="span2">
      <p><small><span class="muted">Otros:</span></small></p>
      <p class="lead"><?php echo $form->textFieldRow($model,'otros',array('prepend'=>'<i class="icon-money"></i>',
                                                                             'labelOptions'=>array('label'=>' '),
                                                                             'class'=>'span6')); ?></i><p>
      </div>
      <div class="span2">
      <p><small><span class="muted">Overhead:</span></small></p>
      <p class="lead"><?php echo $form->textFieldRow($model,'overhead',array('prepend'=>'<i class="icon-money"></i>',
                                                                             'labelOptions'=>array('label'=>' '),
                                                                             'class'=>'span6')); ?><p>
      </div>
    </div>
  </div>
  <hr/>
  <h3>Información de Gestión</h3>
  <div class="row-fluid">
    <div class="span3">
    <p><small><span class="muted">Investigador(a) Principal:</span></small></p>
    <p class="lead"><?php echo $form->dropDownListRow($model,'investigador',Personal::model()->getInvestigadorList(),array('labelOptions'=>array('label'=>''))); ?><p>
    </div>
    <div class="span3">
    <p><small><span class="muted">Centro:</span></small></p>
    <p class="lead"><?php echo $form->dropDownListRow($model,'centro',Centro::model()->getCentroList(),array('labelOptions'=>array('label'=>''))); ?><p>
    </div>
    <div class="span3">
    <p><small><span class="muted">Facultad:</span></small></p>
    <p class="lead"><?php echo $form->dropDownListRow($model,'dir_admin',Facultad::model()->getFacultadFilters(),array('labelOptions'=>array('label'=>''))); ?><p>
    </div>
    <div class="span3">
    <p><small><span class="muted">Donante:</span></small></p>
    <p class="lead"><?php echo $form->dropDownListRow($model,'donante',Donante::model()->getDonantesList('nombre is not null'),array('labelOptions'=>array('label'=>''))); ?></i><p>
    </div>
  </div>
    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'buttons'=>array(
        array('label'=>'Establecer Cambios','type'=>'primary','htmlOptions'=>array('id'=>'enviarCambios'),'icon'=>'ok'),
    ),
    'htmlOptions'=>array('class'=>'pull-right'),
  )); ?>
    </div>
    <?php $this->endWidget(); ?>
