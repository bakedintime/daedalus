<?php
	/** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'informacionTecnica',
	'type'=>'horizontal',
	'htmlOptions'=>array('enctype' => 'multipart/form-data')
)); ?>
 
    <fieldset>
        <legend>Página 2</legend>
    </fieldset> 
	<?php echo $form->textAreaRow($model, 'url_abstract', array('class'=>'span8', 'rows'=>5)); ?>
        <?php echo $form->textAreaRow($model, 'url_informe_final', array('class'=>'span8', 'rows'=>5)); ?>
        <?php echo $form->textAreaRow($model, 'referencias', array('class'=>'span8', 'rows'=>5)); ?>
<?php $this->endWidget(); ?>