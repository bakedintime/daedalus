<?php
   $this->breadcrumbs=array(
        'Proyectos'=>array('proyecto/index'),
	'Edición de Proyectos',
    );
?>
<h1>Edición de Proyectos</h1>
<p>
    <?php $this->widget('bootstrap.widgets.TbLabel', array(
     'type'=>'info',
     'label'=>'Info',
    ));?> Las columnas editables son: <small>Donante, Facultad, Centro e Investigador</small>.
</p>
<?php
$this->widget('bootstrap.widgets.TbExtendedGridView', array(
    'type'=>'striped bordered',
    'filter'=>Proyecto::model(),
    'ajaxUrl'=>$this->createUrl('proyecto/edit'),
    'dataProvider' => $gridDataProvider,
    'summaryText'=>'Mostrando de {start} hasta {end} de un total de {count} resultado(s)',
    'columns' =>array(
        array('name'=>'tipo',
	      'value'=> '(is_numeric($data->tipo))?TipoProyecto::model()->findByPk($data->tipo)->descripcion:$data->tipo',
               'header'=>'Tipo de Proyecto',
	    'filter'=> array('TODOS'=>'Todos')+TipoProyecto::model()->getTipos(),
            'headerHtmlOptions' => array('style' => 'width:50px'),
            'class'=>'bootstrap.widgets.TbEditableColumn',
            'editable' => array(
                'type' => 'select',
		'url'=> $this->createUrl('proyecto/editAll'),
                'source'  => TipoProyecto::model()->getTipos(),
            )
        ),
        array('name'=>'nombre_largo', 'header'=>'Nombre Largo',
	      'filter'=>false,
	      'value'=>'($data->nombre_largo=="")?$data->nombre_sistema:$data->nombre_largo',
            'headerHtmlOptions' => array('style' => 'width:100px'),
            'htmlOptions' => array('style' => 'width:100px'),
        ),
        array(
            'name'=>'donante',
            'value'=>'$data->donante0->nombre',
            'header'=>'Donante',
	    'filter'=>array('TODOS'=>'Todos')+Donante::model()->getDonantesList('nombre is not null'),
	    'headerHtmlOptions' => array('style' => 'width:50px'),
            'class'=>'bootstrap.widgets.TbEditableColumn',
            'editable' => array(
                'type' => 'select',
		'url'=> $this->createUrl('proyecto/editAll'),
                'source'  => Donante::model()->getDonantesList('nombre is not null'),
            )
        ),
        array(
            'name'=>'dir_admin',
            'value'=>'$data->facultad0->nombre',
            'header'=>'Facultad',
	    'filter'=>array('TODOS'=>'Todos')+Facultad::model()->getFacultadFilters(),
	    'headerHtmlOptions' => array('style' => 'width:50px'),
            'class'=>'bootstrap.widgets.TbEditableColumn',
            'editable' => array(
                'type' => 'select',
		'url'=> $this->createUrl('proyecto/editAll'),
                'source'=>Facultad::model()->getFacultadFilters('nombre is not null'),
            )
        ),
        array(
            'name'=>'centro',
            'value'=>'$data->centro0->nombre',
            'header'=>'Centro',
	    'filter'=>array('TODOS'=>'Todos')+Centro::model()->getCentroList(),
	    'headerHtmlOptions' => array('style' => 'width:50px','class'=>'header-editable'),
            'class'=>'bootstrap.widgets.TbEditableColumn',
            'editable' => array(
                'type' => 'select',
		'url'=> $this->createUrl('proyecto/editAll'),
                'source'=>Centro::model()->getCentroList('nombre is not null'),
            )
        ),
        array(
            'name'=>'investigador',
            'value'=>'$data->investigador0->nombreCompleto',
            'header'=>'Investigador',
	    'filter'=>array('TODOS'=>'Todos')+Personal::model()->getInvestigadorList(),
	    'headerHtmlOptions' => array('style' => 'width:50px'),
            'class'=>'bootstrap.widgets.TbEditableColumn',
            'editable' => array(
                'type' => 'select',
		'url'=> $this->createUrl('proyecto/editAll'),
                'source'=>Personal::model()->getInvestigadorList('nombreCompleto is not null'),
            )
        ),
    )
));
?>
