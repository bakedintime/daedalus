<?php
	Yii::app()->clientScript->registerScript('pC',"
		$('#listaCONCYT').val(".$model->palabrasClaveAsignadas('CONCYT',$model->idProyecto).").trigger('change');
		$('#listaUVG').val(".$model->palabrasClaveAsignadas('UVG',$model->idProyecto).").trigger('change');
		$('#listaOTRAS').val(".$model->palabrasClaveAsignadas('OTRO',$model->idProyecto).").trigger('change');
	",CClientScript::POS_READY);
	

	/** @var BootActiveForm $form */
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'palabrasClave',
		'type'=>'horizontal',
	)); ?>
 
    <fieldset>
        <legend>Página 3</legend>
     </fieldset> 
    <div class="row-fluid">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'size'=>'small',
			'type'=>'info',
			'buttonType'=>'button',
			'label'=>'Mostrar información sobre Tipos de Investigación',
			'htmlOptions'=>array('class'=>'showContent','data-title'=>'Mostrar información sobre Tipos de Investigación',
					    'style'=>'display: block; width: 100%;')
		)); ?>
		<div class="content-box well" style="display:none;">
			<h4>Investigación Básica</h4>
			<p>
				La investigación básica consiste en trabajos experimentales o teóricos que se emprenden fundamentalmente para obtener nuevos
				conocimientos acerca de los fundamentos de fenómenos y hechos observables, sin pensar en darles ninguna aplicación o utilización determinada.
			</p>
			<h4>Investigación Aplicada</h4>
			<p>
				La investigación aplicada consiste también en trabajos originales realizados para adquirir nuevos conocimientos;
				sin embargo, está dirigida fundamentalmente hacia un objetivo práctico específico
			</p>	
			<h4>Desarrollo Experimental</h4>
			<p>
				El desarrollo experimental consiste en trabajos sistemáticos basados en los conocimientos existentes, 
				derivados de la investigación y/o la experiencia práctica, dirigidos a la producción de nuevos materiales, productos o dispositivos;
				al establecimiento de nuevos procesos, sistemas y servicios; o a la mejora sustancial de los ya existentes.
			</p>
		</div>
	</div>
	<br/>
	<div class="row-fluid">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'size'=>'small',
			'type'=>'info',
			'buttonType'=>'button',
			'label'=>'Mostrar información sobre Disciplinas Científicas',
			'htmlOptions'=>array('class'=>'showContent','data-title'=>'Mostrar información sobre Disciplinas Científicas',
					    'style'=>'display: block; width: 100%;')
		)); ?>
		<div class="content-box well" style="display:none;">
			<h4>1. Ciencias Naturales y Exactas</h4>
			<ul>
			<li>1.1 Matemáticas e informática (matemáticas y otras áreas afines; informática y otras disciplinas afines (sólo desarrollo de software; el desarrollo de equipos debe clasificarse en ingeniería))</li>
			<li>1.2 Ciencias físicas (astronomía y ciencias espaciales, física, otras áreas afines)</li>
			<li>1.3 Ciencias químicas (química, otras áreas afines)</li>
			<li>1.4 Ciencias de la tierra y ciencias relacionadas con el medio ambiente (geología, geofísica, mineralogía, geografía física y otras ciencias de la tierra, meteorología y otras ciencias de la atmósfera incluyendo la investigación climática, oceanografía, vulcanología, paleoecología, otras ciencias afines)</li>
			<li>1.5 Ciencias biológicas (biología, botánica, bacteriología, microbiología, zoología, entomología, genética, bioquímica, biofísica, otras disciplinas afines a excepción de ciencias clínicas y veterinarias)</li>
			</ul>
			<h4>2. Ingeniería y Tecnología</h4>
			<ul>
			<li>2.1 Ingeniería civil (ingeniería arquitectónica, ciencia e ingeniería de los edificios, ingeniería de la construcción, ingeniería municipal, ingeniería estructural y otras disciplinas afines)</li>
			<li>2.2 Ingeniería eléctrica, electrónica (ingeniería eléctrica, electrónica, ingeniería de los sistemas de comunicación, ingeniería informática (sólo equipos) y otras disciplinas afines).</li>
			<li>2.3 Otras ciencias de la ingeniería (tales como la ingeniería química, técnicas aeronáuticas y aerospaciales, mecánica, metalurgia e ingeniería de los materiales y las correspondientes subdivisiones especializadas: productos forestales, ciencias aplicadas como geodesia, química industrial, etc.; ciencia y tecnología de producción de alimentos, tecnologías especializadas o áreas interdisciplinarias, por ejemplo, análisis de sistemas, metalurgia, minas, tecnología textil y otras disciplinas afines)</li>
			</ul>
			<h4>3. Ciencias Médicas</h4>
			<ul>
			<li>3.1 Medicina fundamentall (anatomía, citología, fisiología, genética, farmacia, farmacología, toxicología, inmunología e inmunohematología, química clínica, microbiología clínica, patología)</li>
			<li>3.2 Medicina clínica (anestesiología, pediatría, obstetricia y ginecología, medicina interna, cirugía, estomatología, neurología, psiquiatría, radiología, terapéutica, otorrinolaringología, oftalmología)</li>
			<li>3.3 Ciencias de la salud (salud pública, higiene del trabajo, higiene del medio ambiente, enfermería, epidemiología)</li>
			</ul>
			<h4>4. Ciencias Agrícolas</h4>
			<ul>
			<li>4.1 Agricultura, silvicultura, pesca y ciencias afines (agronomía, zootecnia, pesca, silvicultura, horticultura, otras disciplinas afines)</li>
			<li>4.2  Medicina veterinaria</li>
			</ul>
			<h4>5. Ciencias Sociales</h4>
			<ul>
			<li>5.1 Psicología</li>
			<li>5.2 Economía</li>
			<li>5.3 Ciencias de la educación (educación, formación y otras disciplinas afines)</li>
			<li>5.4 Otras ciencias sociales (antropología (social y cultural) y etnología, demografía, geografía (humana, económica y social), planificación urbana y rural, gestión, derecho, lingüística, ciencias políticas, sociología, métodos y organización, ciencias sociales varias y actividades interdisciplinarias, actividades metodológicas e históricas de I+D relacionadas con disciplinas de este grupo. La antropología física, la geografía física y la psicofisiología deben clasificarse normalmente en ciencias exactas y naturales.</li>
			</ul>
			<h4>6. Humanidades</h4>
			<ul>
			<li>6.1 Historia (historia, prehistoria e historia, así como ciencias auxiliares de la historia, tales como la arqueología, la numismática, la paleografía, la genealogía, etc.).</li>
			<li>6.2 Lengua y literatura (lenguas y literaturas antiguas y modernas)</li>
			<li>6.3 Otras humanidades [filosofía (incluyendo la historia de las ciencias y de la técnica), arte, historia del arte, crítica de arte, pintura, escultura, musicología, arte dramático a excepción de "investigaciones" artísticas de cualquier tipo, religión.</li>
			</ul>
			<h4>7. Sin Asignar</h4>
		</div>
	</div>
	<br/>
	<div class="row-fluid">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'size'=>'small',
			'type'=>'info',
			'buttonType'=>'button',
			'label'=>'Mostrar información sobre Objetivos Socioeconómicos',
			'htmlOptions'=>array('class'=>'showContent','data-title'=>'Mostrar información sobre Objetivos Socioeconómicos',
					    'style'=>'display: block; width: 100%;')
		)); ?>
		<div class="content-box well" style="display:none;">
			<h4>1. Exploración y explotación de la tierra</h4>
			<p>
				Abarca la investigación cuyos objetivos estén relacionados con la exploración de la corteza y la cubierta terrestre, los mares,
				los océanos y la atmósfera, y la investigación sobre su explotación. También incluye la investigación climática y meteorológica,
				 la exploración polar (bajo diferente OSE, si es necesario) y la hidrológica. No incluye:
				 <ul>
					<li>La mejora de suelos y el uso del territorio (OSE 2).</li>
					<li>La investigación sobre la contaminación (OSE 3).</li>
					<li>La pesca (OSE 6).</li>
				</ul>
			</p>
			<h4>2. Infraestructuras y ordenación del territorio</h4>
			<p>
				Cubre la investigación sobre infraestructura y desarrollo territorial, incluyendo la investigación sobre construcción de edificios.
				En general, este OSE engloba toda la investigación relativa a la planificación general del suelo.
				Esto incluye la investigación en contra de los efectos dañinos en el urbanismo urbano y rural pero no la investigación de otros tipos de contaminación (OSE 3).
			</p>
			<h4>3. Control y protección del medio ambiente</h4>
			<p>
				Comprende la investigación sobre el control de la contaminación destinada a la identificación y análisis de las fuentes de contaminación y sus causas,
				y todos los contaminantes, incluyendo su dispersión en el medio ambiente y los efectos sobre el hombre, sobre las especies vivas (fauna, flora, microorganismos)
				y la biosfera. Incluye el desarrollo de instalaciones de control para la medición de todo tipo de contaminantes. Lo mismo es válido para la eliminación y prevención
				de todo tipo de contaminantes en todos los tipos de ambientes.
			</p>
			<h4>4. Protección y mejora de la salud humana</h4>
			<p>
				Incluye la investigación destinada a proteger, promocionar y restaurar la salud humana, interpretada en sentido amplio para incluir los aspectos sanitarios de la nutrición
				y de la higiene alimentaria. Cubre desde la medicina preventiva, incluyendo todos los aspectos de los tratamientos médicos y quirúrgicos, tanto para individuos como para grupos
				así como la asistencia hospitalaria y a domicilio, hasta la medicina social, la pediatría y la geriatría.
			</p>
			<h4>5. Producción distribución y utilización racional de la energía</h4>
			<p>
				Cubre la investigación sobre la producción, almacenamiento, transporte, distribución y uso racional de todas las formas de la energía.
				 También incluye la investigación sobre los procesos diseñados para incrementar la eficacia de la producción y la distribución de energía,
				  y el estudio de la conservación de la energía. No incluye:
				<ul>
					<li>La investigación relacionada con prospecciones (OSE 1).</li>
					<li>La investigación de la propulsión de vehículos y motores (OSE 7).</li>
				</ul>
			</p>
			<h4>6. Producción y tecnología agrícola</h4>
			<p>
				Abarca toda investigación sobre la promoción de la agricultura, los bosques, la pesca y la producción de alimentos.
				Incluye: la investigación en fertilizantes químicos, biocidas, control biológico de las plagas y la mecanización de la agricultura;
				la investigación sobre el impacto de las actividades agrícolas y forestales en el medio ambiente; la investigación en el desarrollo
				de la productividad y la tecnología alimentaria. No incluye:
				<ul>
					<li>La investigación para reducir la contaminación (OSE 3).</li>
					<li>La investigación para el desarrollo de las áreas rurales, el proyecto y la construcción de edificios, 
						la mejora de instalaciones rurales de ocio y descanso y el suministro de agua en la agricultura (OSE 2).</li>
					<li>La investigación en medidas energéticas (OSE 5).</li>
					<li>La investigación en la industria alimentaria (OSE 7).</li>
				</ul>
			</p>
			<h4>7. Producción y tecnología industrial</h4>
			<p>
				Cubre la investigación sobre la mejora de la producción y tecnología industrial.
				Incluye la investigación de los productos industriales y sus procesos de fabricación,
				excepto en los casos en que forman una parte integrante de la búsqueda de otros objetivos (por ejemplo, defensa, espacio, energía, agricultura).
			</p>
			<h4>8. Estructuras y relaciones sociales</h4>
			<p>
					Incluye la investigación sobre objetivos sociales, como los analizan en particular las ciencias sociales y las humanidades,
					que no tienen conexiones obvias con otros OSE. Este análisis engloba los aspectos cuantitativos, cualitativos, organizativos
					y prospectivos de los problemas sociales.
			</p>
			<h4>9. Exploración y explotación del espacio</h4>
			<p>
				Cubre toda la investigación civil en el terreno de la tecnología espacial.
				La investigación análoga realizada en el terreno militar se clasifica en el OSE 13.
				Aunque la investigación espacial civil no está en general centrada sobre un objetivo específico,
				con frecuencia sí tiene un fin determinado, como el aumento del conocimiento general (por ejemplo la astronomía),
				o se refiere a aplicaciones especiales (por ejemplo, los satélites de telecomunicaciones).
			</p>
			<h4>10. Investigación no orientada</h4>
			<p>
				Abarca todos los créditos presupuestarios que se asignan a I+D pero que no pueden atribuirse a un objetivo.
				Puede ser útil una distribución suplementaria por disciplinas científicas.
			</p>
			<h4>11. Otra investigación civil</h4>
			<p>
				Cubre la investigación civil que no puede (aún) ser clasificada en una OSE particular.
			</p>
			<h4>12. Defensa</h4>
			<p>
				Abarca la investigación (y el desarrollo) con fines militares. También comprende la investigación básica y
				la investigación nuclear y espacial financiada por los ministerios de defensa. La investigación civil financiada
				por los ministerios de defensa, por ejemplo, en lo relativo a meteorología, telecomunicaciones y sanidad,
				debe clasificarse en los OSE pertinentes.
			</p>
			<h4>13. Otro</h4>
			<h4>15. Indefinido</h4>
		</div>
	</div>
	<br/>
	
	<?php echo $form->dropDownListRow($model, 'tipo',
		array('Seleccionar...')+TipoProyecto::model()->getTipos()); ?>
	<?php echo $form->dropDownListRow($model, 'disciplina_cientifica',array('Seleccionar...')+Disciplina::model()->getDisciplinas()); ?>
	<?php echo $form->dropDownListRow($model, 'objetivo_socioeconomico',array('Seleccionar...')+ObjetivoSocioeconomico::model()->getObjetivos()); ?>
	<hr/>
	<h4>Palabras Clave</h4>
	<br/>
	<label class="control-label" for="listaOTRAS">Palabras Clave <small>UVG</small></label>
	<div class="controls">
		<?php $this->widget('bootstrap.widgets.TbSelect2', array(
			'asDropDownList' => false,
			'name' => 'listaUVG',
			'options' => array(
			    'labelOptions'=>array('label'=>'Lista UVG'),
			    'tags' => 'js:'.Palabrasclave::model()->getTags('UVG'),
			    'placeholder' => 'Ingrese por lo menos una palabra clave de la lista de palabras UVG',
			    'width' => '80%',
			    'tokenSeparators' => array(',', ' '),
		 ))); ?>
	</div>
	<label class="control-label" for="listaCONCYT">Palabras Clave <small>CONCYT</small></label>
	<div class="controls">
		<?php $this->widget('bootstrap.widgets.TbSelect2', array(
			'asDropDownList' => false,
			'name' => 'listaCONCYT',
			'options' => array(
			'value'=>'000',
			    'tags' => 'js:'.Palabrasclave::model()->getTags('CONCYT'),
			    'placeholder' => 'Ingrese por lo menos una palabra clave de la lista de palabras CONCYT',
			    'width' => '80%',
			    'tokenSeparators' => array(',', ' ')
		))); ?>
	</div>
	<label class="control-label" for="listaOTRAS">Palabras Clave <small>OTRAS</small></label>
	<div class="controls">
	<?php $this->widget('bootstrap.widgets.TbSelect2', array(
	    'asDropDownList' => false,
	    'name' => 'listaOTRAS',
	    'options' => array(
		'tags' => 'js:'.Palabrasclave::model()->getTags('OTRO'),
		'placeholder' => 'Ingrese por lo menos una palabra clave',
		'width' => '80%',
		'tokenSeparators' => array(',', ' '),
	))); ?>
	</div>
<?php $this->endWidget(); ?>