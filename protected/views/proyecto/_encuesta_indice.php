<fieldset>
    <legend>Estimado Investigador Principal,</legend>
</fieldset>
	
<div class="row">
	<div class="well span4">
	<p><b>Nombre del Proyecto</b>: <?php echo $model->nombre_largo; ?></p>
	</div>

        <p class="span3" style="text-align: center">
            Se ha generado un nuevo registro de proyecto bajo su supervision en la base de datos del
            Instituto de Investigaciones.  
        </p>
        <p class="span3" style="text-align: center">Atentamente le solicitamos proporcionar informacion adicional
            para ampliar la informacion asociada a su proyecto contestando las siguientes preguntas.</p>
        <p class="span3" style="text-align: center">Sus respuestas podran ser modificadas entrando directamente al sistema de informacion
            utilizando su usuario y clave.</p>
</div>