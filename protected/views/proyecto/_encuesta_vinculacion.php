<?php
    /** @var BootActiveForm $form */
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'institucionesForm',
	    'type'=>'horizontal',
    )); ?>
    <fieldset>
        <legend>Página 4</legend>
   </fieldset>
    <h4>Instituciones
	<?php /*$this->widget('bootstrap.widgets.TbButton', array(
		'label'=>'?',
		'type'=>'info',
		'htmlOptions'=>array('data-title'=>'Donante Indirecto','data-placement'=>'bottom', 'data-content'=>'Definición', 'rel'=>'popover'),
	));*/ ?> 
	<?php $this->widget('bootstrap.widgets.TbButton',array(
		'label' => 'Agregar',
		'icon'=>'plus',
		'htmlOptions'=>array('class'=>'pull-right','id'=>'agregarInstitucion'),
		));
	?>
    </h4>
    <div id="instituciones"></div>
    <div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton',array(
	'label' => 'Finalizar encuesta',
	'icon'=>'ok white',
	'type' => 'primary',
	'htmlOptions'=>array('class'=>'pull-right','id'=>'enviarDatos'),
	));
    ?>
    </div>
<?php $this->endWidget(); ?>
