<?php
$this->breadcrumbs=array(
    'Proyectos',
);
    Yii::app()->clientScript->registerScript('readyFunctions', "
        $('.showContent').off('click');
        $('.showContent').on('click', function(e) {
            var button = $(this);
            // Find the next box element in the DOM
            $(this).next('.content-box').slideToggle('fast', function() {
                button.text($(this).is(':visible') ? 'Ocultar' : $(button).attr('data-title'));
            });
            return false;
        });
    ",CClientScript::POS_READY);
?>
<h1>Vista Previa <small>Proyectos</small>
    <?php $this->widget('bootstrap.widgets.TbButton',array(
	'label' => 'Editar Proyectos',
        'htmlOptions'=>array('class'=>'pull-right'),
        'url'=>Yii::app()->createUrl('proyecto/edit'),
    ));
    ?>
</h1>
<hr/>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'size'=>'small',
        'type'=>'info',
        'buttonType'=>'button',
        'label'=>'Subir Archivos',
        'htmlOptions'=>array('class'=>'showContent','data-title'=>'Subir Archivos',
                            'style'=>'display: block; width: 100%;')
    )); ?>
    <div class="content-box well" style="display:none;">
        <?php echo $this->renderPartial('_uploadView',true,true); ?>
    </div>
    <br/>
<div id="resultadoArchivo" style="display:none;">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
       'size'=>'small',
       'type'=>'info',
       'buttonType'=>'button',
       'label'=>'Revisión del Archivo',
       'htmlOptions'=>array('class'=>'showContent','data-title'=>'Subir Archivos',
                           'style'=>'display: block; width: 100%;')
    )); ?>
    <div class="content-box well" style="display:none;">
        <?php echo $this->renderPartial('projectMatchings',true,true); ?>
    </div>
</div>
<div id="flip-scroll">
<?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'id'=> 'projects_grid',
        'dataProvider' => $dataProvider,
        'filter'=>Proyecto::model(),
        'summaryText'=>'Mostrando de {start} hasta {end} de un total de {count} resultado(s)',
        'template'=>'{summary}{pager}{items}',
        'columns' => array(
            array(
                'header' => 'Tipo de Proyecto',
                'name' => 'tipo',
                'value'=> '(is_numeric($data->tipo))?TipoProyecto::model()->findByPk($data->tipo)->descripcion:$data->tipo',
                'filter'=> array('TODOS'=>'Todos')+TipoProyecto::model()->getTipos(),
                'type' => 'raw'
            ),
            array(
                'header' => 'Nombre Largo',
                'name' => 'nombre_largo',
                'filter'=>false,
                'value'=> 'empty($data["nombre_largo"])?$data["nombre_sistema"]:$data["nombre_largo"]'
            ),
            array(
                'header' => 'Centro',
                'name' => 'centro',
                'filter'=> array('TODOS'=>'Todos')+Centro::model()->getCentroList(),
                'value'=> '$data->centro0->nombre'
            ),
            array(
                'header' => 'Facultad',
                'name' => 'dir_admin',
                'filter'=> array('TODOS'=>'Todos')+Facultad::model()->getFacultadFilters(),
                'value'=> '$data->facultad0->nombre'
            ),
            array(
                'header' => 'Investigador Principal',
                'name' => 'investigador',
                'filter'=> array('TODOS'=>'Todos')+Personal::model()->getInvestigadorList(),
                'value'=> '$data->investigador0->nombreCompleto'
            ),
            array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'template'=>'{view}',
                'buttons'=>array(
                    'view' => array
                    (
                        'icon'=>'chevron-sign-right'
                    ),
                ),
                'viewButtonUrl'=>'Yii::app()->createUrl("proyecto/view",array("id"=>$data->idProyecto))',
            )
        ),
    ));
?>
</div>
