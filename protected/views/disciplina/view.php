<?php
$this->breadcrumbs=array(
	'Disciplinas'=>array('index'),
	$model->idDisciplina,
);

$this->menu=array(
	array('label'=>'Listar Disciplina', 'url'=>array('index')),
	array('label'=>'Crear Disciplina', 'url'=>array('create')),
	array('label'=>'Update Disciplina', 'url'=>array('update', 'id'=>$model->idDisciplina)),
	array('label'=>'Delete Disciplina', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idDisciplina),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Disciplina', 'url'=>array('admin')),
);
?>

<h1>View Disciplina #<?php echo $model->idDisciplina; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idDisciplina',
		'nombre',
	),
)); ?>
