<?php
$this->breadcrumbs=array(
	'Disciplinas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Listar Disciplina', 'url'=>array('index')),
	array('label'=>'Administrar Disciplina', 'url'=>array('admin')),
);
?>

<h1>Crear Disciplina</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>