<?php
$this->breadcrumbs=array(
	'Disciplinas'=>array('index'),
	$model->idDisciplina=>array('view','id'=>$model->idDisciplina),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Disciplina', 'url'=>array('index')),
	array('label'=>'Crear Disciplina', 'url'=>array('create')),
	array('label'=>'View Disciplina', 'url'=>array('view', 'id'=>$model->idDisciplina)),
	array('label'=>'Administrar Disciplina', 'url'=>array('admin')),
);
?>

<h1>Update Disciplina <?php echo $model->idDisciplina; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>