<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idDisciplina')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idDisciplina), array('view', 'id'=>$data->idDisciplina)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />


</div>