<h1 style="margin: 10px 0;font-family: inherit;font-weight: bold;line-height: 40px;color: inherit;text-rendering: optimizelegibility;font-size: 38.5px">Estimado Investigador Principal,</h1>
<div class="form">
    <p class="lead" style="text-align: justify;margin: 0 0 10px;margin-bottom: 20px;font-size: 21px;font-weight: 200;line-height: 30px">
        Se ha generado un nuevo registro de proyecto bajo su supervision en la base de datos del
        Instituto de Investigaciones.<br />
        Atentamente le solicitamos proporcionar informacion adicional
        para ampliar la informacion asociada a su proyecto contestando las siguientes preguntas.<br />
        Sus respuestas podran ser modificadas entrando directamente al sistema de informacion
        utilizando su usuario y clave en esta dirección:
    </p>
    <div class="well" style="min-height: 20px;padding: 19px;margin-bottom: 20px;background-color: #f5f5f5;border: 1px solid #e3e3e3;-webkit-border-radius: 4px;-moz-border-radius: 4px;border-radius: 4px;-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05)">
        <p style="margin: 0 0 10px"><b>Nombre del Proyecto</b>:<br /></p><p style="text-align: center;margin: 0 0 10px"><a href="<?php echo Yii::app()->createAbsoluteUrl('proyecto/encuesta',array('id'=>$model->idProyecto)) ?>" style="color: #08c;text-decoration: none">
                <?php echo $model->nombre_largo; ?>
        </a></p>
    </div>
</div>