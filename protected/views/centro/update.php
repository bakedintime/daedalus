<?php
$this->breadcrumbs=array(
	'Centros'=>array('index'),
	$model->idCentro=>array('view','id'=>$model->idCentro),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Centro', 'url'=>array('index')),
	array('label'=>'Crear Centro', 'url'=>array('create')),
	array('label'=>'View Centro', 'url'=>array('view', 'id'=>$model->idCentro)),
	array('label'=>'Administrar Centro', 'url'=>array('admin')),
);
?>

<h1>Update Centro <?php echo $model->idCentro; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>