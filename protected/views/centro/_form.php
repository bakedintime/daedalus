<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'centro-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('size'=>60,'maxlength'=>150)); ?>
		<?php echo $form->error($model,'nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'facultad'); ?>
		<?php #echo $form->textField($model,'facultad'); ?>
		<?php echo $form->dropDownList($model,'facultad', CHtml::listData(facultad::model()->findAll(), 'idFacultad', 'nombre'), array('empty'=>'-- Seleccionar facultad --')); ?>
		<?php echo $form->error($model,'facultad'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->