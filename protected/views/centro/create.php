<?php
$assets = dirname(__FILE__).'/assets';
$baseUrl = Yii::app()->assetManager->publish($assets, false, -1, true);
Yii::app()->clientScript->registerCssFile($baseUrl . '/centro-create.css');

$this->breadcrumbs=array(
	'Centros'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Mostrar Centros', 'url'=>array('index')),
	array('label'=>'Administrar Centros', 'url'=>array('admin')),
	array('label'=>'Crear director de Centro', 'url'=>array('directorCentro/create')),	
	array('label'=>'Mostrar directores de centro', 'url'=>array('directoCentro/index')),
	array('label'=>'Administrar directores de centro', 'url'=>array('directorCentro/admin')),	
);
?>

<h1>Crear Centro</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>