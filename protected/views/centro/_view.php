<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCentro')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idCentro), array('view', 'id'=>$data->idCentro)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('facultad')); ?>:</b>
	<?php echo CHtml::encode($data->facultad); ?>
	<br />


</div>