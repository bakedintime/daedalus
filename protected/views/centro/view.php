<?php
$this->breadcrumbs=array(
	'Centros'=>array('index'),
	$model->idCentro,
);

$this->menu=array(
	array('label'=>'Listar Centro', 'url'=>array('index')),
	array('label'=>'Crear Centro', 'url'=>array('create')),
	array('label'=>'Update Centro', 'url'=>array('update', 'id'=>$model->idCentro)),
	array('label'=>'Delete Centro', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idCentro),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Centro', 'url'=>array('admin')),
);
?>

<h1>View Centro #<?php echo $model->idCentro; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idCentro',
		'nombre',
		'facultad',
	),
)); ?>
