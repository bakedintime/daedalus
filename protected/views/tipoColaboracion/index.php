<?php
/* @var $this TipoColaboracionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tipo Colaboracions',
);

$this->menu=array(
	array('label'=>'Create TipoColaboracion', 'url'=>array('create')),
	array('label'=>'Manage TipoColaboracion', 'url'=>array('admin')),
);
?>

<h1>Tipo Colaboracions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
