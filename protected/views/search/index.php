<?php
$this->breadcrumbs=array(
    'Búsqueda',
);
Yii::app()->clientScript->registerScript('headFunctions',"
  function validFilters(filtros){
      if (filtros.fechaI=='' && filtros.fechaF=='' && $.isEmptyObject(filtros.investigadores)
    && (filtros.idSistema == undefined || filtros.idSistema=='') && $.isEmptyObject(filtros.palabrasClave)){
    if ($.isEmptyObject(filtros.facultad) && $.isEmptyObject(filtros.centro) &&
        $.isEmptyObject(filtros.disciplina) && $.isEmptyObject(filtros.objetivo))
    {
        return true;
    }
    else{
        return false;
    }
      }else{
    return false;
      }
  }
    ",CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScript('readyFunctions',"
    $('.content-box').hide();
    $('#showContent').off('click');
    $('#showContent').on('click', function(e) {
  // This is only needed if your using an anchor to target the box elements
  e.preventDefault();
  var button = $(this);
  // Find the next box element in the DOM
  $(this).parents('.btn-group').next('.content-box').slideToggle('fast', function() {
      button.text($(this).is(':visible') ? 'Ocultar' : 'Mostrar más filtros');
  });
  return false;
    });
    $('#limpiar').off('click');
    $('#limpiar').on('click',function(){
  $('#Proyecto_nombre_sistema').val('');
  $('#Proyecto_nombre_sistema').removeAttr('attributeId');
  $('#s2id_investigador .select2-search-choice-close').click();
  $('#resultado').hide('fast');
    });
    $('#search').off('click');
    $('#search').on('click',function(){
  resultadoFiltros = {};
  resultadoFiltros.fechaI = $('#inicio_ejecucion').val();
  resultadoFiltros.fechaF = $('#fin_ejecucion').val();
  resultadoFiltros.facultad = $('input:radio[name=\'Proyecto[dir_admin]\']:checked').val();
  resultadoFiltros.centro = $('input:radio[name=\'Proyecto[centro]\']:checked').val();
  resultadoFiltros.disciplina = $('input:radio[name=\'Proyecto[disciplina]\']:checked').val();
  resultadoFiltros.objetivo = $('input:radio[name=\'Proyecto[objetivo_socioeconomico]\']:checked').val();
  resultadoFiltros.investigadores = $('#investigador').val();
  resultadoFiltros.palabrasClave = $('#palabrasClave').val();
  resultadoFiltros.idSistema = $('#Proyecto_nombre_sistema').attr('attributeId');
  if (validFilters(resultadoFiltros)){
      toastr.warning('Error de Búsqueda','Debe seleccionar algún filtro.');
  }
  else{
      $.ajax({
    type:'POST',
    dataType:'json',
    url:'".Yii::app()->getController()->createUrl('search')."',
    data:{filtros:resultadoFiltros},
    beforeSend:function(){
        $('.progress').show('fast');
    },
    success:function(response){
        if (response.result=='success'){
      $('#resultado').html(response.html);
      $('#resultado').show('fast');
      toastr.success('Búsqueda completa',response.message);
        }else
        {
      toastr.warning('Error de Búsqueda',response.message);
        }
    },
    error:function(){
        $('#resultado').hide('fast');
        toastr.error('Error de Búsqueda','No fue posible encontrar resultados con los filtros seleccionados. Por favor, intente de nuevo');
    },
    complete:function(){
        $('.progress').hide('fast');
    }
      });
  }
  return false;
    });
    $('input[type=\'radio\']').prettyCheckable();
    $('.removeAllFilter').off('click');
    $('.removeAllFilter').on('click',function(){
  $('.removeFilter').click();
    });
    $('.removeFilter,.removeAllFilter').tooltip();
    $('.removeFilter').off('click');
    $('.removeFilter').on('click',function(){
  var id = $(this).parents('.control-label').attr('for');
  console.log(id);
  console.log($('#'+id).attr('type'));
  if ($('#'+id).attr('type')=='text'){
      $('#'+id).val('');
  }
  else {
      $('input[name=\"Proyecto['+id+']\"]').each(function(i,selected){
    console.log(selected);
    if ($(selected).attr('type')=='radio' && $(selected).next('a').hasClass('checked')){
        $(selected).next('a').removeClass('checked');
    }
    $(selected).prop('checked', false);
      });

  }
    });
    $('.removeFilter').css('color','#fff');

      $('.showContent').off('click');
      $('.showContent').on('click', function(e) {
          var button = $(this);
          // Find the next box element in the DOM
          $(this).next('.content-box').slideToggle('fast', function() {});
          return false;
      });
    ",CClientScript::POS_READY);
?>
<h1>Búsqueda de proyectos <small>Criterios de Búsqueda</small></h1>
<hr/>
    <div id="filterOptions">
  <div class="row-fluid">
      <div class="span3 well alert-info">
    <h3>Filtros
        <a class="removeAllFilter" data-title="Limpiar Todos los Filtros" data-placement="right" data-container="body"><i class="icon-remove-sign"></i></a>
    </h3>
    <?php /** @var BootActiveForm $form */
        $model=Proyecto::model();
        $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
          'id'=>'Proyecto-form',
          'action'=>Yii::app()->createUrl('search/index'),
        ));
    ?>
    <div class="control-group ">
        <label class="control-label" for="inicio_ejecucion">Inicio de Ejecución
      <a class="removeFilter pull-right" data-title="Limpiar Filtro" data-placement="right" data-container="body"><i class="icon-remove-sign"></i></a>
        </label>
        <div class="controls">
      <div class="input-prepend">
          <span class="add-on"><i class="icon-calendar"></i></span>
          <?php
        $this->beginWidget('bootstrap.widgets.TbDatePicker', array(
            'model'=>$model,
            'name'=>'inicio_ejecucion',
            'htmlOptions'=>array('placeholder'=>'Seleccionar desde que fecha',
                'style'=>'width:101%'),
        ));
        $this->endWidget();
          ?>
      </div>
        </div>
    </div>
    <div class="control-group ">
        <label class="control-label" for="fin_ejecucion">Fin de Ejecución
      <a class="removeFilter pull-right" data-title="Limpiar Filtro" data-placement="right" data-container="body"><i class="icon-remove-sign"></i></a>
        </label>
        <div class="controls">
      <div class="input-prepend">
          <span class="add-on"><i class="icon-calendar"></i></span>
          <?php
        $this->beginWidget('bootstrap.widgets.TbDatePicker', array(
            'model'=>$model,
            'name'=>'fin_ejecucion',
            'htmlOptions'=>array('placeholder'=>'Seleccionar hasta que fecha',
                 'style'=>'width:101%'),
        ));
        $this->endWidget();
          ?>
      </div>
        </div>
    </div>
    <div class="control-group ">
        <label class="control-label showContent" for="dir_admin">
          Facultad
          <a class="removeFilter pull-right" data-title="Limpiar Filtro" data-placement="right" data-container="body">
            <i class="icon-remove-sign"></i>
          </a>
        </label>
        <div class="controls content-box">
          <?php
            echo $form->radioButtonList(
              $model,
              'dir_admin',
              Facultad::model()->getFacultadFilters(),
              array('labelOptions'=>array('style'=>'display:none;'))
            );
          ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label showContent" for="centro">Centro
      <a class="removeFilter pull-right" data-title="Limpiar Filtro" data-placement="right" data-container="body"><i class="icon-remove-sign"></i></a>
        </label>
        <div class="controls content-box">
      <?php echo $form->radioButtonList($model, 'centro', Centro::model()->getCentroFilters(),array('labelOptions'=>array('style'=>'display:none;'))); ?>
        </div>
    </div>
    <div class="control-group ">
        <label class="control-label showContent" for="disciplina">Disciplina Científica
      <a class="removeFilter pull-right" data-title="Limpiar Filtro" data-placement="right" data-container="body"><i class="icon-remove-sign"></i></a>
        </label>
        <div class="controls content-box">
      <?php echo $form->radioButtonList($model, 'disciplina', Disciplina::model()->getDisciplinaFilters(),array('labelOptions'=>array('style'=>'display:none;'))); ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label showContent" for="objetivo_socioeconomico">Objetivo socioeconómico
      <a class="removeFilter pull-right" data-title="Limpiar Filtro" data-placement="right" data-container="body"><i class="icon-remove-sign"></i></a>
        </label>
        <div class="controls content-box">
      <?php echo $form->radioButtonList($model, 'objetivo_socioeconomico', ObjetivoSocioeconomico::model()->getObjetivoFilters(),array('labelOptions'=>array('style'=>'display:none;'))); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
      </div>
      <div class="span9">
    <div class="row-fluid">
        <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
      'size'=>'small',
      'buttons'=>array(
        array('buttonType'=>'button','label'=>'Limpiar','htmlOptions'=>array('id'=>'limpiar')),
        array('buttonType'=>'button','label'=>'Mostrar más filtros','htmlOptions'=>array('id'=>'showContent')),
        array('buttonType'=>'button','label'=>'Buscar', 'type'=>'primary','icon'=>'search white','htmlOptions'=>array('id'=>'search')),
      ),
      'htmlOptions'=>array('class'=>'pull-right')
        )); ?>
        <div class="content-box well">
      <p><span class="label label-warning">Importante</span> Cuando se utilizan estos filtros se omiten los filtros de la izquierda y viceversa.</p>
      <p><span class="label label-info">Info</span> Puede buscar proyectos que involucran a uno o más investigadores o buscar por el nombre de un proyecto, si utiliza ambos, saldrá el proyecto
      con el nombre seleccionado y los proyectos donde participen los investigadores seleccionados:</p>
      <div class="control-group">
          <label class="control-lbl" for="investigador">Investigador</label>
          <div class="controls">
        <?php $this->widget('bootstrap.widgets.TbSelect2', array(
            'asDropDownList' => false,
            'name' => 'investigador',
            'options' => array(
          'tags' => 'js:'.Personal::model()->getInvestigadorJSON(),
          'placeholder' => 'Investigador(es)',
          'width' => '100%',
          'tokenSeparators' => array(',', ' ')
        ))); ?>
          </div>
      </div>
      <p>ó</p>
      <form>
         <div class="control-group">
            <label class="control-lbl" for="nombre_sistema">Nombre del Sistema <small>Nombre Largo</small></label>
            <div class="controls">
              <div class="input-prepend">
                <span class="add-on"><i class="icon-font"></i></span>
                <?php $this->widget('bootstrap.widgets.TbTypeahead', array(
                'model'=>$model,
                'attribute'=>'nombre_sistema',
                'htmlOptions'=>array(
                    'autocomplete'=>'off',
                    'style'=>'width:420%',
                    'placeholder'=>'Escribir nombre del sistema...'),
                    'options'=>array(
                    'highlighter'=>'js:function(item){
                        var regex = new RegExp(\'(\'+this.query+\')\',"gi");
                        if (item.length>90){
                          return item.replace(regex,"<strong>$1</strong>").substring(0,90)+"...";
                        }
                        else {
                          return item.replace(regex, "<strong>$1</strong>");
                        }
                    }',
                    'name'=>'nombreSistema',
                    'source'=>'js:function(query,process){
                        projects = [];
                        map = {};

                        var data = '.Proyecto::model()->getProjectFilters().';

                        $.each(data, function (i, project) {
                          map[project.nombre] = project;
                          projects.push(project.nombre);
                        });

                        process(projects);
                    }',
                    'items'=>4,
                    'sorter'=>'js:function (items) {
                        return items.sort();
                    }',
                    'updater'=>'js:function (item) {
                        selectedProject = map[item].id;
                        $("#Proyecto_nombre_sistema").attr("attributeId",selectedProject);
                        return item;
                    }'
                  )));?>
              </div>
            </div>
        </div>
      </form>
      <p>ó</p>
      <div class="control-group">
          <label class="control-lbl" for="palabrasClave">Palabras Clave</label>
          <div class="controls">
        <?php $this->widget('bootstrap.widgets.TbSelect2', array(
            'asDropDownList' => false,
            'name' => 'palabrasClave',
            'options' => array(
          'tags' => 'js:'.Palabrasclave::model()->getAllProjectTags(),
          'placeholder' => 'Palabras Clave',
          'width' => '100%',
          'tokenSeparators' => array(',', ' ')
        ))); ?>
          </div>
      </div>
        </div>
          </div>
    <div class="progress progress-info progress-striped active" style="display:none;">
        <div class="bar" style="width: 100%">Cargando...</div>
    </div>
    <div id="resultado" class="well" style="display:none;"></div>
      </div>
  </div>
    </div>
