<?php
if(sizeof($results)>0){
    $projectDataProvider = empty ($results) ? null : new CArrayDataProvider($results, array(
        'id' => 'project',
        'keyField'=>'idProyecto',
        'sort' => array(
            'attributes' => array(
                'idProyecto',
                'tipo',
                //'idPermiso',
                //'no_conv',
                //'donante',
                //'fondo_internacional',
                'nombre_sistema',
                'nombre_largo',
                //'url_abstract',
                'periodo_ejecucion',
                'investigador',
                //'dir_admin',
                'centro',
                //'concluido',
                //'url_informe_final',
                //'monto_presupuestado',
                //'monto_ejecutado',
                'disciplina_cientifica',
                'objetivo_socioeconomico',
                //'salarios',
                //'equipo',
                //'software',
                //'otros'
            ),
        ),
        'pagination' => array(
            'pageSize' => 20,
        ),
    ));

    //CVarDumper::dump($projectDataProvider,10,true);
    $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider' => $projectDataProvider,
        'columns' => array(
            array(
                'header' => 'COD.PB',
                'name' => 'idProyecto',
                'type' => 'raw'
            ),
            array(
                'header' => 'TIPO DE PROYECTO',
                'name' => 'tipo',
                //'type' => 'raw'
                'value'=> 'utf8_encode($data["tipo"])',
                'type' => 'raw'
            ),
            /*array(
                'header' => 'GRANT ID',
                'name' => 'idPermiso'
            ),*/
            /*array(
                'header' => 'NO. CONV.',
                'name' => 'no_conv',
            ),*/
            /*array(
                'header' => 'Nombre completo del DONANTE',
                'name' => 'donante',
                'value'=> 'utf8_encode($data["donante"])',
                'type' => 'raw'
            ),*/
            /*array(
                'header' => 'Fondos Nacionales o Internacionales',
                'name' => 'fondo_internacional',
            ),*/
            array(
                'header' => 'NOMBRE SISTEMA',
                'name' => 'nombre_sistema',
                'value'=> 'utf8_encode($data["nombre_sistema"])',
                'type' => 'raw'
            ),
            array(
                'header' => 'NOMBRE LARGO',
                'name' => 'nombre_largo',
                'value'=> 'utf8_encode($data["nombre_largo"])',
                'type' => 'raw'
            ),
            /*array(
                'header' => 'Vínculo a Abstract/Resumen',
                'name' => 'url_abstract',
            ),*/
            array(
                'header' => 'PERIODO DE EJECUCION',
                'name' => 'periodo_ejecucion'
            ),
            array(
                'header' => 'DIRECTOR INVESTIGADOR PRINCIPAL',
                'name' => 'investigador',
                'value'=> '$data["investigador"]',
                'type' => 'raw'
                //'value' => 'Investigador::model()->findByPK($data["investigador"])->nombre'
            ),
            /*array(
                'header' => 'DIRECCION/FACULTAD',
                'name' => 'dir_admin',
            ),*/
            array(
                'header' => 'CENTRO',
                'name' => 'centro',
                'value'=> 'Centro::model()->findByPk($data["centro"])->nombre',
                'type' => 'raw'
            ),
            /*array(
                'header' => 'Proyecto Vigente o Concluído',
                'name' => 'concluido',
            ),*/
            /*array(
                'header' => 'Vínculo a Informe Final (.pdf)',
                'name' => 'url_informe_final',
            ),*/
            /*array(
                'header' => 'Monto Presupuestado',
                'name' => 'monto_ejecutado',
            ),*/
            array(
                'header' => 'disciplina científica',
                'name' => 'disciplina_cientifica',
                'value'=> 'Disciplina::model()->findByPk($data["disciplina_cientifica"])->nombre',
                'type' => 'raw'
            ),
            array(
                'header' => 'objetivo socioeconomico',
                'name' => 'objetivo_socioeconomico',
                'value'=> 'ObjetivoSocioeconomico::model()->findByPk($data["objetivo_socioeconomico"])->nombre',
                'type' => 'raw'
                //'value'=> 'ObjetivoSocioeconomico::model()->findByPK($data["objetivo_socioeconomico"])->nombre',
            ),
            /*array(
                'header' => 'SALARIOS',
                'name' => 'salarios',
            ),*/
            /*array(
                'header' => 'EQUIPO',
                'name' => 'equipo',
            ),*/
            /*array(
                'header' => 'SOFTWARE',
                'name' => 'software',
            ),*/
            /*array(
                'header' => 'OTROS',
                'name' => 'otros',
            ),*/
        ),
    ));
}else{
    echo CHtml::openTag('div', array('class'=>'errorSummary')).
            CHtml::openTag('h3').'Ningún Proyecto Encontrado'.CHtml::closeTag('h3').
    CHtml::closeTag('div');
}
?>
