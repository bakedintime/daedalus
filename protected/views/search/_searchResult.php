<?php
    $this->widget('bootstrap.widgets.TbGridView', array(
        'dataProvider' => $gridDataProvider,
        'template'=>'{items}',
        'emptyText'=>'No hay resultados para la búsqueda. Intente con una búsqueda distinta.',
        'columns' => array(
            array(
                'header' => 'Tipo de Proyecto','name' => 'tipo',
                'value'=> 'is_numeric($data->tipo)?$data->tipoProyecto->descripcion:$data->tipo','type' => 'raw'
            ),
            array(
                'header' => 'Nombre Largo','name' => 'nombre_largo',
                'value'=> 'empty($data["nombre_largo"])?$data["nombre_sistema"]:$data["nombre_largo"]'
            ),
            array(
                'header' => 'Centro','name' => 'centro',
                'value'=> '$data->centro0->nombre'
            ),
            array(
                'header' => 'Facultad','name' => 'dir_admin',
                'value'=> '$data->facultad0->nombre'
            ),
            array(
                'header' => 'Investigador Principal','name' => 'investigador',
                'value'=> '$data->investigador0->nombreCompleto'
            ),
            array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'template'=>'{view}',
                'buttons'=>array(
                    'view' => array
                    (
                        'icon'=>'chevron-sign-right'
                    ),
                ),
                'viewButtonUrl'=>'Yii::app()->createUrl("proyecto/view",array("id"=>$data->idProyecto))',
            )
        ),
    ));
?>
