<?php
$this->breadcrumbs=array(
	'Personals'=>array('index'),
	$model->idPersonal=>array('view','id'=>$model->idPersonal),
	'Update',
);

$this->menu=array(
	array('label'=>'Listar Personal', 'url'=>array('index')),
	array('label'=>'Crear Personal', 'url'=>array('create')),
	array('label'=>'View Personal', 'url'=>array('view', 'id'=>$model->idPersonal)),
	array('label'=>'Administrar Personal', 'url'=>array('admin')),
);
?>

<h1>Update Personal <?php echo $model->idPersonal; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>