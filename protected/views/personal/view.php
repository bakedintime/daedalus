<?php
$this->breadcrumbs=array(
	'Personals'=>array('index'),
	$model->idPersonal,
);

$this->menu=array(
	array('label'=>'Listar Personal', 'url'=>array('index')),
	array('label'=>'Crear Personal', 'url'=>array('create')),
	array('label'=>'Update Personal', 'url'=>array('update', 'id'=>$model->idPersonal)),
	array('label'=>'Delete Personal', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idPersonal),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar Personal', 'url'=>array('admin')),
);
?>

<h1>View Personal #<?php echo $model->idPersonal; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idPersonal',
		'nombre',
		'apellido',
		'direccion',
		'telefono',
		'email',
		'titulos_universitarios',
		'centro',
	),
)); ?>
