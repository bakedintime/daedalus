<?php $this->pageTitle=Yii::app()->name; ?>

<h1><i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<p><strong>Decanatura del Instituto de Investigaciones</strong> <a href="http://uvg.edu.gt/investigacion/2013_%20Organigrama.pdf" target="_blank">(ver organigrama)</a></p> 
<p>El Instituto de Investigaciones está conformado por los siguientes centros y laboratorios de investigación:</p> 
<h3>ÁREAS TEMÁTICAS DE INVESTIGACIÓN UVG</h3>
<p style="text-align:justify">
  El Instituto de Investigaciones concentra sus actividades alrededor de las siguientes áreas temáticas de investigación: 
  Conservación, estudio y potenciación de la riqueza cultural y natural de Guatemala 
  Desarrollo de procesos y esquemas productivos sostenibles 
  Desarrollo y mejoramiento de los sistemas productivos de alimentos y energía renovables 
  Ecología, epidemiología, diagnóstico y control de enfermedades humanas 
  Estudio, evaluación y capacitación del proceso educativo en Guatemala 
  Estudios para mitigar los problemas ambientales y mantener los procesos ecológicos en función del bienestar humano. 
  Ecología, epidemiologia, diagnóstico y manejo integrado de organismos y entidades asociadas a especies agrícolas y forestales. 
  Desarrollo de infraestructura de alto rendimiento de las Tecnologías de Información y Comunicación 
</p>
<ul> 
  <li><a href="http://uvg.edu.gt/investigacion/CEA-UVG/index.html" target="_top">Centro de Estudios Atitlán (CEA)</a></li> 
  <li ><a href="http://uvg.edu.gt/investigacion/cib/index.html">Centro de Ingeniería Bioquímica (CIB)</a></li> 
  <li ><a href="http://uvg.edu.gt/investigacion/ceb/index.html">Centro de Estudios en Biotecnología (CEB)</a></li> 
  <li ><a href="http://uvg.edu.gt/investigacion/cie/index.html">Centro de Investigaciones Educativas (CIE)</a></li> 
  <li><a href="http://uvg.edu.gt/investigacion/ceab/index.html" target="_top">Centro de Estudios Ambientales y de Biodiversidad (CEAB)</a></li>
    <ul> 
      <li>Área Ambiental</li>
        <ul> 
          <li><a href="http://uvg.edu.gt/investigacion/ceab/cea/index.html" target="_top">Centro de Estudios Ambientales </a></li>
            <ul> 
              <li><a href="http://uvg.edu.gt/investigacion/ceab/cea/cafe/index.html">Proyecto: Cambios Globales y Café</a></li> 
            </ul>
          <li><a href="http://uvg.edu.gt/investigacion/ceab/LabSIG/index.html" target="_blank">Laboratorio de sistemas de Información Geográfica</a></li> 
        </ul>
      <li>Área Biológica
        <ul> 
          <li><a href="http://uvg.edu.gt/investigacion/ceab/herbario/index.html" target="_blank">Herbario</a></li> 
          <li><a href="http://uvg.edu.gt/investigacion/ceab/les/index.html" target="_top">Laboratorio de Entomología Sistemática</a></li> 
        </ul> 
      </li> 
      </ul>
  <li><a href="http://uvg.edu.gt/investigacion/ceaf/index.html">Centro de Estudios Agrícolas y Forestales (CEAF)</a></li>
    <ul> 
      <li><a href="http://uvg.edu.gt/investigacion/ceaf/proteccionv/index.html">Laboratorio de Protección Vegetal</a></li> 
      <li><a href="http://uvg.edu.gt/investigacion/ceaf/entomologia/index.html">Laboratorio de Entomología Aplicada</a></li> 
      <li><a href="http://uvg.edu.gt/investigacion/ceaf/agronomia/index.html">Laboratorio de Suelos</a></li> 
    </ul>
  <li><a href="http://uvg.edu.gt/investigacion/cecta/index.html" target="_top">Centro de Estudios en Ciencia y Tecnología de los Alimentos (CECTA)</a></li> 
  <li>Centro de Estudios en Informática Aplicada (CEIA)</li>
    <ul> 
      <li>Internet 2</li> 
    </ul>
  <li><a href="http://uvg.edu.gt/investigacion/ces/index.html" target="_top">Centro de Estudios en Salud (CES)</a></li> 
  <li><a href="http://uvg.edu.gt/investigacion/ciaa/index.html" target="_top">Centro de Investigaciones Arqueológicas y Antropológicas (CIAA)</a></li> 
  <li>Centro de Procesos Industriales (CPI)</li> 
</ul> 
