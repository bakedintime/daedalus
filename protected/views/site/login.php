<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<h1>Login</h1>

<p>Por favor ingrese sus credenciales:</p>

<div class="form well">
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'login-form',
	'enableAjaxValidation'=>true,
)); ?>
	<p class="note">Campos con <span class="required">*</span> son requeridos.</p>

	<div class="row">
		<?php echo $form->textFieldRow($model,'username'); ?>
	</div>
	<div class="row">
		<?php echo $form->passwordFieldRow($model,'password'); ?>
		<p class="hint">
			Hint: puede entrar con los usuarios <tt>demo/demo</tt> o <tt>admin/admin</tt>.
		</p>
	</div>
	<div class="row">
		<?php echo $form->checkBoxRow($model,'rememberMe'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Login'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
