<?php

class SearchController extends Controller
{
  public $layout='//layouts/plain';

  /**
   * @return array action filters
   */
  public function filters()
  {
    return array(
      'accessControl', // perform access control for CRUD operations
    );
  }

  /**
   * Specifies the access control rules.
   * This method is used by the 'accessControl' filter.
   * @return array access control rules
   */
  public function accessRules()
  {
    return array(
      array('allow',  // allow all users to perform 'index' and 'view' actions
        'actions'=>array('index','search'),
        'users'=>array('@'),
      ),
      array('deny',  // deny all users
        'users'=>array('*'),
      ),
    );
  }

  public function actionIndex()
  {
    if (isset($_POST['Proyecto'])){
      $proyecto = $_POST['Proyecto'];
      $proyecto['inicio_ejecucion']=strtotime($proyecto['inicio_ejecucion']);
      $proyecto['fin_ejecucion']=strtotime($proyecto['fin_ejecucion']);
      $gridData=new CActiveDataProvider('Proyecto', array(
        'criteria'=>array(
            'condition'=>'levenshtein(nombre_largo,:nombre_largo)<2 or inicio_ejecucion BETWEEN :inicio_ejecucion AND :fin_ejecucion',
            'params'=>array(
          ':nombre_largo'=>$proyecto['nombre_sistema'],
          ':inicio_ejecucion'=>$proyecto['inicio_ejecucion'],
          ':fin_ejecucion'=>$proyecto['fin_ejecucion'],
             ),
            'order'=>'nombre_largo DESC',
        ),
        'pagination'=>array(
            'pageSize'=>20,
        ),
      ));
    }
    else {
      $gridData=null;
    }
                $this->render('index', array(
      'gridDataProvider'=>$gridData,
                ));
  }

  /**
   *
   */
  public function actionSearch()
  {
    try{
      if (isset($_POST['filtros'])){
        $filtros = $_POST['filtros'];
        if (empty($filtros['investigadores']) && empty($filtros['idSistema']) && empty($filtros['palabrasClave'])){
          $fechaI=strtotime($filtros['fechaI']);
          $fechaF=strtotime($filtros['fechaF']);
          $facultad=empty($filtros['facultad'])?null:$filtros['facultad'];
          $centro=empty($filtros['centro'])?null:$filtros['centro'];
          $objetivo=empty($filtros['objetivo'])?null:$filtros['objetivo'];
          $disciplina=empty($filtros['disciplina'])?null:$filtros['disciplina'];
          $condition=' ';
          if (!empty($fechaI)&&!empty($fechaF)){
            if (empty($fechaI) && !empty($fechaF)){
              $condition = '(fin_ejecucion < :fin_ejecucion)';
            }else if (!empty($fechaI) && empty($fechaF)){
              $condition = '(inicio_ejecucion > :inicio_ejecucion)';
            }
            else{
              $condition = '(inicio_ejecucion BETWEEN :inicio_ejecucion AND :fin_ejecucion)';
            }
          }else{
            $condition='';
          }
          $condition.=(empty($facultad)?'':(empty($condition)?'dir_admin = :facultad':' AND dir_admin = :facultad'));
          $condition.=(empty($centro)?'':(empty($condition)?'centro = :centro':' AND centro = :centro'));
          $condition.=(empty($objetivo)?'':(empty($condition)?'objetivo_socioeconomico = :objetivo':' AND objetivo_socioeconomico = :objetivo'));
          $condition.=(empty($disciplina)?'':(empty($condition)?'disciplina_cientifica = :disciplina':' AND disciplina_cientifica = :disciplina'));
          $params = array();
          if (!empty($fechaI)&&!empty($fechaF)){
            if (empty($fechaI) && !empty($fechaF)){
              $params[':fin_ejecucion']=$fechaF;
            }else if (!empty($fechaI) && empty($fechaF)){
              $params[':inicio_ejecucion']=$fechaI;
            }
            else{
              $params[':inicio_ejecucion']=$fechaI;
              $params[':fin_ejecucion']=$fechaF;
            }
          }
          if (!empty($facultad)){$params[':facultad']=intval($facultad);}
          if (!empty($centro)){$params[':centro']=intval($centro);}
          if (!empty($objetivo)){$params[':objetivo']=intval($objetivo);}
          if (!empty($disciplina)){$params[':disciplina']=intval($disciplina);}
        }
        else {
          $idSistema=empty($filtros['idSistema'])?0:$filtros['idSistema'];
          $palabrasClave=empty($filtros['palabrasClave'])?'':$filtros['palabrasClave'];
          $investigadores=empty($filtros['investigadores'])?'':$filtros['investigadores'];
          $condition = '';
          $condition = (empty($idSistema)?'':' idProyecto = :idProyecto ');
          $condition .=(empty($filtros['investigadores'])?'':(empty($condition)?'investigador in ('.$investigadores.')':' or investigador in ('.$investigadores.') '));
          $condition .=(empty($filtros['palabrasClave'])?'':(empty($condition)?'idProyecto in (select idProyecto from proyecto_palabrasclave where idPalabraClave in ('.$palabrasClave.'))':' or idProyecto in (select idProyecto from proyecto_palabrasclave where idPalabraClave in ('.$palabrasClave.')) '));
          $params = array();
          if (!empty($idSistema)){$params[':idProyecto']=$idSistema;}
        }

        $gridData=new CActiveDataProvider('Proyecto', array(
          'criteria'=>array(
              'condition'=>$condition,
              'params'=>$params,
              'order'=>'nombre_largo DESC',
          ),
          'pagination'=>array(
              'pageSize'=>20,
          ),
        ));
        $renderedView = $this->renderPartial('_searchResult',array('gridDataProvider' => $gridData),true,true);
        $html='<h3>Resultados</h3>'.$renderedView;
        $return = array('result'=>'success',
            'html'=>$html,
            'message'=>'Búsqueda exitosa!');
        echo json_encode($return);
      }
      else{
        $return = array('result'=>'warning',
            'html'=>'',
            'message'=>'Debe ingresar datos en los filtros para realizar la búsqueda');
        echo json_encode($return);
      }
    }
    catch(Exception $e){
      $return = array('result'=>'error',
          'message'=>'Error Interno');
      echo json_encode($return);
    }

  }


}
