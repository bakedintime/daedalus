<?php

class ProyectoController extends Controller
{
  static $mapeoColumnas = array(
    'idProyecto'=>null,
    'idPermiso'=>'grant id',
    'tipo'=>'tipo de proyecto',
    'no_conv'=>'no. conv.',
    'no_conv'=>'no. convenio',
    'donante'=>'nombre completo del donante',
    'fondo_internacional'=>'fondos nacionales o ',
    'fondo_internacional'=>'fondos nacionales o internacionales',
    'nombre_sistema'=>'nombre sistema',
    'nombre_largo'=>'nombre largo',
    'url_abstract'=>'vínculo a abstract/resumen',
    'url_abstract'=>'vinculo a abstract/resumen',
    'inicio_ejecucion'=>'inicio',
    'fin_ejecucion'=>'finalización',
    'fin_ejecucion'=>'finalizacion',
    'investigador'=>'director investigador principal',
    'dir_admin'=>'direccion/ facultad',
    'centro'=>'centro',
    'referencias'=>null,
    'concluido'=>'proyecto vigente o concluído',
    'concluido'=>'proyecto vigente o concluido',
    'url_informe_final'=>'vínculo a informe final (.pdf)',
    'url_informe_final'=>'vinculo a informe final (.pdf)',
    'monto_presupuestado'=>'monto presupuestado',
    'monto_ejecutado'=>'monto ejecutado',
    'disciplina_cientifica'=>'disciplina científica',
    'disciplina_cientifica'=>'disciplina cientifica',
    'disciplina'=>'ejecutado por disciplina',
    'objetivo_socioeconomico'=>'objetivo ',
    'objetivo_socioeconomico'=>'objetivo socioeconomico',
    'salarios'=>'salarios',
    'equipo'=>'equipo',
    'software'=>'software',
    'overhead'=>'overhead',
    'otros'=>'otros',
  );

  /**
   * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
   * using two-column layout. See 'protected/views/layouts/column2.php'.
   */
  public $layout='//layouts/plain';

  /**
   * @return array action filters
   */
  public function filters()
  {
    return array(
      'accessControl', // perform access control for CRUD operations
    );
  }

  /**
   * Specifies the access control rules.
   * This method is used by the 'accessControl' filter.
   * @return array access control rules
   */
  public function accessRules()
  {
    return array(
      array('allow',  // allow all users to perform 'index' and 'view' actions
        'actions'=>array('encuesta','saveEncuesta'),
        'users'=>array('*'),
      ),
      array('allow', // allow authenticated user to perform 'create' and 'update' actions
        'actions'=>array('index','edit','editAll','create','update','multipleCreate','up','search',
             'view','parseExcel','displayUploadMatches','saveBestMatches','getUploadMatches',
             'enviarEncuesta','enviarMailEncuesta',
             'editOne','update'),
        'users'=>array('@'),
      ),
      array('allow', // allow admin user to perform 'admin' and 'delete' actions
        'actions'=>array('admin','delete'),
        'users'=>array('admin'),
      ),
      array('deny',  // deny all users
        'users'=>array('*'),
      ),
    );
  }

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionView($id)
  {
    $dataProvider =new CActiveDataProvider('Proyecto', array(
        'criteria' => array(
      'condition'=>'idProyecto='.$id,
        ),
    ));
    $this->render('view',array(
      'model'=>$this->loadModel($id),
      'dataProvider'=>$dataProvider,
    ));
  }


  public function actionEdit(){
    $model = new Proyecto();
    if (isset($_GET['Proyecto'])){
      $model->attributes=$_GET['Proyecto'];

      $showAll = false;
      $pastFilters = Yii::app()->user->getState('pastFiltersEdit');
      $oldModel = (empty($pastFilters))?new Proyecto():$pastFilters;
      foreach($_GET['Proyecto'] as $field=>$value){
        if ($value=='TODOS'){
          $showAll = true;
          break;
        }
        else{
          if(empty($model->$field) && !empty($oldModel->$field)){
            $model->$field= $oldModel->$field;
          }
        }
      }
      if (!$showAll){
        Yii::app()->user->setState('pastFiltersEdit',$model);
      }
      else{
        $model->unsetAttributes();
        Yii::app()->user->setState('pastFiltersEdit',new Proyecto());
      }
    }
    $this->render('edit',array(
      'gridDataProvider'=>$model->search(),
      'model'=>$model
    ));
  }

  /**
   * Displays a particular model.
   * @param integer $id the ID of the model to be displayed
   */
  public function actionEncuesta($id)
  {
    $this->layout='column1';
    $model = Proyecto::model()->findByPk($id);
    if ($model->encuestaAbierta==1){
      $donante = Donante::model()->findByPk($model->donante);
      /*** Form de los Donantes ***/
      ob_start();
      $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'fondosForm',
        'type'=>'horizontal',
      ));
      $tiposDonantes = new TipoDonante();
      $donanteI = new DonanteIndirecto();

      echo $form->textFieldRow($donanteI,'nombre')."<label>¿El donante indirecto es Nacional o Internacional?</label>";
      echo $form->dropDownListRow($donanteI, 'procedencia',array('Seleccionar...', 'Nacional', 'Internacional'))."<div id=\"paisDonanteDirecto\" style=\"display:none;\">".$form->dropDownListRow($donanteI, 'pais',array('Seleccionar...')+Donante::model()->getPaises())."</div>";
      echo $form->dropDownListRow($donanteI, 'tipo',array('Seleccionar...')+$tiposDonantes->getTipos());
      $this->widget('bootstrap.widgets.TbButton',array(
          'label' => 'Cancelar',
          'icon'=>'remove',
          'htmlOptions'=>array('class'=>'removerDonanteIndirecto'),
        ));
      $donanteIForm = ob_get_contents();
      $this->endWidget();
      ob_end_clean();
      /******/

      /***  Form  de Institucion ***/
      ob_start();
      $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'instForm',
        'type'=>'horizontal',
      ));
      $tiposColaboracion = new TipoColaboracion();
      $institucion = new Institucion();

      echo $form->textFieldRow($institucion,'nombre')."<label>¿Con qué tipo de colaboración aporta?</label>";
      echo $form->dropDownListRow($institucion, 'idTipoColaboracion',array('Seleccionar...')+$tiposColaboracion->getTipos());
      echo '<div class="control-group ">';
          echo '<label class="control-label" for="disciplina">¿La entidad es subcontratada?</label>';
        echo '<div class="controls">';
          echo CHtml::checkBox('subcontratada',false,array());
        echo '</div>';
      echo '</div>';
      echo '<br/>';
      $this->widget('bootstrap.widgets.TbButton',array(
          'label' => 'Cancelar',
          'icon'=>'remove',
          'htmlOptions'=>array('class'=>'removerInstitucion'),
      ));
      $instForm = ob_get_contents();
      $this->endWidget();
      ob_end_clean();
      /*****/

      $procedenciaFondos = $this->renderPartial('_encuesta_procedenciaFondos',array('model'=>$model,'donante'=>$donante),true,true);
      $infoTecnica = $this->renderPartial('_encuesta_informacionTecnica',array('model'=>$model),true,true);
      $palabrasClave = $this->renderPartial('_encuesta_palabrasClave',array('model'=>$model),true,true);
      $indice = $this->renderPartial('_encuesta_indice',array('model'=>$model,'donante'=>$donante),true,true);
      Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/prettyCheckable.js');
      $vinculacion = $this->renderPartial('_encuesta_vinculacion',array(),true,true);

      $this->render('encuesta',array('model'=>$model,
                   'donante'=>$donante,
                   'procedenciaFondos'=>$procedenciaFondos,
                   'donanteForm'=>$donanteIForm,
                   'infoTecnica'=>$infoTecnica,
                   'palabrasClave'=>$palabrasClave,
                   'indice'=>$indice,
                   'vinculacion'=>$vinculacion,
                   'instForm'=>$instForm));
    }
    else{
      Yii::app()->user->setFlash('info', 'La encuesta para este proyecto ya ha sido contestada.<br/> Para modificar la información de este proyecto debe ingresar a la vista de edición dentro del sistema o pedir que se habilite de nuevo la encuesta.');
      $this->redirect(array('site/index'));
    }
  }

  public function actionSaveEncuesta(){
    if (isset($_POST['proyecto'])){
      $proyecto = $_POST['proyecto'];
      $modeloProyecto = Proyecto::model()->findByPk($proyecto['idProyecto']);

      $connection=Yii::app()->db;
      $transaction=$connection->beginTransaction();
      try{
        //donante Directo
        $donanteDirecto = $proyecto['donanteDirecto'];
        $modeloDonante = Donante::model()->find('idDonante=:idDonante',
                  array(':idDonante'=>$donanteDirecto['id']));
        $modeloDonante->pais = $donanteDirecto['pais'];
        $modeloDonante->tipo = $donanteDirecto['tipo'];
        $modeloDonante->procedencia = $donanteDirecto['procedencia'];
        $modeloDonante->save();
        $modeloProyecto->donante = $donanteDirecto['id'];

        $donantesIndirectos = array_key_exists('donantesIndirectos',$proyecto)?$proyecto['donantesIndirectos']:array();
        foreach($donantesIndirectos as $donanteIndirecto){
          $donanteI = new Donanteindirecto();
          $donanteI->nombre = $donanteIndirecto['nombre'];
          $donanteI->pais = ($donanteIndirecto['pais']==0 && $donanteIndirecto['procedencia']==1)?'00':$donanteIndirecto['pais'];
          $donanteI->tipo = $donanteIndirecto['tipo'];
          $donanteI->procedencia = $donanteIndirecto['procedencia'];//1 - nacional; 2 - internacional
          if ($donanteI->save()){
            $donanteIProyecto = new DonanteIndirecto_Proyecto();
            $donanteIProyecto->idProyecto = $proyecto;
            $donanteIProyecto->idDonanteIndirecto = $donanteI->idDonanteIndirecto;
            $donanteIProyecto->save();
          }
        }

        //Datos del proyecto
        $modeloProyecto->url_abstract = $proyecto['abstract'];
        $modeloProyecto->url_informe_final = $proyecto['informeFinal'];
        $modeloProyecto->referencias = $proyecto['referencias'];
        $modeloProyecto->tipo = $proyecto['tipoProyecto'];
        $modeloProyecto->disciplina_cientifica = $proyecto['disciplina'];
        $modeloProyecto->objetivo_socioeconomico = $proyecto['objetivo'];

        //Palabras Clave
        $tagsUVG = explode(',',$proyecto['tagsUVG']);
        foreach($tagsUVG as $tagUVG){
          if (is_numeric($tagUVG)){//ya existe en la BD
            $tagProyecto = new ProyectoPalabrasClave();
            $tagProyecto->idProyecto = $modeloProyecto->idProyecto;
            $tagProyecto->idPalabraClave = $tagUVG;
            $existe = ProyectoPalabrasClave::model()->exists('idProyecto=:idProyecto and idPalabraClave=:idPalabra',
                array(':idProyecto'=> $modeloProyecto->idProyecto,
                      ':idPalabra'=>$tagUVG));
            if (!$existe){
              $tagProyecto->save();
            }
          }// de lo contrario, se ignora debido a que esta lista es estatica
        }
        $tagsCONCYT = explode(',',$proyecto['tagsCONCYT']);
        foreach($tagsCONCYT as $tagCONCYT){
          if (is_numeric($tagCONCYT)){//ya existe en la BD
            $tagProyecto = new ProyectoPalabrasClave();
            $tagProyecto->idProyecto =  $modeloProyecto->idProyecto;
            $tagProyecto->idPalabraClave = $tagCONCYT;
            $existe = ProyectoPalabrasClave::model()->exists('idProyecto=:idProyecto and idPalabraClave=:idPalabra',
                array(':idProyecto'=> $modeloProyecto->idProyecto,
                      ':idPalabra'=>$tagCONCYT));
            if (!$existe){
              $tagProyecto->save();
            }
          }// de lo contrario, se ignora debido a que esta lista es estatica
        }
        $tagsOTRAS = explode(',',$proyecto['tagsOTRAS']);
        foreach($tagsOTRAS as $tagOTRA){
          if (is_numeric($tagOTRA)){//ya existe en la BD
            $tagProyecto = new ProyectoPalabrasClave();
            $tagProyecto->idProyecto =  $modeloProyecto->idProyecto;
            $tagProyecto->idPalabraClave = $tagOTRA;
            $existe = ProyectoPalabrasClave::model()->exists('idProyecto=:idProyecto and idPalabraClave=:idPalabra',
                array(':idProyecto'=> $modeloProyecto->idProyecto,
                      ':idPalabra'=>$tagOTRA));
            if (!$existe){
              $tagProyecto->save();
            }
          }else// de lo contrario se agrega
          {
            $palabraClave = new Palabrasclave();
            $palabraClave->nombre = $tagOTRA;
            $palabraClave->idTipoPalabraClave = TipoPalabraClave::TIPO_OTRA;
            if ($palabraClave->save()){
              $tagProyecto = new ProyectoPalabrasClave();
              $tagProyecto->idProyecto =  $modeloProyecto->idProyecto;
              $tagProyecto->idPalabraClave = $palabraClave->idPalabra;
              $tagProyecto->save();
            }
          }
        }
        $instituciones = array_key_exists('instituciones',$proyecto)?$proyecto['instituciones']:array();
        foreach($instituciones as $institucion){
          $ins = new Institucion();
          $ins->nombre = $institucion['nombre'];
          $ins->idTipoColaboracion = $institucion['tipoColaboracion'];
          $ins->subcontratada = ($institucion['subcontratada']=='true')?1:0;
          if ($ins->save()){
            $institucionProyecto = new InstitucionProyecto();
            $institucionProyecto->idProyecto = $proyecto;
            $institucionProyecto->idInstitucion = $ins->idInstitucion;
            $institucionProyecto->save();
          }
        }
        if ($modeloProyecto->save()){
          $transaction->commit();
          $modeloProyecto->encuestaAbierta = 0;
          $modeloProyecto->save();
          $response = array('title'=>'Éxito',
                'result'=>'success',
                'message'=>'Los datos del proyecto han sido guardados. Gracias por su tiempo.');

        }else{
          $transaction->rollback();
          $response = array('title'=>'Datos incorrectos',
                'result'=>'error',
              'message'=>'Se han enviado datos incorrectos del proyecto, Por favor intente de nuevo.');
        }
      }
      catch(Exception $e){
        $transaction->rollback();
        $response = array('title'=>'Datos incorrectos',
              'result'=>'error',
        'message'=>'Se han enviado datos incorrectos del proyecto, Por favor intente de nuevo.');
      }
    }
    else {
      $response = array('title'=>'Datos incorrectos',
            'result'=>'error',
        'message'=>'Se han enviado datos incorrectos del proyecto, Por favor intente de nuevo.');
    }
    Yii::app()->user->setFlash($response['result'], $response['message']);
    echo json_encode($response);
  }

  /**
   *
   */
  public function actionEnviarEncuesta($id)
  {
    $this->layout='column1';
    $model = Proyecto::model()->findByPk($id);
    $this->render('enviarEncuesta',array('model'=>$model));
  }

  public function actionEnviarMailEncuesta(){
    $mails = $_POST['emails'];
    $proyecto = $_POST['idProyecto'];

    $model = Proyecto::model()->findByPk($proyecto);

    try{
      $mail = new YiiMailer();
      $mail->setView('encuesta');
      $mail->setData(array('model'=>$model));

      $mail->render();
      $mail->From = 'no-reply@institutoinvestigaciones.com';
      $mail->FromName = 'Instituto de Investigaciones - UVG';
      $mail->Subject = 'Usted tiene un proyecto asignado';
      $mailsSeparados = explode(';',$mails);
      $mail->AddAddress($mailsSeparados[0]);

      if ($mail->Send()) {
        $mail->ClearAddresses();
        $model->encuestaAbierta = 1;
        $model->save();
        $response = array('title'=>'Mensaje Enviado',
              'result'=>'success','message'=>'La encuesta ha sido enviada al investigador.');
      } else {
        $response = array('title'=>'Error de Envío',
              'result'=>'error','message'=>'No se ha podido enviar la encuesta'.$mail->ErrorInfo);
      }
    }
    catch(Exception $e){
      $response = array('title'=>'Error Interno',
            'result'=>'error','message'=>$e->getMessage());
    }
    Yii::app()->user->setFlash($response['result'], $response['message']);
    echo json_encode($response);
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionCreate()
  {
    $model=new Proyecto;

    // Uncomment the following line if AJAX validation is needed
    // $this->performAjaxValidation($model);

    if(isset($_POST['Proyecto']))
    {
      $model->attributes=$_POST['Proyecto'];
      if($model->save())
        $this->redirect(array('view','id'=>$model->idProyecto));
    }

                $this->render('create',array(
      'model'=>$model,
    ));
  }

  /**
   * Creates a new model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   */
  public function actionUpdate($id)
  {
    $model=$this->loadModel($id);
    if(isset($_POST['Proyecto']))
    {
      $model->attributes = $_POST['Proyecto'];
      $model->inicio_ejecucion = strtotime($model->inicio_ejecucion);
      $model->fin_ejecucion = strtotime($model->fin_ejecucion);
      if($model->save()){
        $response = array('title'=>'Datos actualizados',
              'result'=>'success',
              'message'=>'Datos actualizados exitosamente');
      }
      else{
        $response = array('title'=>'Error',
              'result'=>'error',
              'message'=>'Error al actualizar datos');
      }
    }else{
      $response = array('title'=>'Datos incorrectos',
              'result'=>'error',
              'message'=>'Faltan datos para actualizar');
    }
    Yii::app()->user->setFlash($response['result'],$response['message']);
    echo json_encode($response);
  }

  public function actionUp()
  {
    try{
      header('Vary: Accept');
      if (isset($_SERVER['HTTP_ACCEPT']) &&
          (strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false))
      {
          header('Content-type: application/json');
      } else {
          header('Content-type: text/plain');
      }
      $data = array();

      $model = new ExcelFile('upload');
      $model->excelFile = CUploadedFile::getInstance($model, 'excelFile');
      if ($model->excelFile !== null  && $model->validate(array('excelFile')))
      {
          if ($model->excelFile->saveAs(Yii::getPathOfAlias('frontend.www.files').'/excel/'.$model->excelFile->name)){
            $model->file_name = $model->excelFile->name;
            $data[] = array(
              'name' => $model->excelFile->name,
              'type' => $model->excelFile->type,
              'size' => $model->excelFile->size,
              'url' => $model->getExcelFileUrl(),
            );
          }
      } else {
          if ($model->hasErrors('excelFile'))
          {
        $data[] = array('error', $model->getErrors('excelFile'));
          } else {
        throw new CHttpException(500, "Could not upload file ".     CHtml::errorSummary($model));
          }
      }
      // JQuery File Upload expects JSON data
      echo json_encode($data);
    }catch(Exception $e){
      echo $e->getMessage();
    }
  }


  public function actionParseExcel(){
    try{
      $file=$_POST['file_name'];
      $reader = PHPExcel_IOFactory::createReaderForFile($file);
      $reader->setReadDataOnly(true);
      $objXLS = $reader->load($file);

      $proyectos=array();
      $mapeoIndices = array();
      $objXLS->setActiveSheetIndex(0);
      $worksheet = $objXLS->getActiveSheet();
      $worksheetTitle= $worksheet->getTitle();
      $highestRow = $worksheet->getHighestRow();
      $highestColumn = $worksheet->getHighestColumn();
      $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
      $correlativeFound = false;
      $lowestRow = 1;
      for ($row = 1;$row <= $highestRow; ++ $row){
        $cell = $worksheet->getCellByColumnAndRow(0, $row);
        $val = $cell->getCalculatedValue();
        if ($val=="CORR."){
          $lowestRow=$row;
          $correlativeFound = true;
          for ($col = 0; $col < $highestColumnIndex; ++ $col) {
            $cell = $worksheet->getCellByColumnAndRow($col, $lowestRow);
            $val = $cell->getCalculatedValue();
            $filteredValue = preg_replace('/(\s+|\n\r+)/', ' ', $val);
            $key = array_search(strtolower($filteredValue),$this::$mapeoColumnas);
            if (!empty($val) && $key){
              $columnName = $this::$mapeoColumnas[$key];
              $mapeoIndices[$columnName]=$col;
            }
          }
        }else if ($correlativeFound){
          for ($col = 0; $col < $highestColumnIndex; ++ $col) {
            $cell = $worksheet->getCellByColumnAndRow($col, $lowestRow+1);
            $cellPrior = $worksheet->getCellByColumnAndRow($col, $lowestRow-1);

            $val = $cell->getCalculatedValue();
            $filteredValue = preg_replace('/(\s+|\n\r+)/', ' ', $val);
            $valPrior = $cellPrior->getCalculatedValue();
            $filteredValuePrior = preg_replace('/(\s+|\n\r+)/',' ',$valPrior);
            $key = array_search(strtolower($filteredValue),$this::$mapeoColumnas);
            $keyPrior = array_search(strtolower($filteredValuePrior),$this::$mapeoColumnas);
            if (!empty($val) && $key){
              $columnName = $this::$mapeoColumnas[$key];
              $mapeoIndices[$columnName]=$col;
            }
            if (!empty($valPrior) && $keyPrior){
              $columnName = $this::$mapeoColumnas[$keyPrior];
              $mapeoIndices[$columnName]=$col;
            }
          }
          break;
        }
      }
      if (!$correlativeFound){
        $response = array('response'=>'error',
            'message'=>'No se pudo encontrar la fila con título "CORR." en la primera columna el documento.');
        echo json_encode($response);
        return 0; //termina la ejecución
      }
      // TODO evaluar si se encontraron las columnas necesarias del documento
      $atributos = Proyecto::model()->getAttributes(true);
      for ($row = $lowestRow+2; $row <= $highestRow; ++ $row) {
        $cell = $worksheet->getCellByColumnAndRow(0, $row);
        $val = $cell->getCalculatedValue();
          if (!empty($val)){
        $p = new Proyecto();
        foreach($atributos as $atributo=>$v){
          if ($this::$mapeoColumnas[$atributo]!=null && array_key_exists($this::$mapeoColumnas[$atributo],$mapeoIndices)){
            $cell = $worksheet->getCellByColumnAndRow($mapeoIndices[$this::$mapeoColumnas[$atributo]], $row);
            $val = $cell->getCalculatedValue();
            $dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
            if (in_array($atributo,array('inicio_ejecucion','fin_ejecucion'))){
              $val = PHPExcel_Shared_Date::ExcelToPHPObject($val);
              $p[$atributo] = $val->getTimestamp();
              if ($p[$atributo] < 0){
                $p[$atributo] = 18000; //01/01/1970
              }

            }
            else{
              $p[$atributo] = $val;
            }
          }
        }
        $alreadyExists = Proyecto::model()->exists(
              'idPermiso=:idPermiso and no_conv=:no_conv and levenshtein(nombre_sistema,:nombre_sistema)<5',
                    array(':idPermiso'=>$p->idPermiso,
                          ':no_conv'=>$p->no_conv,
                          ':nombre_sistema'=>$p->nombre_sistema));
        if (!$alreadyExists){
            array_push($proyectos,$p);
        }else{ //si el proyecto ya existe, solamente actualizar datos
            $proyectoToUpdate = Proyecto::model()->find(
              'idPermiso=:idPermiso and no_conv=:no_conv and levenshtein(nombre_sistema,:nombre_sistema)<5',
                    array(':idPermiso'=>$p->idPermiso,
                          ':no_conv'=>$p->no_conv,
                          ':nombre_sistema'=>$p->nombre_sistema));
            $proyectoToUpdate->url_abstract = $p->url_abstract;
            $proyectoToUpdate->url_informe_final = $p->url_informe_final;
            $proyectoToUpdate->inicio_ejecucion=$p->inicio_ejecucion;
            $proyectoToUpdate->fin_ejecucion=$p->fin_ejecucion;
            $proyectoToUpdate->concluido=$p->concluido;
            $proyectoToUpdate->monto_presupuestado =$p->monto_presupuestado ;
            $proyectoToUpdate->monto_ejecutado =$p->monto_ejecutado ;
            $proyectoToUpdate->disciplina=$p->disciplina;
            $proyectoToUpdate->objetivo_socioeconomico=$p->objetivo_socioeconomico;

            $proyectoToUpdate->salarios=$p->salarios;
            $proyectoToUpdate->equipo=$p->equipo;
            $proyectoToUpdate->software=$p->software;
            $proyectoToUpdate->overhead=$p->overhead;
            $proyectoToUpdate->otros=$p->otros;

            if (Centro::model()->exists('nombre=:nombre',array(':nombre'=>$p->centro))){
              $centroID = Centro::model()->find('nombre=:nombre',array(':nombre'=>$p->centro))->idCentro;
            }else{
                $modeloCentro = new Centro();
                $modeloCentro->nombre = $p->centro;
                $modeloCentro->save();
                $centroID = $modeloCentro->idCentro;
            }
            $proyectoToUpdate->centro=$centroID;
            $proyectoToUpdate->update();
            Yii::trace('project updated '.$proyectoToUpdate->nombre_sistema, 'sistemas.web');
        }
          }
      }
      //    }
      $objXLS->disconnectWorksheets();
      unset($objXLS);

      $projectsToMatch=array();
      $projectMatchings=array();
      foreach($proyectos as $proyecto){
        $personalInstituto = Personal::model()->getPersonal();
        $centros = Centro::model()->getCentros();
        $donantes = Donante::model()->getDonantes();
        $facultades = Facultad::model()->getFacultades();

        //obtener codigos del investigador
        $idPersonal = -1;
        $idCentro = -1;
        $idDonante = -1;
        $idFacultad = -1;
        $investigadorProyecto = preg_replace('/(\s+|\n|\r)+/',' ',$proyecto->investigador);
        $centroProyecto = preg_replace('/(\s+|\n|\r)+/',' ',$proyecto->centro);
        $donanteProyecto= preg_replace('/(\s+|\n|\r)+/',' ',$proyecto->donante);
        $facultadProyecto= preg_replace('/(\s+|\n|\r)+/',' ',$proyecto->dir_admin);

        $personalMatches=array();
        $centroMatches=array();
        $donanteMatches=array();
        $facultadMatches=array();
        foreach($personalInstituto as $personal) {
          $nombreConjunto = $personal->nombre;
          similar_text($nombreConjunto,$investigadorProyecto,$porcentajeInvestigador);
          if ($porcentajeInvestigador>90){
            $idPersonal = $personal->idPersonal;
            $personalMatches = array();
            break;
          }
          else if ($porcentajeInvestigador<=90 and $porcentajeInvestigador>70){
            if (empty($personalMatches[$proyecto->investigador])){
              $personalMatches[$proyecto->investigador]=array($nombreConjunto);
            }else{
              $personalMatches[$proyecto->investigador]=array_merge($personalMatches[$proyecto->investigador],array($nombreConjunto));
            }
          }
        }
        if ($proyecto->centro===''){
          if (!Centro::model()->exists('nombre=:nombre', array(':nombre'=>'No definido'))){
            $modeloCentro = new Centro();
            $modeloCentro->nombre = 'No definido';
            $modeloCentro->save();
            $idCentro = $modeloCentro->idCentro;
          }else {
            $idCentro = Centro::model()->find('nombre=:nombre',array(':nombre'=>'No definido'))->idCentro;
          }
        }else{
          foreach($centros as $centro) {
            $nombreCentro = preg_replace('/(\s+|\n|\r)+/',' ',$centro->nombre);
            similar_text($nombreCentro,$centroProyecto,$porcentajeCentro);
            if ($porcentajeCentro>90){
              $idCentro = $centro->idCentro;
              $centroMatches = array();
              break;
            }else if ($porcentajeCentro<=90 and $porcentajeCentro>70){
              if (empty($centroMatches[$proyecto->centro])){
                $centroMatches[$proyecto->centro]=array($centro->nombre);
              }
              else{
                $centroMatches[$proyecto->centro]=array_merge($centroMatches[$proyecto->centro],array($centro->nombre));
              }
            }
          }
        }
        foreach($donantes as $donante) {
          $nombreDonante = preg_replace('/(\s+|\n|\r)+/',' ',$donante->nombre);
          similar_text($nombreDonante,$donanteProyecto,$porcentajeDonante);
          if ($porcentajeDonante>90){
            $idDonante = $donante->idDonante;
            $donanteMatches = array();
            break;
          }else if ($porcentajeDonante<=90 and $porcentajeDonante>70){
            if (empty($donanteMatches[$proyecto->donante])){
              $donanteMatches[$proyecto->donante]=array($donante->nombre);
            }
            else{
              $donanteMatches[$proyecto->donante]=array_merge($donanteMatches[$proyecto->donante],array($donante->nombre));
            }
          }
        }
        foreach($facultades as $facultad) {
          $nombreFacultad = preg_replace('/(\s+|\n|\r)+/',' ',$facultad->nombre);
          similar_text($nombreFacultad,$facultadProyecto,$porcentajeFacultad);
          if ($porcentajeFacultad>90){
            $idFacultad = $facultad->idFacultad;
            $facultadMatches = array();
            break;
          }else if ($porcentajeFacultad<=90 and $porcentajeFacultad>70){
            if (empty($facultadMatches[$proyecto->dir_admin])){
              $facultadMatches[$proyecto->dir_admin]=array($facultad->nombre);
            }
            else{
              $facultadMatches[$proyecto->dir_admin]=array_merge($facultadMatches[$proyecto->dir_admin],array($facultad->nombre));
            }
          }
        }
        //obtener el codigo del investigador
        if ($idPersonal==-1 || $idPersonal==0){
          if (empty($personalMatches)){
            $personal = new Personal();
            $personal->nombreCompleto=$proyecto->investigador;
            $personal->save();

            Yii::log($personal->getErrors(),CLogger::LEVEL_INFO,'system.web.proyectos');
            $proyecto->investigador = $personal->idPersonal;
          }
        }else{
          $proyecto->investigador = $idPersonal;
        }
        //obtener codigo del centro
        if ($idCentro==-1){
          if (empty($centroMatches)){
            $centro = new Centro();
            $centro->nombre = $proyecto->centro;
            $centro->save();
            $proyecto->centro = $centro->idCentro;
          }
        }
        else {
          if ($idCentro ==0){
            if (!Centro::model()->exists('nombre=:nombre', array(':nombre'=>'No definido'))){
              $modeloCentro = new Centro();
              $modeloCentro->nombre = 'No definido';
              $modeloCentro->save();
              $idCentro = $modeloCentro->idCentro;
            }else {
              $idCentro = Centro::model()->find('nombre=:nombre',array(':nombre'=>'No definido'))->idCentro;
            }
          }else{
            $proyecto->centro = $idCentro;
          }
        }
        //obtener codigo del donante
        if ($idDonante==-1 || $idDonante ==0){
          if (empty($donanteMatches)){
            $donante = new Donante();
            $donante->nombre = $proyecto->donante;
            $donante->save();
            $proyecto->donante = $donante->idDonante;
          }
        }else{
          $proyecto->donante = $idDonante;
        }
        //obtener codigo de la facultad
        if ($idFacultad ==-1||$idFacultad ==0){
          if (empty($facultadMatches)){
            $facultad = new Facultad();
            $facultad->nombre = $proyecto->dir_admin;
            $facultad->save();
            $proyecto->dir_admin=$facultad->idFacultad;
          }
        }
        else{
          $proyecto->dir_admin = $idFacultad;
        }
        if (empty($personalMatches) and empty($centroMatches) and empty($donanteMatches) and empty($facultadMatches)){
          $proyecto->validate();
          if ($proyecto->save()){
            $index = array_search($proyecto, $proyectos);
            unset($proyectos[$index]);
          }
        }
        else {
          $projectMatches[$proyecto->idPermiso] = array($personalMatches,$centroMatches,$donanteMatches,$facultadMatches);
          array_push($projectMatchings,$projectMatches);
          $projectsToMatch[$proyecto->idPermiso]=$proyecto;
        }
      }
      if (!empty($projectMatchings)){
        $this->actionDisplayUploadMatches($projectsToMatch,$projectMatches);
        Yii::app()->user->setState('projectsToMatch',$projectsToMatch);
      }
      else{
        echo json_encode(array('message'=>'success'));
      }
    }catch(Exception $e){
      Yii::log($e->getMessage(),CLogger::LEVEL_INFO,'system.web.proyectos');
    }
  }

  /**
   */
  public function actionDisplayUploadMatches($projectsToMatch,$projectMatchings){
    $listOfMatchings = '<div id="bestMatches">';
    foreach($projectsToMatch as $project){
      $list = '<div id="'.$project->idPermiso.'"><h4> Datos Proyecto - '.$project->nombre_sistema.'</h4><hr>';
      $matchings = $projectMatchings[$project->idPermiso];
      $personal =$matchings[0];
      $centros = $matchings[1];
      $donantes = $matchings[2];
      $facultades = $matchings[3];
      if (!empty($personal)){
        $llave = array_keys($personal);
        $investigador = $llave[0];
        $personal[$investigador]=array_merge($personal[$investigador],array('--Nuevo Elemento--'));
        $list=$list.'<h5>Director Investigador Principal:</h5><div class="control-group"><label class="control-label">'.$investigador.'</label>';
        ob_start();
        $this->widget('bootstrap.widgets.TbSelect2',array(
          'name'=>preg_replace("/(\s+|\n|\r)+|([\.\'\(\)])+/", "", utf8_encode($investigador)),
          'data'=>$personal[$investigador],
        ));
        $select = ob_get_contents();
        ob_end_clean();
        $list=$list.'<div class="controls">'.$select.'</div></div>';
      }
      if (!empty($centros)){
        $llave = array_keys($centros);
        $centro = $llave[0];
        $centros[$centro]=array_merge($centros[$centro],array('--Nuevo Elemento--'));
        $list=$list.'<h5>Centro:</h5><div class="control-group"><label class="control-label">'.$centro.'</label>';
        ob_start();
        $this->widget('bootstrap.widgets.TbSelect2',array(
          'name'=>preg_replace("/(\s+|\n|\r)+|([\.\'\(\)])+/", "", utf8_encode($centro)),
          'data'=>$centros[$centro],
        ));
        $select = ob_get_contents();
        ob_end_clean();
        $list=$list.'<div class="controls">'.$select.'</div></div>';
      }
      if (!empty($donantes)){
        $llave = array_keys($donantes);
        $donante =$llave[0];
        $donantes[$donante]=array_merge($donantes[$donante],array('--Nuevo Elemento--'));
        $list=$list.'<h5>Nombre del Donante:</h5><div class="control-group"><label class="control-label">'.$donante.'</label>';
        ob_start();
        $this->widget('bootstrap.widgets.TbSelect2',array(
          'name'=>preg_replace("/(\s+|\n|\r)+|([\.\'\(\)])+/", "", utf8_encode($donante)),
          'data'=>$donantes[$donante],
        ));
        $select = ob_get_contents();
        ob_end_clean();
        $list=$list.'<div class="controls">'.$select.'</div></div>';
      }
      if (!empty($facultades)){
        $llave = array_keys($facultades);
        $facultad = $llave[0];
        $facultades[$facultad]=array_merge($facultades[$facultad],array('--Nuevo Elemento--'));
        $list=$list.'<h5>Dirección/Facultad:</h5><div class="control-group"><label class="control-label">'.$facultad.'</label>';
        ob_start();
        $this->widget('bootstrap.widgets.TbSelect2',array(
          'name'=>preg_replace("/(\s+|\n|\r)+|([\.\'\(\)])+/", "", utf8_encode($facultad)),
          'data'=>$facultades[$facultad],
        ));
        $select = ob_get_contents();
        ob_end_clean();
        $list=$list.'<div class="controls">'.$select.'</div></div>';
      }
      $list=$list.'<a class="btn btn-primary sendBestMatch">Establecer selección</a> <a class="btn cancelBestMatch">Cancelar</a></div>';
      $listOfMatchings=$listOfMatchings.$list;
    }
    $listOfMatchings=$listOfMatchings.'</div>';
    if (empty($projectsToMatch)){
      echo json_encode(array('message'=>'success'));
    }
    else {
      echo json_encode(array('listsHtml'=>$listOfMatchings));
    }
  }

  /**
   *
  */
  public function actionSaveBestMatches(){
    $matches = json_decode($_POST['matches']);
    $permisoProyecto = $matches[0];
    $proyectos = Yii::app()->user->getState('projectsToMatch');
    if (array_key_exists($permisoProyecto,$proyectos)){
      $proyecto = $proyectos[$permisoProyecto];
      $fields = $matches[1];
      $i = 0;
      foreach($fields as $field){
        $matchings = $matches[2+$i];
        $value = $matchings[0];
        $match = $matchings[1];
        if ($field=='Director Investigador Principal:'){
          if ($match=='--Nuevo Elemento--'){
            $personal = new Personal();
            $personal->nombreCompleto = $value;
            $personal->save();
            $proyecto->investigador=$personal->idPersonal;
          }
          else{
            $personal = Personal::model()->find("levenshtein(IFNULL(nombreCompleto,''),:nombre)<5",array(':nombre'=>$match));
            $proyecto->investigador=$personal->idPersonal;
            //agregar al bestMatch
            $bestMatch = new BestMatches();
            $bestMatch->value=$value;
            $bestMatch->suggestion=$match;
            $bestMatch->domain='Investigador';
            $bestMatch->idDominio = $personal->idPersonal;
            $bestMatch->save();
          }
        }
        else if ($field=='Nombre del Donante:'){
          if ($match=='--Nuevo Elemento--'){
            $donante = new Donante();
            $donante->nombre = $value;
            $donante->save();
            $proyecto->donante=$donante->idDonante;
          }
          else{
            $donante = Donante::model()->find('levenshtein(nombre,:nombre)<5',array(':nombre'=>$match));
            $proyecto->donante=$donante->idDonante;
            //agregar al bestMatch
            $bestMatch = new BestMatches();
            $bestMatch->value=$value;
            $bestMatch->suggestion=$match;
            $bestMatch->domain='Donante';
            $bestMatch->idDominio=$donante->idDonante;
            $bestMatch->save();
          }
        }
        else if ($field=='Centro:'){
          if ($match=='--Nuevo Elemento--'){
            $centro = new Centro();
            $centro->nombre = $value;
            $centro->save();
            $proyecto->centro=$centro->idCentro;
          }
          else{
            $centro = Centro::model()->find('levenshtein(nombre,:nombre)<5',array(':nombre'=>$match));
            $proyecto->centro=$centro->idCentro;
            //agregar al bestMatch
            $bestMatch = new BestMatches();
            $bestMatch->value=$value;
            $bestMatch->suggestion=$match;
            $bestMatch->domain='Centro';
            $bestMatch->idDominio=$centro->idCentro;
            $bestMatch->save();
          }
        }
        else if ($field=='Dirección/Facultad:'){
          if ($match=='--Nuevo Elemento--'){
            $facultad = new Facultad();
            $facultad->nombre = $value;
            $facultad->save();
            $proyecto->dir_admin=$facultad->idFacultad;
          }
          else{
            $facultad = Facultad::model()->find('levenshtein(nombre,:nombre)<5',array(':nombre'=>$match));
            $proyecto->dir_admin=$facultad->idFacultad;
            //agregar al bestMatch
            $bestMatch = new BestMatches();
            $bestMatch->value=$value;
            $bestMatch->suggestion=$match;
            $bestMatch->domain='Facultad';
            $bestMatch->idDominio=$facultad->idFacultad;
            $bestMatch->save();
          }
        }
        $i++;
      }
      if ($proyecto->validate()){
        if ($proyecto->save()){
          $index = array_search($proyecto, $proyectos);
          unset($proyectos[$index]);
          Yii::app()->user->setState('projectsToMatch', $proyectos);
        }

        $arr = array('response'=>'success',
             'message'=>'Proyecto ingresado exitosamente.');
      }
      else{
        $arr = array('response'=>'error',
               'message'=>'Error Interno');
      }
    }
    else {
      $arr = array('response'=>'success',
             'message'=>'Proyecto ya ingresado.');
    }
    echo json_encode($arr);
  }

  /**
   * Deletes a particular model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id the ID of the model to be deleted
   */
  public function actionDelete($id)
  {
    if(Yii::app()->request->isPostRequest)
    {
      // we only allow deletion via POST request
      $this->loadModel($id)->delete();

      // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
      if(!isset($_GET['ajax']))
        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }
    else
      throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
  }

  /**
   * Lists all models.
   */
  public function actionIndex()
  {
    $model = new Proyecto();
    if (isset($_GET['Proyecto'])){
      $model->attributes=$_GET['Proyecto'];

      $showAll = false;
      $pastFilters = Yii::app()->user->getState('pastFilters');
      $oldModel = (empty($pastFilters))?new Proyecto():$pastFilters;
      foreach($_GET['Proyecto'] as $field=>$value){
        if ($value=='TODOS'){
          $showAll = true;
          break;
        }
        else{
          if(empty($model->$field) && !empty($oldModel->$field)){
            $model->$field= $oldModel->$field;
          }
        }
      }
      if (!$showAll){
        Yii::app()->user->setState('pastFilters',$model);
      }
      else{
        $model->unsetAttributes();
        Yii::app()->user->setState('pastFilters',new Proyecto());
      }
    }
    $this->render('index',array(
      'dataProvider'=>$model->search(),
    ));
  }

  public function actionEditOne($id){
    $model=Proyecto::model()->findByPk($id);
    $this->render('editOne',array(
      'model'=>$model,
    ));
  }

  /**
   * Manages all models.
   */
  public function actionAdmin()
  {
    $model=new Proyecto('search');
    $model->unsetAttributes();  // clear any default values
    if(isset($_GET['Proyecto']))
      $model->attributes=$_GET['Proyecto'];

    $this->render('admin',array(
      'model'=>$model,
    ));
  }

  public function actionEditAll(){
    $r = Yii::app()->getRequest();
    $proyecto = Proyecto::model()->findByPk($r->getParam('pk'));
    $field = $r->getParam('name');
    $value = $r->getParam('value');
    $proyecto->$field = $value;
    if ($proyecto->save()){
      echo $value;
    }
    else{
      echo 'Hubo un error al guardar este campo';
    }
  }

  /**
   * Returns the data model based on the primary key given in the GET variable.
   * If the data model is not found, an HTTP exception will be raised.
   * @param integer the ID of the model to be loaded
   */
  public function loadModel($id){
            $model=Proyecto::model()->findByPk((int)$id);
            if($model===null)
                throw new CHttpException(404,'The requested page does not exist.');
            return $model;
  }

  /**
   * Performs the AJAX validation.
   * @param CModel the model to be validated
   */

  protected function performAjaxValidation($model){
            if(isset($_POST['ajax']) && $_POST['ajax']==='proyecto-form'){
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
  }
}
