<?php

/**
 * This is the model class for table "institucion".
 *
 * The followings are the available columns in table 'institucion':
 * @property integer $idInstitucion
 * @property string $nombre
 * @property integer $idTipoColaboracion
 * @property integer $subcontratada
 */
class Institucion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Institucion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'institucion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, idTipoColaboracion, subcontratada', 'required'),
			array('idTipoColaboracion, subcontratada', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idInstitucion, nombre, idTipoColaboracion, subcontratada', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tipoColaboracion'=>array(self::BELONGS_TO,'TipoColaboracion','idTipoColaboracion')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idInstitucion' => 'Institución',
			'nombre' => 'Nombre',
			'idTipoColaboracion' => 'Tipo Colaboración',
			'subcontratada' => 'Subcontratada',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idInstitucion',$this->idInstitucion);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('idTipoColaboracion',$this->idTipoColaboracion);
		$criteria->compare('subcontratada',$this->subcontratada);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}