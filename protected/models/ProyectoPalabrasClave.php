<?php

/**
 * This is the model class for table "proyecto_palabrasClave".
 *
 * The followings are the available columns in table 'proyecto_palabrasClave':
 * @property integer $idProyectoPalabraClave
 * @property integer $idProyecto
 * @property integer $idPalabraClave
 *
 * The followings are the available model relations:
 * @property Proyecto $idProyecto0
 * @property Palabrasclave $idPalabraClave0
 */
class ProyectoPalabrasClave extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProyectoPalabrasClave the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'proyecto_palabrasclave';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idProyecto, idPalabraClave', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idProyectoPalabraClave, idProyecto, idPalabraClave', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idProyecto0' => array(self::BELONGS_TO, 'Proyecto', 'idProyecto'),
			'idPalabraClave0' => array(self::BELONGS_TO, 'Palabrasclave', 'idPalabraClave'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idProyectoPalabraClave' => 'Id Proyecto Palabra Clave',
			'idProyecto' => 'Id Proyecto',
			'idPalabraClave' => 'Id Palabra Clave',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idProyectoPalabraClave',$this->idProyectoPalabraClave);
		$criteria->compare('idProyecto',$this->idProyecto);
		$criteria->compare('idPalabraClave',$this->idPalabraClave);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}