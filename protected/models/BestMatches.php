<?php /**
 * This is the model class for table "bestMatches".
 *
 * The followings are the available columns in table 'bestMatches':
 * @property integer $idMatch
 * @property string $value
 * @property string $suggestion
 * @property string $domain
 */
class BestMatches extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return BestMatches the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'bestmatches';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('value, suggestion, domain', 'required'),
            array('value, suggestion', 'length', 'max'=>100),
            array('domain', 'length', 'max'=>45),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('idMatch, value, suggestion, domain', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idMatch' => 'Id Match',
            'value' => 'Value',
            'suggestion' => 'Suggestion',
            'domain' => 'Domain',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('idMatch',$this->idMatch);
        $criteria->compare('value',$this->value,true);
        $criteria->compare('suggestion',$this->suggestion,true);
        $criteria->compare('domain',$this->domain,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}
?>