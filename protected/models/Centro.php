<?php

/**
 * This is the model class for table "centro".
 *
 * The followings are the available columns in table 'centro':
 * @property integer $idCentro
 * @property string $nombre
 * @property integer $facultad
 *
 * The followings are the available model relations:
 * @property Facultad $facultad0
 * @property Personal[] $personals
 */
class Centro extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Centro the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'centro';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('facultad', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>150),
                        array('nombre', 'unique'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idCentro, nombre, facultad', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'facultad0' => array(self::BELONGS_TO, 'Facultad', 'facultad'),
			'personals' => array(self::HAS_MANY, 'Personal', 'centro'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCentro' => 'Id Centro',
			'nombre' => 'Nombre',
			'facultad' => 'Facultad',
		);
	}

	public function getCentros(){
		$centrosList = array();
		$centros = Centro::model()->findAll();
		//ademas agregarle las best matches del suggestMatches para centros
		foreach($centros as $c){
			$centroTemplate = new stdClass;
			$centroTemplate->nombre = $c->nombre;
			$centroTemplate->idCentro = $c->idCentro;
			array_push($centrosList,$centroTemplate);
		}
		$centroMatches = BestMatches::model()->findAll('domain="Centro"');
		foreach($centroMatches as $centroMatch){
			$centroTemplate = new stdClass;
			$centroTemplate->nombre = $centroMatch->value;
			$centroTemplate->idCentro = $centroMatch->idDominio;
			array_push($centrosList,$centroTemplate);
		}
		return $centrosList;
	}
	
	public function getCentroFilters(){
		$centroList = array();
		$centros = Centro::model()->findAll();
		foreach($centros as $c){
			array_push($centroList,ucfirst(strtolower($c->nombre)));
		}
		return $centroList;
	}
	
	public function getCentroList($filtro=''){
		$centros = Centro::model()->findAll($filtro);
		return CHtml::listData($centros,'idCentro','nombre');
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCentro',$this->idCentro);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('facultad',$this->facultad);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}