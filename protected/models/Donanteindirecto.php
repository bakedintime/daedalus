<?php

/**
 * This is the model class for table "donanteindirecto".
 *
 * The followings are the available columns in table 'donanteindirecto':
 * @property integer $idDonanteIndirecto
 * @property string $nombre
 * @property string $pais
 * @property integer $tipo
 * @property integer $procedencia
 */
class Donanteindirecto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Donanteindirecto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'donanteindirecto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tipo, procedencia', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>255),
			array('pais', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idDonanteIndirecto, nombre, pais, tipo, procedencia', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'tipoDonante'=>array(self::BELONGS_TO, 'TipoDonante','tipo'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idDonanteIndirecto' => 'Id Donante Indirecto',
			'nombre' => 'Nombre',
			'pais' => 'Pais',
			'tipo' => 'Tipo',
			'procedencia' => 'Procedencia',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idDonanteIndirecto',$this->idDonanteIndirecto);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('pais',$this->pais,true);
		$criteria->compare('tipo',$this->tipo);
		$criteria->compare('procedencia',$this->procedencia);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}