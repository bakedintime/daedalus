<?php

/**
 * This is the model class for table "donanteIndirecto_proyecto".
 *
 * The followings are the available columns in table 'donanteIndirecto_proyecto':
 * @property integer $idDonanteIndirecto_Proyecto
 * @property integer $idDonanteIndirecto
 * @property integer $idProyecto
 *
 * The followings are the available model relations:
 * @property Proyecto $idProyecto0
 * @property Donanteindirecto $idDonanteIndirecto0
 */
class DonanteIndirecto_Proyecto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DonanteIndirecto_Proyecto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'donanteIndirecto_proyecto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idDonanteIndirecto, idProyecto', 'required'),
			array('idDonanteIndirecto, idProyecto', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idDonanteIndirecto_Proyecto, idDonanteIndirecto, idProyecto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idProyecto0' => array(self::BELONGS_TO, 'Proyecto', 'idProyecto'),
			'idDonanteIndirecto0' => array(self::BELONGS_TO, 'Donanteindirecto', 'idDonanteIndirecto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idDonanteIndirecto_Proyecto' => 'Id Donante Indirecto Proyecto',
			'idDonanteIndirecto' => 'Id Donante Indirecto',
			'idProyecto' => 'Id Proyecto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idDonanteIndirecto_Proyecto',$this->idDonanteIndirecto_Proyecto);
		$criteria->compare('idDonanteIndirecto',$this->idDonanteIndirecto);
		$criteria->compare('idProyecto',$this->idProyecto);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}