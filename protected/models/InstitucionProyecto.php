<?php

/**
 * This is the model class for table "institucion_proyecto".
 *
 * The followings are the available columns in table 'institucion_proyecto':
 * @property integer $idInstitucion_Proyecto
 * @property integer $idInstitucion
 * @property integer $idProyecto
 *
 * The followings are the available model relations:
 * @property Proyecto $idProyecto0
 * @property Institucion $idInstitucion0
 */
class InstitucionProyecto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InstitucionProyecto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'institucion_proyecto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idInstitucion, idProyecto', 'required'),
			array('idInstitucion, idProyecto', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idInstitucion_Proyecto, idInstitucion, idProyecto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idProyecto0' => array(self::BELONGS_TO, 'Proyecto', 'idProyecto'),
			'idInstitucion0' => array(self::BELONGS_TO, 'Institucion', 'idInstitucion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idInstitucion_Proyecto' => 'Id Institucion Proyecto',
			'idInstitucion' => 'Id Institucion',
			'idProyecto' => 'Id Proyecto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idInstitucion_Proyecto',$this->idInstitucion_Proyecto);
		$criteria->compare('idInstitucion',$this->idInstitucion);
		$criteria->compare('idProyecto',$this->idProyecto);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}