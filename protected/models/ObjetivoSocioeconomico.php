<?php

/**
 * This is the model class for table "objetivo_socioeconomico".
 *
 * The followings are the available columns in table 'objetivo_socioeconomico':
 * @property integer $idObjetivo_socioeconomico
 * @property string $nombre
 *
 * The followings are the available model relations:
 * @property Proyecto[] $proyectos
 */
class ObjetivoSocioeconomico extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return ObjetivoSocioeconomico the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'objetivo_socioeconomico';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idObjetivo_socioeconomico', 'required'),
			array('idObjetivo_socioeconomico', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idObjetivo_socioeconomico, nombre', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'proyectos' => array(self::HAS_MANY, 'Proyecto', 'idObjetivo_socioeconomico'),
		);
	}

	public function getObjetivos(){
		$objetivos = ObjetivoSocioeconomico::model()->findAll();
		return CHtml::listData($objetivos,'idObjetivo_socioeconomico','nombre');
	}
	
	public function getObjetivoFilters(){
		$objetivos = ObjetivoSocioeconomico::model()->findAll();
		return CHtml::listData($objetivos,'idObjetivo_socioeconomico',function($objetivo){
			return ucfirst(strtolower($objetivo->nombre));
		});
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idObjetivo_socioeconomico' => 'Id Objetivo Socioeconomico',
			'nombre' => 'Nombre',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idObjetivo_socioeconomico',$this->idObjetivo_socioeconomico);
		$criteria->compare('nombre',$this->nombre,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}