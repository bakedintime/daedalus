<?php

/**
 * This is the model class for table "director_centro".
 *
 * The followings are the available columns in table 'director_centro':
 * @property integer $idDirector_centro
 * @property integer $personal_dc
 *
 * The followings are the available model relations:
 * @property Personal $personalDc0
 */
class DirectorCentro extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return DirectorCentro the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'director_centro';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('personal_dc', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idDirector_centro, personal_dc', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'personalDc0' => array(self::BELONGS_TO, 'Personal', 'personal_dc'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idDirector_centro' => 'Id Director Centro',
			'personal_dc' => 'Personal Dc',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idDirector_centro',$this->idDirector_centro);
		$criteria->compare('personal_dc',$this->personal_dc);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}