<?php

/**
 * This is the model class for table "personal".
 *
 * The followings are the available columns in table 'personal':
 * @property integer $idPersonal
 * @property string $nombreCompleto
 * @property string $direccion
 * @property integer $telefono
 * @property string $email
 * @property string $titulos_universitarios
 * @property integer $centro
 *
 * The followings are the available model relations:
 * @property DirectorCentro[] $directorCentros
 * @property Investigador[] $investigadors
 * @property Centro $centro0
 */
class Personal extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Personal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'personal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('telefono, centro', 'numerical', 'integerOnly'=>true),
			array('nombreCompleto, email', 'length', 'max'=>45),
			array('direccion, titulos_universitarios', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idPersonal, nombreCompleto, direccion, telefono, email, titulos_universitarios, centro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'directorCentros' => array(self::HAS_MANY, 'DirectorCentro', 'personal_dc'),
			'investigadors' => array(self::HAS_MANY, 'Investigador', 'personal_i'),
			'centro0' => array(self::BELONGS_TO, 'Centro', 'centro'),
		);
	}

	public function getPersonal(){
		$personal = array();
		$personalInstituto = Personal::model()->findAll(array('order'=>'nombreCompleto','condition'=>'nombreCompleto is not null'));
		//ademas agregarle las best matches del suggestMatches para personal
		foreach($personalInstituto as $p){
			$personalTemplate = new stdClass;
			$personalTemplate->nombre = $p->nombreCompleto;
			$personalTemplate->idPersonal = $p->idPersonal;
			array_push($personal,$personalTemplate);
		}
		$personalMatches = BestMatches::model()->findAll('domain="Investigador"');
		foreach($personalMatches as $personalMatch){
			$personalTemplate = new stdClass;
			$personalTemplate->nombre = $personalMatch->value;
			$personalTemplate->idPersonal = $personalMatch->idDominio;
			array_push($personal,$personalTemplate);
		}
		return $personal;
	}

	public function getPersonalId($nombre){
		$investigador =$this->find("levenshtein(COALESCE(nombreCompleto,' '),:nombreInvestigador) < 4",
					   array(':nombreInvestigador'=>$nombre));
		return $investigador->idPersonal;
	}

	/**
	 *
	 */
	public function getInvestigadores(){
		$investigadores = array();
		$investigador = Personal::model()->findAll(array('order'=>'nombreCompleto','condition'=>'nombreCompleto is not null'));
		foreach($investigador as $i){
			//$nombreApellido = trim(preg_replace('/\s+/', ' ', $i->nombreCompleto));
			array_push($investigadores,ucfirst(strtolower($i->nombreCompleto)));
		}
		return $investigadores;
	}

	public function getInvestigadorList($filtro=''){
		$investigador = Personal::model()->findAll(array('order'=>'nombreCompleto','condition'=>'nombreCompleto is not null'));
		return CHtml::listData($investigador,'idPersonal',function($investigador) {
			return $investigador->nombreCompleto;
		});
	}

	public function getInvestigadorJSON($filtro=''){
		$investigadores = Personal::model()->findAll(array('order'=>'nombreCompleto','condition'=>'nombreCompleto is not null'));
		$investigadoresN = array();
		foreach($investigadores as $i){
			array_push($investigadoresN,array('id'=>$i->idPersonal,'text'=>$i->nombreCompleto));
		}
		return CJSON::encode($investigadoresN);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idPersonal' => 'Id Personal',
			'nombreCompleto' => 'Nombre Completo',
			'direccion' => 'Direccion',
			'telefono' => 'Telefono',
			'email' => 'Email',
			'titulos_universitarios' => 'Titulos Universitarios',
			'centro' => 'Centro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idPersonal',$this->idPersonal);
		$criteria->compare('nombreCompleto',$this->nombre,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('telefono',$this->telefono);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('titulos_universitarios',$this->titulos_universitarios,true);
		$criteria->compare('centro',$this->centro);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}
