<?php

/**
 * This is the model class for table "palabrasclave".
 *
 * The followings are the available columns in table 'palabrasclave':
 * @property integer $idPalabra
 * @property string $nombre
 * @property integer $idTipoPalabraClave
 *
 * The followings are the available model relations:
 * @property ProyectoPalabrasClave[] $proyectoPalabrasClaves
 */
class Palabrasclave extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Palabrasclave the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'palabrasclave';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idTipoPalabraClave', 'required'),
			array('idTipoPalabraClave', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>100),
			array('nombre','checkUniqueness'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idPalabra, nombre, idTipoPalabraClave', 'safe', 'on'=>'search'),
		);
	}
	
	public function checkUniqueness(){
		$coincidencia = $this->exists('nombre=:nombre and idTipoPalabraClave=:idTipoPalabraClave',
			      array(':nombre'=>$this->nombre,
				    ':idTipoPalabraClave'=>$this->idTipoPalabraClave));
		if ($coincidencia){
			$this->addError('nombre','Ya existe una coincidencia para esta palabra clave');
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'proyectoPalabrasClaves' => array(self::HAS_MANY, 'ProyectoPalabrasClave', 'idPalabraClave'),
			'tipoPalabraClave'=>array(self::BELONGS_TO,'TipoPalabraClave','idTipoPalabraClave')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idPalabra' => 'Id Palabra',
			'nombre' => 'Nombre',
			'idTipoPalabraClave' => 'Id Tipo Palabra Clave',
		);
	}
	
	/**
	 */
	public function getTags($tipoTag){
		$model = TipoPalabraClave::model()->find('descripcion like :descripcion',array(':descripcion'=>$tipoTag));
		$tags = Palabrasclave::model()->findAll('idTipoPalabraClave=:idTipoPalabra',array(':idTipoPalabra'=>$model->idTipoPalabraClave));
		$tagNames = array();
		foreach($tags as $tag){
			array_push($tagNames,array('id'=>$tag->idPalabra,'text'=>$tag->nombre));
		}
		return CJSON::encode($tagNames);
	}
	
	public function getAllProjectTags(){
		$tags = Palabrasclave::model()->findAll('idPalabra in (select distinct idPalabraClave from proyecto_palabrasclave)');
		$tagNames = array();
		foreach($tags as $tag){
			array_push($tagNames,array('id'=>$tag->idPalabra,'text'=>$tag->nombre));
		}
		return CJSON::encode($tagNames);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idPalabra',$this->idPalabra);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('idTipoPalabraClave',$this->idTipoPalabraClave);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}