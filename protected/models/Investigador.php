<?php

/**
 * This is the model class for table "investigador".
 *
 * The followings are the available columns in table 'investigador':
 * @property integer $idInvestigador
 * @property integer $personal_i
 *
 * The followings are the available model relations:
 * @property Personal $personalI0
 * @property Proyecto[] $proyectos
 */
class Investigador extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Investigador the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'investigador';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('personal_i', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idInvestigador, personal_i', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'personalI0' => array(self::BELONGS_TO, 'Personal', 'personal_i'),
			'proyectos' => array(self::HAS_MANY, 'Proyecto', 'investigador'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idInvestigador' => 'Id Investigador',
			'personal_i' => 'Personal I',
		);
	}
	

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idInvestigador',$this->idInvestigador);
		$criteria->compare('personal_i',$this->personal_i);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}