<?php

/**
 * This is the model class for table "proyecto".
 *
 * The followings are the available columns in table 'proyecto':
 * @property integer $idProyecto
 * @property integer $idPermiso
 * @property string $tipo
 * @property string $no_conv
 * @property string $salarios
 * @property string $equipo
 * @property string $software
 * @property string $otros
 * @property string $overhead
 * @property string $disciplina_cientifica
 * @property string $url_informe_final
 * @property string $monto_presupuestado
 * @property string $monto_ejecutado
 * @property int $fondo_internacional
 * @property integer $concluido
 * @property string $inicio_ejecucion
 * @property string $fin_ejecucion
 * @property string $url_abstract
 * @property integer $disciplina
 * @property integer $investigador
 * @property integer $objetivo_socioeconomico
 * @property integer $dir_admin
 * @property integer $donante
 * @property integer $centro
 * @property string $referencias
 *
 * The followings are the available model relations:
 * @property Disciplina $disciplina0
 * @property Investigador $investigador0
 * @property ObjetivoSocioeconomico $objetivoSocioeconomico0
 * @property DirectorAdministrador $dirAdmin0
 * @property Donante $donante0
 */
class Proyecto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Proyecto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'proyecto';
	}

	public function beforeSave(){
		$hasOcurrence = Proyecto::model()->exists(
			'idPermiso=:idPermiso and no_conv=:no_conv and levenshtein(nombre_sistema,:nombre_sistema) < 5',
			 array(
			 	':idPermiso'=>$this->idPermiso,
			 	':no_conv'=>$this->no_conv,
			 	':nombre_sistema'=>$this->nombre_sistema
			 )
		);
		if ($this->isNewRecord && !$hasOcurrence){
			$this->concluido = empty($this->concluido) ? 'vigente' : $this->concluido;
			$this->salarios = (empty($this->salarios) ? 0 : $this->salarios);
			$this->equipo = (empty($this->equipo) ? 0 : $this->equipo);
			$this->software  = (empty($this->software) ? 0 : $this->software);
			$this->otros = (empty($this->otros) ? 0 : $this->otros);
			$this->overhead = (empty($this->overhead) ? 0 : $this->overhead);
			//ejecutado por disiciplina
			$this->disciplina = (empty($this->disciplina)?0:$this->disciplina);
			$this->url_abstract = (empty($this->url_abstract)? "no definido":$this->url_abstract);
			$this->url_informe_final = (empty($this->url_informe_final)? "no definido":$this->url_informe_final);
			$this->objetivo_socioeconomico = (empty($this->objetivo_socioeconomico)?14:$this->objetivo_socioeconomico);
			$this->disciplina_cientifica = (empty($this->disciplina_cientifica)?7:$this->disciplina_cientifica);
			$fondos = array('internacional','internacionales');
			$this->fondo_internacional=(in_array(strtolower(trim($this->fondo_internacional)),$fondos)==1 ? 1: 0);
			try{
				$this->monto_presupuestado = (float) $this->monto_presupuestado;
			}
			catch(Exception $e){
				$this->monto_presupuestado = 0;
			}
			$this->monto_ejecutado = (empty($this->monto_ejecutado) ? 0 : $this->monto_ejecutado);
			$this->inicio_ejecucion = (strtolower($this->inicio_ejecucion)=='indefinido') ? null : $this->inicio_ejecucion;
			$this->fin_ejecucion = (strtolower($this->fin_ejecucion)=='indefinido') ? null : $this->fin_ejecucion;
			$this->encuestaAbierta = empty($this->encuestaAbierta)?0:$this->encuestaAbierta;
		}
		else{
			$this->salarios = (empty($this->salarios) ? 0 : $this->salarios);
			$this->equipo = (empty($this->equipo) ? 0 : $this->equipo);
			$this->software  = (empty($this->software) ? 0 : $this->software);
			$this->otros = (empty($this->otros) ? 0 : $this->otros);
			$this->overhead = (empty($this->overhead) ? 0 : $this->overhead);
			if (empty($this->inicio_ejecucion) || (strtolower($this->inicio_ejecucion)=='indefinido')){
				$this->inicio_ejecucion = null;
			}
			else{
				$this->inicio_ejecucion = $this->inicio_ejecucion;
			}
			if (empty($this->fin_ejecucion) || (strtolower($this->fin_ejecucion)=='indefinido')){
				$this->fin_ejecucion = null;
			}
			else{
				$this->fin_ejecucion = $this->fin_ejecucion;
			}
			try{
				$this->monto_presupuestado = (float) $this->monto_presupuestado;
			}
			catch(Exception $e){
				$this->monto_presupuestado = 0;
			}
			$this->monto_ejecutado = (empty($this->monto_ejecutado) ? 0 : $this->monto_ejecutado);
			$this->disciplina= (empty($this->disciplina)?7:$this->disciplina);
			$this->objetivo_socioeconomico = (empty($this->objetivo_socioeconomico)?14:$this->objetivo_socioeconomico);
			$this->disciplina_cientifica = (empty($this->disciplina_cientifica)?7:$this->disciplina_cientifica);
		}
		return parent::beforeSave();
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idPermiso,centro,dir_admin,investigador,donante', 'required'),
			array('idProyecto, idPermiso, objetivo_socioeconomico', 'numerical', 'integerOnly'=>true),
			array('tipo, disciplina_cientifica, url_informe_final, url_abstract', 'length', 'max'=>45),
			array('no_conv', 'length', 'max'=>150),
			array('inicio_ejecucion, fin_ejecucion', 'length', 'max'=>40),
			array('idProyecto','unicidad'),
			array('referencias','length','max'=>500),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idProyecto,encuestaAbierta,referencias,nombre_sistema,nombre_largo, idPermiso, tipo, no_conv, salarios,overhead, equipo, software, otros, disciplina_cientifica, url_informe_final, monto_presupuestado, monto_ejecutado, fondo_internacional, concluido, inicio_ejecucion,fin_ejecucion, url_abstract, disciplina, investigador, objetivo_socioeconomico, dir_admin, donante,centro', 'safe'),
		);
	}

	/**
	 * Se asegura que el proyecto no ha sido ingresado
	 * posteriormente
	 */
	public function unicidad($attribute,$params){
		$existe = Proyecto::model()->exists('idPermiso=:idPermiso and no_conv=:no_conv and levenshtein(nombre_sistema,:nombre_sistema) < 5 and idProyecto != :idProyecto',
						     array(':idPermiso'=>$this->idPermiso,
							   ':no_conv'=>$this->no_conv,
							   ':nombre_sistema'=>$this->nombre_sistema,
							   ':idProyecto'=>$this->idProyecto));
		if ($existe){
			$this->addError('idProyecto','Proyecto repetido');
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'investigador0' => array(self::BELONGS_TO, 'Personal', 'investigador'),
			'objetivosSocio' => array(self::BELONGS_TO, 'ObjetivoSocioeconomico', 'objetivo_socioeconomico'),
			'facultad0' => array(self::BELONGS_TO, 'Facultad', 'dir_admin'),
			'donante0' => array(self::BELONGS_TO, 'Donante', 'donante'),
			'centro0'=> array(self::BELONGS_TO, 'Centro', 'centro'),
			'disciplinas'=>array(self::BELONGS_TO, 'Disciplina','disciplina_cientifica'),
			'tipoProyecto'=>array(self::BELONGS_TO,'TipoProyecto','tipo'),
			'palabrasClave'=>array(self::HAS_MANY,'Palabrasclave','idProyecto'),
			'institucion'=>array(self::HAS_MANY,'Institucion','idProyecto'),
			'donantesIndirectos'=>array(self::HAS_MANY,'Donanteindirecto','idProyecto')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idProyecto' => 'Id Proyecto',
			'idPermiso' => 'Id Permiso',
			'tipo' => 'Tipo',
			'no_conv' => 'No Conv',
			'salarios' => 'Salarios',
			'equipo' => 'Equipo',
			'software' => 'Software',
			'otros' => 'Otros',
			'disciplina_cientifica' => 'Disciplina Cientifica',
			'url_informe_final' => 'Informe Final',
			'monto_presupuestado' => 'Monto Presupuestado',
			'monto_ejecutado' => 'Monto Ejecutado',
			'fondo_internacional' => 'Fondo Internacional',
			'concluido' => 'Concluido',
			'inicio_ejecucion' => 'Inicio de la Ejecución',
			'fin_ejecucion'=>'Fin de la Ejecución',
			'url_abstract' => 'Abstract del Proyecto',
			'disciplina' => 'Disciplina',
			'investigador' => 'Investigador',
			'objetivo_socioeconomico' => 'Objetivo Socioeconomico',
			'dir_admin' => 'Facultad',
			'donante' => 'Donante',
			'overhead' => 'Overhead',
			'referencias' =>'Referencias',
			'encuestaAbierta'=>'Encuesta Abierta',
		);
	}

	public function getProjectNames(){
		$names=array();
		$proyectos=Proyecto::model()->findAll();
		foreach($proyectos as $proyecto){
			$nombreProyecto = trim(preg_replace('/\s+/', ' ', $proyecto->nombre_largo));
			array_push($names,$nombreProyecto);
		}
		return $names;
	}

	public function getProjectFilters(){
		$projects=array();
		$proyectos=Proyecto::model()->findAll();
		foreach($proyectos as $proyecto){
			$project = array();
			$project['id']=$proyecto->idProyecto;
			$project['nombre']= trim(preg_replace('/\s+/', ' ', $proyecto->nombre_largo));
			array_push($projects,$project);
		}
		return json_encode($projects);
	}

	public function palabrasClaveAsignadas($tipoTag,$id){
		$model = TipoPalabraClave::model()->find('descripcion like :descripcion',array(':descripcion'=>$tipoTag));
		$tags = Palabrasclave::model()->findAll('idTipoPalabraClave=:idTipoPalabra',array(':idTipoPalabra'=>$model->idTipoPalabraClave));

		$palabrasClave ='';
		foreach($tags as $tag){
			$palabrasClave .= $tag->idPalabra.',';
		}
		$palabrasClave = substr($palabrasClave,0,strlen($palabrasClave)-1);
		$tagNames = array();

		if ($palabrasClave!=''){
			$palabrasAsignadas = ProyectoPalabrasClave::model()->findAll('idProyecto=:idProyecto and idPalabraClave in (:palabrasClave)',
									     array(':idProyecto'=>$id,
									     	   ':palabrasClave'=>$palabrasClave));
			foreach($palabrasAsignadas as $asignada){
				array_push($tagNames,$asignada->idPalabraClave);
			}
		}
		return CJSON::encode($tagNames);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('no_conv',$this->no_conv,true);
		$criteria->compare('salarios',$this->salarios,true);
		$criteria->compare('equipo',$this->equipo,true);
		$criteria->compare('software',$this->software,true);
		$criteria->compare('otros',$this->otros,true);
		$criteria->compare('overhead',$this->otros,true);
		$criteria->compare('disciplina_cientifica',$this->disciplina_cientifica,true);
		$criteria->compare('url_informe_final',$this->url_informe_final,true);
		$criteria->compare('monto_presupuestado',$this->monto_presupuestado,true);
		$criteria->compare('monto_ejecutado',$this->monto_ejecutado,true);
		$criteria->compare('fondo_internacional',$this->fondo_internacional,true);
		$criteria->compare('concluido',$this->concluido);
		$criteria->compare('inicio_ejecucion',$this->inicio_ejecucion,true);
		$criteria->compare('fin_ejecucion',$this->fin_ejecucion,true);
		$criteria->compare('centro',$this->centro,true);
		$criteria->compare('url_abstract',$this->url_abstract,true);
		$criteria->compare('disciplina',$this->disciplina);
		$criteria->compare('investigador',$this->investigador);
		$criteria->compare('objetivo_socioeconomico',$this->objetivo_socioeconomico);
		$criteria->compare('dir_admin',$this->dir_admin);
		$criteria->compare('donante',$this->donante);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}
