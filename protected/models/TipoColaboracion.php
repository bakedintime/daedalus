<?php

/**
 * This is the model class for table "tipoColaboracion".
 *
 * The followings are the available columns in table 'tipoColaboracion':
 * @property integer $idTipoColaboracion
 * @property string $descripcion
 */
class TipoColaboracion extends CActiveRecord
{
	const TIPO_UVG = 1;
	const TIPO_CONCYT = 2;
	const TIPO_OTRA = 3;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TipoColaboracion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tipoColaboracion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idTipoColaboracion, descripcion', 'required'),
			array('idTipoColaboracion', 'numerical', 'integerOnly'=>true),
			array('descripcion', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idTipoColaboracion, descripcion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idTipoColaboracion' => 'Tipo de Colaboración',
			'descripcion' => 'Descripcion',
		);
	}

	public function getTipos(){
		$tipos = TipoColaboracion::model()->findAll();
		return CHtml::listData($tipos, 'idTipoColaboracion', 'descripcion');
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idTipoColaboracion',$this->idTipoColaboracion);
		$criteria->compare('descripcion',$this->descripcion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}