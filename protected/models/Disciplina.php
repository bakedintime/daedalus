<?php

/**
 * This is the model class for table "disciplina".
 *
 * The followings are the available columns in table 'disciplina':
 * @property integer $idDisciplina
 * @property string $nombre
 *
 * The followings are the available model relations:
 * @property Proyecto[] $proyectos
 */
class Disciplina extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Disciplina the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'disciplina';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('idDisciplina', 'required'),
			array('idDisciplina', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>150),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idDisciplina, nombre', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'proyectos' => array(self::HAS_MANY, 'Proyecto', 'disciplina'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idDisciplina' => 'Id Disciplina',
			'nombre' => 'Nombre',
		);
	}
	
	/**
	 *
	 */
	public function getDisciplinas(){
		$disciplinas = Disciplina::model()->findAll();
		return CHtml::listData($disciplinas,'idDisciplina','nombre');
	}
	
	/**
	 *
	 */
	public function getDisciplinaFilters(){
		$disciplinas = Disciplina::model()->findAll();
		return CHtml::listData($disciplinas,'idDisciplina',function($disciplina){
			return ucfirst(strtolower($disciplina->nombre));
		});
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idDisciplina',$this->idDisciplina);
		$criteria->compare('nombre',$this->nombre,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}