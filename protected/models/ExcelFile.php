<?php
class ExcelFile extends CModel {
    /**
    * This is the attribute holding the uploaded file
    * @var CUploadedFile
    */
    public $excelFile;
    public $file_name;
    
    
   /**
   * @return array validation rules for model attributes.
   */
   public function rules()
    {
        return array(
            array('excelFile', 'length', 'max' => 255, 'tooLong' => '{attribute} es muy largo (max {max} carácteres).', 'on' => 'upload'),
            array('excelFile', 'file', 'types' => 'xls,xlsx', 'maxSize' => 1024 * 1024 * 20, 'tooLarge' => 'El tamaño del archivo debe ser menor a 20MB !!!', 'on' => 'upload'),
        );
    }
    
    public function getExcelFileUrl(){
        return Yii::getPathOfAlias('frontend.www.files').'/excel/'.$this->file_name;
    }
    
   public function attributeNames()
{

return array('excelFile','file_name');	

}

 public function __get( $name )
        {
                if( property_exists( $this, $name ) )
                {
                        return $this->$name;
                }
                else
                {
                        return parent::__get( $name );
                }
        }
        
        public function __set( $name, $value )
        {
                if( property_exists( $this, $name ) )
                {
                        $this->$name = $value;
                }
                else
                {
                        parent::__set( $name, $value );
                }
        }
}
?>