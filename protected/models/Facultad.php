<?php

/**
 * This is the model class for table "facultad".
 *
 * The followings are the available columns in table 'facultad':
 * @property integer $idFacultad
 * @property string $nombre
 * @property integer $decano
 *
 * The followings are the available model relations:
 * @property Centro[] $centros
 * @property Decano $decano0
 */
class Facultad extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Facultad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'facultad';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('decano', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>150),
			array('nombre','unique'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idFacultad, nombre, decano', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'centros' => array(self::HAS_MANY, 'Centro', 'facultad'),
			'decano0' => array(self::BELONGS_TO, 'Decano', 'decano'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idFacultad' => 'Id Facultad',
			'nombre' => 'Nombre',
			'decano' => 'Decano',
		);
	}

	public function getFacultades(){
		$facultadesList = array();
		$facultades = Facultad::model()->findAll();
		//ademas agregarle las best matches del suggestMatches para facultades
		foreach($facultades as $f){
			$facultadTemplate = new stdClass;
			$facultadTemplate->nombre = $f->nombre;
			$facultadTemplate->idFacultad = $f->idFacultad;
			array_push($facultadesList,$facultadTemplate);
		}
		$facultadMatches = BestMatches::model()->findAll('domain="Facultad"');
		foreach($facultadMatches as $facultadMatch){
			$facultadTemplate = new stdClass;
			$facultadTemplate->nombre = $facultadMatch->value;
			$facultadTemplate->idFacultad = $facultadMatch->idDominio;
			array_push($facultadesList,$facultadTemplate);
		}
		return $facultadesList;
	}
	
	public function getFacultadFilters($filter=''){
		$facultades = Facultad::model()->findAll($filter);
		return CHtml::listData($facultades,'idFacultad',function($facultad) {
			return ucfirst(strtolower($facultad->nombre));
		});
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idFacultad',$this->idFacultad);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('decano',$this->decano);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}