<?php

// uncomment the following to define a path alias
Yii::setPathOfAlias('frontend.www.files', dirname(__FILE__).DIRECTORY_SEPARATOR.'../../uploadedFiles/');
Yii::setPathOfAlias('logging', dirname(__FILE__).DIRECTORY_SEPARATOR.'../runtime/');


// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'Instituto de Investigaciones',
    // preloading 'log' component
    'preload'=>array('log',
        'bootstrap',
    ),
    // autoloading model and component classes
    'import'=>array(
        'application.models.*',
        'application.components.*',
        'application.vendors.phpexcel.PHPExcel',
        'application.extensions.escriptboost.*',
        'ext.YiiMailer.YiiMailer',
    ),
    'modules'=>array(
        // uncomment the following to enable the Gii tool
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'root',
            'generatorPaths'=>array('bootstrap.gii'),
        ),
    ),
    // application components
    'components'=>array(
        'user'=>array(
            // enable cookie-based authentication
            'allowAutoLogin'=>true,
            'loginRequiredAjaxResponse'=>'YII_LOGIN_REQUIRED',
        ),
        'bootstrap'=>array(
            'class'=>'ext.bootstrap.components.Bootstrap',
            'responsiveCss'=>true,
        ),
        /*'assetManager'=>array(
            'class'=>'EAssetManagerBoost',
            'minifiedExtensionFlags'=>array('min.js','minified.js','packed.js'),
        ),*/
        /*'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName'=>false,
            'rules'=>array(
                '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>/<id:\d+>/*'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            ),
        ),*/
        /*'db'=>array(
            'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
        ),*/
        'db'=>array(
            'connectionString' => (!IN_PRODUCTION)?'mysql:host=localhost;dbname=daedalus':'mysql:host=localhost;dbname=dbarrios_daedalus',
            'emulatePrepare' => true,
            'username' => (!IN_PRODUCTION)?'root':'dbarrios_daedalu',
            'password' => (!IN_PRODUCTION)?'root':'investigaciones2012',
            'schemaCachingDuration'=>(!IN_PRODUCTION)?0:3600,
            'charset' => 'utf8',
            'enableProfiling'=>true,
            'enableParamLogging'=>true,
        ),
        /*'clientScript'=>array(
            'class'=>'EClientScriptBoost',
            'cacheDuration'=>(YII_DEBUG) ? 30 : 120,
        ),*/
        'cache'=>array(
            'class'=>'CFileCache',
        ),
        'errorHandler'=>array(
            // use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'ext.yii-debug-toolbar.YiiDebugToolbarRoute',
                    'levels'=>'info, warning, error',
                    //'ipFilters'=>array('127.0.0.1','192.168.1.215')tivi,
                ),
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'debug, trace, info, warning, error',
                    //'categories'=>'system.*',
                ),
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(
        // this is used in contact page
        'adminEmail'=>'cantoraljoel@gmail.com',
    ),
);
?>
